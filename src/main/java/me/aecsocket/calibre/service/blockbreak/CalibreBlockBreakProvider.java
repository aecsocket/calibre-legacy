package me.aecsocket.calibre.service.blockbreak;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.chamber.bullet.BulletProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;

public class CalibreBlockBreakProvider implements CalibreBlockBreakService {
    @Override
    public boolean breakBlock(ItemEvent base, BulletProjectile projectile, Block block, TickContext tickContext) {
        Sound sound = block.getSoundGroup().getBreakSound();
        if (block.breakNaturally()) {
            block.getWorld().playSound(block.getLocation(), sound, SoundCategory.BLOCKS, 1, 1);
            return true;
        }
        return false;
    }
}
