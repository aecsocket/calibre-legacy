package me.aecsocket.calibre.service.blockbreak;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.chamber.bullet.BulletProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.block.Block;

public interface CalibreBlockBreakService {
    boolean breakBlock(ItemEvent base, BulletProjectile projectile, Block block, TickContext tickContext);
}
