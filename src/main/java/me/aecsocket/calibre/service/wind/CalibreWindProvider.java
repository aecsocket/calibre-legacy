package me.aecsocket.calibre.service.wind;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CalibreWindProvider implements CalibreWindService, Tickable {
    private static final Random random = new Random();

    private final CalibrePlugin plugin;
    private final Map<World, Vector> wind = new HashMap<>();
    private double intensity;

    public CalibreWindProvider(CalibrePlugin plugin) {
        this.plugin = plugin;
        intensity = plugin.setting("service.wind.intensity", double.class, 5d);
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public Map<World, Vector> getWind() { return wind; }
    public Vector getWind(World world) { return wind.computeIfAbsent(world, this::createNewWind).clone(); }

    public double getIntensity() { return intensity; }
    public void setIntensity(double intensity) { this.intensity = intensity; }

    private double rng() { return random.nextDouble() * 2 - 1; }
    public Vector createNewWind(World world) {
        return new Vector(rng() * intensity, 0, rng() * intensity);
    }

    @Override public Vector getWind(Location location) { return getWind(location.getWorld()); }

    @Override
    public void tick(TickContext tickContext) {}
}
