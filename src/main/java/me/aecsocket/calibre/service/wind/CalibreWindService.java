package me.aecsocket.calibre.service.wind;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public interface CalibreWindService {
    Vector getWind(Location location);
}
