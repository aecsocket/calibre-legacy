package me.aecsocket.calibre.service.stabilization;

import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.Vector2;
import org.bukkit.entity.Player;

public interface CalibreAimStabilizationService {
    boolean stabilize(Player player, TickContext tickContext, Vector2 sway);
}
