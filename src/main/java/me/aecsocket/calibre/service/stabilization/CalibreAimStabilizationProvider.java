package me.aecsocket.calibre.service.stabilization;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.util.PlayerData;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import me.aecsocket.unifiedframework.loop.PreciseLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.Vector2;
import org.bukkit.entity.Player;

public class CalibreAimStabilizationProvider implements CalibreAimStabilizationService, Tickable {
    public static final long DEFAULT_STAMINA = 6000;
    public static final long DEFAULT_REGEN_TIME = 2000;

    private final CalibrePlugin plugin;
    private double multiplier;
    private long stamina;
    private long regenTime;
    private double regenMultiplier;
    private SoundData[] startSound;
    private SoundData[] endSound;
    private boolean show;

    public CalibreAimStabilizationProvider(CalibrePlugin plugin) {
        this.plugin = plugin;
        multiplier = plugin.setting("service.aim_stabilization.multiplier", double.class, 0.5);
        stamina = plugin.setting("service.aim_stabilization.stamina", long.class, DEFAULT_STAMINA);
        regenTime = plugin.setting("service.aim_stabilization.regen_time", long.class, DEFAULT_REGEN_TIME);
        regenMultiplier = plugin.setting("service.aim_stabilization.regen_multiplier", double.class, 1d);
        startSound = plugin.setting("service.aim_stabilization.start_sound", SoundData[].class, null);
        endSound = plugin.setting("service.aim_stabilization.end_sound", SoundData[].class, null);
        show = plugin.setting("service.aim_stabilization.show_stamina", boolean.class, true);
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public double getMultiplier() { return multiplier; }
    public void setMultiplier(double multiplier) { this.multiplier = multiplier; }

    public long getStamina() { return stamina; }
    public void setStamina(long stamina) { this.stamina = stamina; }

    public long getRegenTime() { return regenTime; }
    public void setRegenTime(long regenTime) { this.regenTime = regenTime; }

    public double getRegenMultiplier() { return regenMultiplier; }
    public void setRegenMultiplier(double regenMultiplier) { this.regenMultiplier = regenMultiplier; }

    public SoundData[] getStartSound() { return startSound; }
    public void setStartSound(SoundData[] startSound) { this.startSound = startSound; }

    public SoundData[] getEndSound() { return endSound; }
    public void setEndSound(SoundData[] endSound) { this.endSound = endSound; }

    public boolean shows() { return show; }
    public void setShow(boolean show) { this.show = show; }

    @Override
    public boolean stabilize(Player player, TickContext tickContext, Vector2 sway) {
        if (!(tickContext.getLoop() instanceof PreciseLoop)) return false;
        PlayerData data = plugin.getPlayerData(player);
        if (!player.isSneaking()) {
            if (data.ranOutOfStamina())
                data.setRanOutOfStamina(false);
            if (data.wasStabilized()) {
                data.setWasStabilized(false);
                SoundData.play(endSound, player, player.getLocation());
            }
            return false;
        } else if (data.ranOutOfStamina())
            return false;
        if (!data.wasStabilized()) {
            data.setWasStabilized(true);
            SoundData.play(startSound, player, player.getLocation());
        }
        sway.multiply(multiplier);
        data.setStabilizationStamina(data.getStabilizationStamina() - tickContext.getLoop().getPeriod());
        if (data.getStabilizationStamina() <= 0) {
            data.setRanOutOfStamina(true);
            data.setWasStabilized(false);
            SoundData.play(endSound, player, player.getLocation());
        }
        return true;
    }

    @Override
    public void tick(TickContext tickContext) {
        for (PlayerData data : plugin.getPlayerData().values()) {
            long period = tickContext.getLoop().getPeriod();
            long thisStamina = data.getStabilizationStamina();
            int thisRegen = data.getStabilizationRegen();

            if (thisStamina == -1)
                thisStamina = stamina;
            if (thisStamina < stamina) {
                if (thisRegen >= regenTime)
                    thisStamina = (long)Math.min(stamina, thisStamina + (period * regenMultiplier));
                if (show)
                    PlayerProtocol.sendAir(plugin, data.getPlayer(), thisStamina / (double)stamina);
                data.setStabilizationRegen((int)(thisRegen + period));
            }

            data.setStabilizationStamina(thisStamina);
        }
    }
}
