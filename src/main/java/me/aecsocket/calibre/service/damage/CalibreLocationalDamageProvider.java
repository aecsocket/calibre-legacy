package me.aecsocket.calibre.service.damage;

import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.utils.ParticleData;
import org.bukkit.Particle;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalibreLocationalDamageProvider extends CalibreDamageProvider {
    private static final Type locationType = new TypeToken<HashMap<String, HashMap<String, BodyLocation>>>(){}.getType();
    private static final ParticleData testHitParticle = new ParticleData(Particle.FIREWORKS_SPARK, 0, 0, 0);

    private final Map<EntityType, Map<String, BodyLocation>> locations = new HashMap<>();
    private boolean testMode;

    public CalibreLocationalDamageProvider(CalibrePlugin plugin) {
        super(plugin);
        Map<String, HashMap<String, BodyLocation>> rawLocations = plugin.setting("service.locational_damage.locations", locationType, Collections.emptyMap());
        rawLocations.forEach((type, map) -> locations.put(EntityType.valueOf(type.toUpperCase()), map));
        testMode = plugin.setting("service.locational_damage.test_mode", boolean.class, false);
    }

    public Map<EntityType, Map<String, BodyLocation>> getLocations() { return locations; }
    public Map<String, BodyLocation> getLocations(EntityType type) { return locations.get(type); }
    public BodyLocation getLocation(EntityType type, String name) { return locations.getOrDefault(type, Collections.emptyMap()).get(name); }
    public double getMultiplier(EntityType type, String name) { return locations.getOrDefault(type, Collections.emptyMap()).getOrDefault(name, new BodyLocation()).getMultiplier(); }

    public boolean isTestMode() { return testMode; }
    public void setTestMode(boolean testMode) { this.testMode = testMode; }

    public Map.Entry<String, BodyLocation> getLocationEntry(LivingEntity entity, Vector position) {
        if (position == null) return null;
        EntityType type = entity.getType();
        if (locations.containsKey(type)) {
            for (Map.Entry<String, BodyLocation> entry : locations.get(type).entrySet()) {
                BodyLocation location = entry.getValue();
                if (location.isInside(entity, position)) return entry;
            }
        }
        return null;
    }

    public BodyLocation getLocation(LivingEntity entity, Vector position) {
        Map.Entry<String, BodyLocation> entry = getLocationEntry(entity, position);
        return entry == null ? null : entry.getValue();
    }

    public void damage(@NotNull LivingEntity victim, @Nullable Vector position, double damage, EntityDamageEvent event, BodyLocation location) {
        super.damage(victim, position, damage * location.getMultiplier(), event);
    }

    @Override
    public void damage(@NotNull LivingEntity victim, @Nullable Vector position, double damage, EntityDamageEvent event) {
        if (!damageGuard(victim, event)) return;
        if (testMode) {
            Map.Entry<String, BodyLocation> entry = getLocationEntry(victim, position);
            if (entry == null || position == null) return;
            BodyLocation location = entry.getValue();
            double finalDamage = damage * location.getMultiplier();
            if (event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent)event).getDamager() instanceof Player) {
                Player player = (Player)((EntityDamageByEntityEvent)event).getDamager();
                player.sendMessage(getPlugin().gen(
                        player, "chat.locational_damage.test",
                        entry.getKey(), damage, location.getMultiplier(), finalDamage,
                        location.getMinHeight(), BodyLocation.getDelta(victim, position).getY(), location.getMaxHeight()));
                testHitParticle.spawn(player, position.toLocation(player.getWorld()));
            }
            return;
        }

        BodyLocation location = getLocation(victim, position);
        if (location == null)
            super.damage(victim, position, damage, event);
        else
            damage(victim, position, damage, event, location);
    }
}
