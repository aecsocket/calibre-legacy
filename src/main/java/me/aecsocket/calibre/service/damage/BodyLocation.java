package me.aecsocket.calibre.service.damage;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

public class BodyLocation {
    private double minHeight = Double.MIN_VALUE;
    private double maxHeight = Double.MAX_VALUE;
    private double multiplier;

    public double getMinHeight() { return minHeight; }
    public void setMinHeight(double minHeight) { this.minHeight = minHeight; }

    public double getMaxHeight() { return maxHeight; }
    public void setMaxHeight(double maxHeight) { this.maxHeight = maxHeight; }

    public double getMultiplier() { return multiplier; }
    public void setMultiplier(double multiplier) { this.multiplier = multiplier; }

    public boolean isInside(LivingEntity entity, Vector position) {
        double y = getDelta(entity, position).getY();
        return y >= minHeight && y < maxHeight;
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }

    public static Vector getDelta(LivingEntity entity, Vector position) { return position.clone().subtract(entity.getLocation().toVector()); }
}
