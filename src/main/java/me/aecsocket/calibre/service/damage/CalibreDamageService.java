package me.aecsocket.calibre.service.damage;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface CalibreDamageService {
    void damage(@NotNull LivingEntity victim, @Nullable Vector position, double damage, EntityDamageEvent event);
}
