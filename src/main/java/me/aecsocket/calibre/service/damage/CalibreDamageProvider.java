package me.aecsocket.calibre.service.damage;

import me.aecsocket.calibre.CalibrePlugin;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CalibreDamageProvider implements CalibreDamageService {
    private final CalibrePlugin plugin;

    public CalibreDamageProvider(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    protected static boolean damageGuard(LivingEntity victim, EntityDamageEvent event) {
        if (victim instanceof HumanEntity) {
            GameMode gameMode = ((HumanEntity)victim).getGameMode();
            if (gameMode == GameMode.CREATIVE || gameMode == GameMode.SPECTATOR)
                return false;
        }
        return event.callEvent();
    }


    @Override
    public void damage(@NotNull LivingEntity victim, @Nullable Vector position, double damage, EntityDamageEvent event) {
        if (!damageGuard(victim, event)) return;
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event2 = (EntityDamageByEntityEvent)event;
            double health = Math.max(0, victim.getHealth() - damage);
            if (health == 0)
                victim.damage(damage, event2.getDamager());
            else {
                victim.damage(0, event2.getDamager());
                victim.setHealth(health);
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> victim.setVelocity(new Vector()), 1);
            }
            victim.setLastDamageCause(event);
        } else {
            victim.setLastDamageCause(event);
            victim.setHealth(Math.max(0, victim.getHealth() - damage));
        }
    }
}
