package me.aecsocket.calibre.service.casing;

import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.ItemDescription;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import me.aecsocket.calibre.util.CalibreSoundData;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CalibreCasingProvider implements CalibreCasingService, Tickable {
    private static final Type soundsType = new TypeToken<HashMap<String, HashMap<String, CalibreSoundData[]>>>(){}.getType();
    public static final int ITEM_DESPAWN_TIME = 20 * 60 * 5;
    public static final String DEFAULT_NAME = "default";

    private final CalibrePlugin plugin;
    private final Map<String, Map<Material, SoundData[]>> sounds = new HashMap<>();
    private Map<Item, ChamberComponent> casings = new HashMap<>();

    public CalibreCasingProvider(CalibrePlugin plugin) {
        this.plugin = plugin;
        Map<String, Map<String, SoundData[]>> rawSounds = plugin.setting("service.casing.sounds", soundsType, Collections.emptyMap());
        rawSounds.forEach((group, map) ->
                map.forEach((material, sound) ->
                    sounds.computeIfAbsent(group, f -> new HashMap<>()).put(
                            material.equals(DEFAULT_NAME) ? null : Material.valueOf(material.toUpperCase()), sound)
                ));
    }

    public CalibrePlugin getPlugin() { return plugin; }

    @Override
    public void spawnCasing(ItemEvent base, Player player, GunComponent gun, Location location, ChamberComponent chamber, ChamberSlot slot) {
        if (!sounds.containsKey(chamber.getCasingSoundGroup())) return;
        ItemDescription casingItem = chamber.getCasingItem();
        if (casingItem == null) return;

        Vector ejectOffset = base.stat("casing_offset", Vector.class).clone();
        Location ejectLocation = CalibreUtils.getOptionalViewModelPosition(player, ejectOffset);
        Item droppedItem = player.getWorld().dropItem(ejectLocation, casingItem.create(1));
        droppedItem.setPickupDelay(Integer.MAX_VALUE);
        droppedItem.setCanMobPickup(false);
        droppedItem.setTicksLived(ITEM_DESPAWN_TIME - chamber.getCasingLifetime());
        droppedItem.setVelocity(CalibreUtils.getOptionalViewModelPosition(player, ejectOffset.add(base.stat("casing_velocity", Vector.class))).subtract(ejectLocation).toVector());

        casings.put(droppedItem, chamber);
    }

    public Map<Item, ChamberComponent> getCasings() { return casings; }
    public ChamberComponent getCasing(Item item) { return casings.get(item); }
    public void registerCasing(Item item, ChamberComponent chamber) { casings.put(item, chamber); }
    public ChamberComponent removeCasing(Item item) { return casings.remove(item); }

    @Override
    public void tick(TickContext tickContext) {
        Iterator<Map.Entry<Item, ChamberComponent>> iter = casings.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Item, ChamberComponent> entry = iter.next();
            Item item = entry.getKey();
            if (!item.isValid()) {
                iter.remove();
                return;
            }
            if (item.isOnGround()) {
                Map<Material, SoundData[]> group = sounds.get(entry.getValue().getCasingSoundGroup());
                if (group != null) {
                    Material landOn = item.getLocation().subtract(0, 0.01, 0).getBlock().getType();
                    SoundData.play(group.containsKey(landOn) ? group.get(landOn) : group.get(null), item.getLocation());
                }
                item.remove();
                iter.remove();
            }
        }
    }
}
