package me.aecsocket.calibre.service.casing;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface CalibreCasingService {
    void spawnCasing(ItemEvent base, Player player, GunComponent gun, Location location, ChamberComponent chamber, ChamberSlot slot);
}
