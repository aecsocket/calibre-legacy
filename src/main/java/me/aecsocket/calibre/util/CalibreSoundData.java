package me.aecsocket.calibre.util;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.utils.RealSoundData;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.SoundCategory;

public class CalibreSoundData extends RealSoundData implements AcceptsPlugin {
    private transient CalibrePlugin plugin;

    public CalibreSoundData(CalibrePlugin plugin, String sound, SoundCategory category, float volume, float pitch, double dropoffStart, double maxDistance, double speed) {
        super(sound, category, volume, pitch, dropoffStart, maxDistance, plugin, speed);
    }

    public CalibreSoundData() {}

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; super.setPlugin(plugin); }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
