package me.aecsocket.calibre.util;

import me.aecsocket.calibre.CalibrePlugin;

public interface AcceptsPlugin {
    CalibrePlugin getPlugin();
    void setPlugin(CalibrePlugin plugin);
}
