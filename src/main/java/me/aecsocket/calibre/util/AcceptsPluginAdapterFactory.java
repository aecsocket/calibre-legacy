package me.aecsocket.calibre.util;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import me.aecsocket.calibre.CalibrePlugin;

import java.io.IOException;

public class AcceptsPluginAdapterFactory implements TypeAdapterFactory {
    private final CalibrePlugin plugin;

    public AcceptsPluginAdapterFactory(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        if (!AcceptsPlugin.class.isAssignableFrom(type.getRawType()))
            return null;

        TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter json, T value) throws IOException { delegate.write(json, value); }

            @Override
            public T read(JsonReader json) throws IOException {
                T value = delegate.read(json);
                if (value instanceof AcceptsPlugin)
                    ((AcceptsPlugin)value).setPlugin(plugin);
                return value;
            }
        }.nullSafe();
    }
}
