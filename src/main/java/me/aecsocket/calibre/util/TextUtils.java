package me.aecsocket.calibre.util;

import me.aecsocket.calibre.CalibrePlugin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class TextUtils {
    public TextUtils() {}

    public static List<String> lines(String text, @Nullable String prefix) {
        String[] lines = text.split("\n");
        if (prefix != null) {
            for (int i = 0; i < lines.length; i++)
                lines[i] = prefix + lines[i].replace(ChatColor.RESET.toString(), ChatColor.getLastColors(prefix));
        }
        return new ArrayList<>(Arrays.asList(lines));
    }

    public static List<String> lines(String text) { return lines(text, null); }

    public static String format(String msg, Object... args) {
        msg = ChatColor.translateAlternateColorCodes('&', msg);
        for (int i = 0; i < args.length; i++)
            msg = msg.replace("{" + i + "}", args[i].toString());
        return msg;
    }

    public static String[] createBar(int notches, int amount, String barChar) {
        String[] result = new String[2];
        result[0] = barChar.repeat(Math.max(0, Math.min(amount, notches)));
        result[1] = barChar.repeat(Math.max(0, notches - amount));
        return result;
    }

    public static String[] createBar(int notches, int amount, Player player, CalibrePlugin plugin) {
        return createBar(notches, amount, plugin.gen(player, "bar_char"));
    }

    public static String[] createBar(int notches, int amount, Player player, CalibrePlugin plugin, String settingPrefix) {
        double barSize = plugin.setting(settingPrefix + ".action_bar_size", double.class, 100d);
        if (plugin.setting(settingPrefix + ".action_bar_size_fixed", boolean.class, true))
            return createBar((int)barSize, (int)(((double)notches / amount) * barSize), player, plugin);
        else
            return createBar((int)(amount * barSize), (int)(notches * barSize), player, plugin);
    }
}
