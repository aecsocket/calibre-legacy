package me.aecsocket.calibre.util;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.gui.SlotGUI;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.animation.AnimationInstance;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.event.ItemAction;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import me.aecsocket.unifiedframework.gui.core.GUIView;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.Vector2;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class PlayerData implements Tickable {
    private static final PotionEffect fastDiggingEffect = new PotionEffect(PotionEffectType.FAST_DIGGING, 3, 127, false, false, false);
    private static final PotionEffect slowDiggingEffect = new PotionEffect(PotionEffectType.SLOW_DIGGING, 3, 127, false, false, false);
    private static final ParticleData viewOffsetParticle = new ParticleData(Particle.REDSTONE, 0, 0, 0, 0, 0, new Particle.DustOptions(Color.WHITE, 0.1f));

    private final CalibrePlugin plugin;
    private final Player player;
    private AnimationInstance animation;
    private long availableIn;
    private long startAvailable;
    private Runnable availableCallback;
    private ItemAction lastAction;
    private CalibreItem heldItem;
    private Vector viewOffset;
    private boolean wasHolding;
    private int holdTicks;
    private double spread;
    private Vector2 recoil = new Vector2();
    private boolean showSpread;
    private int flash;
    private long stabilizationStamina = -1;
    private boolean wasStabilized;
    private int stabilizationRegen;
    private boolean ranOutOfStamina;

    public PlayerData(CalibrePlugin plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    public CalibrePlugin getPlugin() { return plugin; }
    public Player getPlayer() { return player; }

    public AnimationInstance getAnimation() { return animation; }
    public void setAnimation(AnimationInstance animation) { this.animation = animation; }

    public long getAvailableIn() { return availableIn; }
    public void setAvailableIn(long availableIn) { this.availableIn = availableIn; }

    public long getStartAvailable() { return startAvailable; }
    public void setStartAvailable(long startAvailable) { this.startAvailable = startAvailable; }

    public ItemAction getLastAction() { return lastAction; }
    public void setLastAction(ItemAction lastAction) { this.lastAction = lastAction; }

    public CalibreItem getHeldItem() { return heldItem; }

    public boolean isAvailable() { return System.currentTimeMillis() > availableIn; }
    public void availableIn(long time, ItemAction action, Runnable callback) {
        startAvailable = System.currentTimeMillis();
        availableIn = startAvailable + time;
        lastAction = action;
        availableCallback = callback;
    }
    public void availableIn(long time, ItemAction action) { availableIn(time, action, null); }
    public long getDuration() { return availableIn - startAvailable; }
    public long getElapsed() { return System.currentTimeMillis() - startAvailable; }

    public Runnable getAvailableCallback() { return availableCallback; }
    public void setAvailableCallback(Runnable availableCallback) { this.availableCallback = availableCallback; }

    public Vector getViewOffset() { return viewOffset; }
    public void setViewOffset(Vector viewOffset) { this.viewOffset = viewOffset; }

    public boolean wasHolding() { return wasHolding; }
    public void setWasHolding(boolean wasHolding) { this.wasHolding = wasHolding; }

    public int getHoldTicks() { return holdTicks; }
    public void setHoldTicks(int holdTicks) { this.holdTicks = holdTicks; }

    public double getSpread() { return spread; }
    public void setSpread(double spread) { this.spread = spread; }

    public Vector2 getRecoil() { return recoil; }
    public void setRecoil(Vector2 recoil) { this.recoil = recoil; }

    public boolean showSpread() { return showSpread; }
    public void setShowSpread(boolean showSpread) { this.showSpread = showSpread; }

    public int getFlash() { return flash; }
    public void setFlash(int flash) { this.flash = flash; }

    public long getStabilizationStamina() { return stabilizationStamina; }
    public void setStabilizationStamina(long stabilizationStamina) {
        if (stabilizationStamina < this.stabilizationStamina)
            stabilizationRegen = 0;
        this.stabilizationStamina = stabilizationStamina;
    }

    public boolean wasStabilized() { return wasStabilized; }
    public void setWasStabilized(boolean wasStabilized) { this.wasStabilized = wasStabilized; }

    public int getStabilizationRegen() { return stabilizationRegen; }
    public void setStabilizationRegen(int stabilizationRegen) { this.stabilizationRegen = stabilizationRegen; }

    public boolean ranOutOfStamina() { return ranOutOfStamina; }
    public void setRanOutOfStamina(boolean ranOutOfStamina) { this.ranOutOfStamina = ranOutOfStamina; }

    public boolean checkAction(ItemAction... actions) {
        for (ItemAction action : actions) {
            if (lastAction == action) return true;
        }
        return false;
    }

    public boolean checkAction(boolean available, ItemAction... actions) {
        if (isAvailable() == available) return false;
        return checkAction(actions);
    }

    public boolean actionIs(ItemAction... actions) { return checkAction(true, actions); }
    public boolean actionWas(ItemAction... actions) { return checkAction(false, actions); }

    public void flash(int duration) {
        if (flash == 0) {
            player.sendTitle(plugin.gen(player, "flash"), " ", plugin.setting("effect.flash.fade_in", int.class, 0), 20 * 60 * 2, 0);
            SoundData.play(plugin.setting("effect.flash.sound", SoundData[].class, null), player, player.getLocation());
        }
        flash += duration;
    }

    private void checkCallback() {
        if (System.currentTimeMillis() >= availableIn && availableCallback != null) {
            Runnable callback = availableCallback;
            availableCallback = null;
            callback.run();
            heldItem = plugin.getItem(player.getInventory().getItemInMainHand());
        }
    }

    @Override
    public void tick(TickContext context) {
        if (player.isDead() || !player.isValid() || player.getGameMode() == GameMode.SPECTATOR) return;

        // Scheduler
        boolean holding = player.isHandRaised();
        if (context.getLoop() instanceof SchedulerLoop) {
            heldItem = plugin.getItem(player.getInventory().getItemInMainHand());
        }

        // Both
        checkCallback();

        if (heldItem != null) {
            heldItem.callEvent(CalibreItem.class, (e, i) -> i.hold(e, player, context));
            if (holding && !wasHolding)
                heldItem.callEvent(CalibreItem.class, (e, i) -> i.changeClickState(e, player, context, true));
            if (!holding && wasHolding)
                heldItem.callEvent(CalibreItem.class, (e, i) -> i.changeClickState(e, player, context, false));
        }
        wasHolding = holding;

        if (!(context.getLoop() instanceof SchedulerLoop))
            return;

        // Scheduler

        // Spread
        if (showSpread) {
            String[] bar = TextUtils.createBar(40 * 10, (int)(spread * 10), player, plugin);
            player.sendTitle("", plugin.gen(player, "show_spread", spread, bar[0], bar[1]), 0, 3, 0);
        }

        if (spread > plugin.setting("gun.spread_threshold", double.class, 0.005))
            spread *= plugin.setting("gun.spread_multiplier", double.class, 0.9);
        else
            spread = 0;

        // Recoil
        if (recoil.manhattanLength() > plugin.setting("gun.recoil_threshold", double.class, 0.1)) {
            PlayerProtocol.positionCamera(plugin, player, (float)recoil.getX(), (float)-recoil.getY());
            recoil.multiply(plugin.setting("gun.recoil_multiplier", double.class, 0.5));
        } else
            recoil.zero();

        // Item
        if (heldItem != null && heldItem instanceof CalibreComponent) {
            CalibreComponent component = (CalibreComponent)heldItem;
            if (component.isAnimated() && component.isComplete()) {
                player.addPotionEffect(fastDiggingEffect);
                player.addPotionEffect(slowDiggingEffect);
            }
        }

        // Animation
        if (animation != null)
            context.tick(animation);

        // Flash
        if (flash > 0) {
            --flash;
            if (flash == 0) {
                player.sendTitle(plugin.gen(player, "flash"), " ", 0, 0, plugin.setting("effect.flash.fade_out", int.class, 0));
                SoundData[] sounds = plugin.setting("effect.flash.sound", SoundData[].class, null);
                if (sounds != null) {
                    for (SoundData sound : sounds)
                        player.stopSound(sound.getSound(), sound.getCategory());
                }
                SoundData.play(plugin.setting("effect.flash.end_sound", SoundData[].class, null), player, player.getLocation());
            }
        }

        // View offset
        if (viewOffset != null)
            viewOffsetParticle.spawn(player, CalibreUtils.getViewOffset(player, viewOffset));

        // Slot GUI
        GUIView view = plugin.getUnifiedFramework().getGUIManager().getViews().get(player.getOpenInventory());
        if (view != null && view.getGUI() instanceof SlotGUI) {
            SlotGUI gui = (SlotGUI)view.getGUI();
            if (gui.getClickedSlot() >= 0) {
                CalibreItem linkedItem = plugin.getItem(player.getInventory().getItem(gui.getClickedSlot()));
                if (linkedItem == null)
                    player.closeInventory();
                else if (!linkedItem.equals(gui.getBase())) {
                    ItemStack cursor = view.getHandle().getCursor();
                    view.getHandle().setCursor(null);
                    gui.setBase(linkedItem);
                    gui.open(player);
                    view.getHandle().setCursor(cursor);
                }
            }
        }

        // TODO Faster movespeed when blocking.

        /*
        Location location = player.getLocation().clone();
        if (oldLocation != null) {
            float fPitch = oldLocation.getPitch();
            float fYaw = oldLocation.getYaw();
            float tPitch = location.getPitch();
            float tYaw = location.getYaw();
            float pitchDelta = tPitch - fPitch;
            float yawDelta = tYaw - fYaw;
            //player.sendMessage("yaw = " + fYaw + " / " + tYaw + " = " + yawDelta);
            if (Math.abs(pitchDelta) > 0.01 || Math.abs(yawDelta) > 0.01) {
                float multiplier = -0.4f;
                //player.sendMessage("delta = " + pitchDelta + " / " + yawDelta);
                // pitchDelta = 10, mult = 0.25
                // target = -7.5
                //player.sendMessage("before: " + pitchDelta + ", " + yawDelta);
                pitchDelta = pitchDelta * multiplier;
                yawDelta = yawDelta * multiplier;
                //player.sendMessage("after: " + pitchDelta + ", " + yawDelta);
                PlayerNMS.positionCamera(player, yawDelta, pitchDelta);
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                    player.sendMessage("raw = ("+location.getPitch()+", "+location.getYaw()+") / now = ("+player.getLocation().getPitch()+", "+player.getLocation().getYaw()+")");
                }, 1);
                location.setPitch(location.getPitch() - pitchDelta);
                location.setYaw(location.getYaw() - yawDelta);
            }
        }
        oldLocation = location.clone();

        Location location = player.getLocation();
        if (oldLocation != null) {
            Vector delta = location.clone().subtract(oldLocation).toVector();
            player.sendMessage(String.format("Speed = %.2f | Delta = (%.2f, %.2f, %.2f)", location.distance(oldLocation), delta.getX(), delta.getY(), delta.getZ()));
            if (player.getInventory().getItemInMainHand().getType() == Material.SHIELD && player.isHandRaised()) {
                player.setSprinting(false);
                double y = delta.getY();
                delta.multiply(1);
                delta.setY(y);

                if (delta.getX() <= 5 && delta.getZ() <= 5) {
                    PacketPlayOutPosition packet = new PacketPlayOutPosition(delta.getX(), 0, delta.getZ(), 0, 0, PlayerNMS.FLAGS, 0);
                    ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
                    location.add(delta);
                }
            }
        }
        oldLocation = location;*/
    }
}
