package me.aecsocket.calibre.util;

import me.aecsocket.unifiedframework.registry.Identifiable;

import java.util.HashMap;
import java.util.Map;

public class CategoryContext extends HashMap<String, Identifiable> {
    private final String name;

    public CategoryContext(int initialCapacity, float loadFactor, String name) {
        super(initialCapacity, loadFactor);
        this.name = name;
    }

    public CategoryContext(int initialCapacity, String name) {
        super(initialCapacity);
        this.name = name;
    }

    public CategoryContext(String name) {
        this.name = name;
    }

    public CategoryContext(Map<? extends String, ? extends Identifiable> m, String name) {
        super(m);
        this.name = name;
    }

    public String getName() { return name; }
}
