package me.aecsocket.calibre.util;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.service.damage.CalibreDamageService;
import me.aecsocket.unifiedframework.utils.ParticleData;
import org.bukkit.Bukkit;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MainHand;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

public final class CalibreUtils {
    public static class SearchContext {
        public static final int OFFHAND_SLOT = -999;

        private final CalibreItem result;
        private final ItemStack stack;
        private final int slot;

        public SearchContext(CalibreItem result, ItemStack stack, int slot) {
            this.result = result;
            this.stack = stack;
            this.slot = slot;
        }

        public CalibreItem getResult() { return result; }
        public ItemStack getStack() { return stack; }
        public int getSlot() { return slot; }

        public boolean isValid(Player player) {
            PlayerInventory inv = player.getInventory();
            return stack.isSimilar(slot == OFFHAND_SLOT ? inv.getItemInOffHand() : inv.getItem(slot));
        }

        public void apply(Player player, @Nullable Consumer<ItemStack> modifier) {
            ItemStack copy = stack.clone();
            if (modifier != null)
                modifier.accept(copy);
            PlayerInventory inv = player.getInventory();
            if (slot == OFFHAND_SLOT)
                inv.setItemInOffHand(copy);
            else
                inv.setItem(slot, copy);
        }
    }

    public static Location getViewModelPosition(Location eye, Vector relative) {
        // Thanks jetp250 for designing this. I suck at trig :/
        Vector facing = eye.getDirection();

        Vector xzTangent = new Vector(-facing.getZ(), 0.0, facing.getX()).normalize();
        Vector yTangent = xzTangent.getCrossProduct(facing).normalize();

        Vector finalOffset = xzTangent.clone().multiply(relative.getX())
                .add(facing.multiply(relative.getZ())
                        .add(yTangent.multiply(relative.getY())));

        return eye.add(finalOffset);
    }

    public static Location getViewOffset(Player player, Vector offset, double alignDistance) {
        Location location = getOptionalViewModelPosition(player, offset);
        if (offset == null) return location;
        if (player.getMainHand() == MainHand.LEFT) {
            offset = offset.clone();
            offset.setX(-offset.getX());
        }

        Location eye = getOptionalEyePosition(player);
        Location zero = eye.clone().add(eye.getDirection().multiply(alignDistance));
        location.setDirection(zero.clone().subtract(location).toVector());

        return location;
    }

    public static Location getViewOffset(Player player, Vector offset) { return getViewOffset(player, offset, 150 ); }

    public static boolean isObstructed(Entity entity, Location barrelLocation) {
        Location checkLocation = CalibreUtils.getOptionalEyePosition(entity);
        Vector delta = barrelLocation.clone().subtract(checkLocation).toVector();
        if (delta.getX() + delta.getY() + delta.getZ() == 0) return false;
        return checkLocation.getWorld().rayTraceBlocks(checkLocation, delta.normalize(), delta.length(), FluidCollisionMode.NEVER, true) != null;
    }
    
    public static Location checkObstructed(Entity entity, Location location) { return isObstructed(entity, location) ? null : location; }

    public static Location zero(Location location, double v /* speed */, double x /* dist */, double y /* height */, double g /* gravity */) {
        double theta = (float)(
                Math.atan((Math.pow(v, 2) - Math.sqrt(Math.pow(v, 4) - (g * (g*Math.pow(x, 2) + 2*y*Math.pow(v, 2))))) / (g*x))
        );
        if (!Double.isFinite(theta))
            return location;
        location = location.clone();
        location.setPitch(location.getPitch() + (float)-Math.toDegrees(theta));
        return location;
    }

    public static void spawnLine(Location location, Vector direction, double spread, double distance, ParticleData[] lineParticle, ParticleData[] hitParticle, Predicate<Entity> predicate) {
        World world = location.getWorld();
        location = location.clone();
        direction = direction.clone().normalize().multiply(spread);
        for (double d = 0; d < distance; d += spread) {
            RayTraceResult ray = world.rayTrace(location, direction, spread, FluidCollisionMode.NEVER, true, 0, predicate);
            ParticleData.spawn(lineParticle, location);
            if (ray == null)
                location.add(direction);
            else {
                location = ray.getHitPosition().toLocation(world);
                ParticleData.spawn(hitParticle, location);
                break;
            }
        }
    }

    public static void damage(LivingEntity victim, Vector position, double damage, EntityDamageEvent event) {
        CalibreDamageService damageService = Bukkit.getServicesManager().isProvidedFor(CalibreDamageService.class) ? Bukkit.getServicesManager().getRegistration(CalibreDamageService.class).getProvider() : null;
        if (damageService != null)
            damageService.damage(victim, position, damage, event);
    }

    public static void damage(LivingEntity victim, Vector position, double damage, Entity damager, EntityDamageEvent.DamageCause cause) {
        damage(victim, position, damage, new EntityDamageByEntityEvent(damager, victim, cause, damage));
    }

    public static void damage(LivingEntity victim, Vector position, Entity damager, double damage) {
        damage(victim, position, damage, damager, EntityDamageEvent.DamageCause.ENTITY_ATTACK);
    }

    public static Location getViewModelPosition(LivingEntity entity, Vector relative) { return getViewModelPosition(entity.getEyeLocation(), relative); }
    public static Location getOptionalViewModelPosition(Entity entity, Vector relative) { return getViewModelPosition(getOptionalEyePosition(entity), relative); }
    public static Location getOptionalEyePosition(Entity entity) { return entity instanceof LivingEntity ? ((LivingEntity)entity).getEyeLocation() : entity.getLocation(); }

    public static boolean isLeftClick(PlayerInteractEvent event) { return event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_AIR; }

    public static void safeGiveItem(Player player, ItemStack item) {
        if (player.getInventory().addItem(item).size() > 0)
            player.getWorld().dropItem(player.getLocation(), item);
    }

    public static SearchContext findItem(CalibrePlugin plugin, Player player, Predicate<CalibreItem> predicate) {
        PlayerInventory inv = player.getInventory();
        CalibreItem test = plugin.getItem(inv.getItemInOffHand());
        if (test != null && predicate.test(test)) return new SearchContext(test, inv.getItemInOffHand(), SearchContext.OFFHAND_SLOT);

        for (int i = 0; i < inv.getContents().length; i++) {
            ItemStack stack = inv.getContents()[i];
            test = plugin.getItem(stack);
            if (test != null && predicate.test(test)) return new SearchContext(test, stack, i);
        }

        return null;
    }

    public static SearchContext findByNumber(CalibrePlugin plugin, Player player, Predicate<CalibreItem> predicate, int start, ToIntFunction<CalibreItem> function) {
        PlayerInventory inv = player.getInventory();
        CalibreItem test = plugin.getItem(inv.getItemInOffHand());
        if (test != null && predicate.test(test)) return new SearchContext(test, inv.getItemInOffHand(), SearchContext.OFFHAND_SLOT);

        int min = start;
        SearchContext result = null;
        for (int i = 0; i < inv.getContents().length; i++) {
            ItemStack stack = inv.getContents()[i];
            test = plugin.getItem(stack);
            if (test != null && predicate.test(test)) {
                int amount = function.applyAsInt(test);
                if (amount > min) {
                    min = amount;
                    result = new SearchContext(test, stack, i);
                }
            }
        }

        return result;
    }
}
