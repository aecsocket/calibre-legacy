package me.aecsocket.calibre.util;

import org.bukkit.entity.Player;

public class PlayerWrapper {
    private final Player player;

    public PlayerWrapper(Player player) { this.player = player; }

    public Player get() { return player; }
}
