package me.aecsocket.calibre.util.nms;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.EquivalentConverter;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import me.aecsocket.calibre.CalibrePlugin;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class PlayerProtocol {
    public enum PlayerTeleportFlag {
        X, Y, Z, Y_ROT, X_ROT
    }

    private PlayerProtocol() {}

    public static final Set<PlayerTeleportFlag> POSITION_FLAGS = new HashSet<>(Arrays.asList(
            PlayerTeleportFlag.X,
            PlayerTeleportFlag.Z,
            PlayerTeleportFlag.Y,
            PlayerTeleportFlag.X_ROT,
            PlayerTeleportFlag.Y_ROT
    ));
    public static final EquivalentConverter<PlayerTeleportFlag> TELEPORT_FLAG_CONVERTER = EnumWrappers.getGenericConverter(MinecraftReflection
            .getMinecraftClass("EnumPlayerTeleportFlags",
                    "PacketPlayOutPosition$EnumPlayerTeleportFlags"), PlayerTeleportFlag.class);

    public static void positionCamera(CalibrePlugin plugin, Player player, float x, float y) {
        /*
        NMS code:
        PacketPlayOutPosition packet = new PacketPlayOutPosition(0, 0, 0, x, y, FLAGS, 0);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);*/
        PacketContainer packet = plugin.getProtocolManager().createPacket(PacketType.Play.Server.POSITION);
        packet.getDoubles().write(0, 0d);
        packet.getDoubles().write(1, 0d);
        packet.getDoubles().write(2, 0d);
        packet.getFloat().write(0, x);
        packet.getFloat().write(1, y);
        packet.getSets(TELEPORT_FLAG_CONVERTER).write(0, POSITION_FLAGS);
        plugin.sendPacket(player, packet);
    }

    public static void sendItem(CalibrePlugin plugin, Player player, int slot, ItemStack item) {
        /*
        NMS code:
        PacketPlayOutSetSlot packet = new PacketPlayOutSetSlot(-2, slot, CraftItemStack.asNMSCopy(item));
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);*/
        PacketContainer packet = plugin.getProtocolManager().createPacket(PacketType.Play.Server.SET_SLOT);
        packet.getIntegers().write(0, -2);
        packet.getIntegers().write(1, slot);
        packet.getItemModifier().write(0, item);
        plugin.sendPacket(player, packet);
    }

    public static void sendHand(CalibrePlugin plugin, Player player, ItemStack item) { sendItem(plugin, player, player.getInventory().getHeldItemSlot(), item); }
    public static void sendOffhand(CalibrePlugin plugin, Player player, ItemStack item) { sendItem(plugin, player, -45, item); } // TODO Correct this.

    public static void setFOV(CalibrePlugin plugin, Player player, float fov) {
        PacketContainer packet = plugin.getProtocolManager().createPacket(PacketType.Play.Server.ABILITIES);
        packet.getBooleans().write(0, player.isInvulnerable());
        packet.getBooleans().write(1, player.isFlying());
        packet.getBooleans().write(2, player.getAllowFlight());
        packet.getFloat().write(0, player.getFlySpeed());
        packet.getFloat().write(1, fov);
        plugin.sendPacket(player, packet);
    }

    public static void resetFOV(CalibrePlugin plugin, Player player) { setFOV(plugin, player, 0.1f); }

    public static void sendAir(CalibrePlugin plugin, Player player, int air) {
        PacketContainer packet = plugin.getProtocolManager().createPacket(PacketType.Play.Server.ENTITY_METADATA);
        packet.getIntegers().write(0, player.getEntityId());
        packet.getWatchableCollectionModifier().write(0, Collections.singletonList(
                new WrappedWatchableObject(new WrappedDataWatcher.WrappedDataWatcherObject(1, WrappedDataWatcher.Registry.get(Integer.class)), air)
        ));
        plugin.sendPacket(player, packet);
    }

    public static void sendAir(CalibrePlugin plugin, Player player, double percentage) {
        sendAir(plugin, player, (int)(((Math.round(percentage * 10.0) / 10.0) - 0.05) * player.getMaximumAir()));
    }
}
