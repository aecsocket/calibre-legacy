package me.aecsocket.calibre.util.nms;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.utility.MinecraftVersion;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.Vector3F;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.ItemDescription;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import me.aecsocket.unifiedframework.utils.PrettyLogger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.UUID;

public final class WorldProtocol {
    private WorldProtocol() {}

    public static void spawnBulletHole(CalibrePlugin plugin, Location location, BlockFace hitFace) {
        // Setup
        ItemDescription item = plugin.setting("gun.bullet_hole.item", ItemDescription.class, null);
        if (item == null) return;

        Vector reflect = FrameworkUtils.getReflectionDirection(hitFace);
        Location locReflect = new Location(null, 0, 0, 0).setDirection(reflect);
        float yaw = locReflect.getYaw();
        float pitch = locReflect.getPitch();

        double x = location.getX();
        double y = location.getY() - (pitch == 90 ? 1.5 : pitch == -90 ? 1.4 : 1.3);
        double z = location.getZ();

        // Protocol
        ProtocolManager protocol = plugin.getProtocolManager();

        // Create entity
        int id = (int)(Math.random() * Integer.MAX_VALUE);;
        PacketContainer entityPacket = protocol.createPacket(PacketType.Play.Server.SPAWN_ENTITY);
        entityPacket.getIntegers().write(0, id); // Entity ID
        entityPacket.getUUIDs().write(0, UUID.randomUUID()); // UUID
        entityPacket.getEntityTypeModifier().write(0, EntityType.ARMOR_STAND); // Entity type
        entityPacket.getDoubles().write(0, x); // X
        entityPacket.getDoubles().write(1, y); // Y
        entityPacket.getDoubles().write(2, z); // Z
        entityPacket.getIntegers().write(1, 0); // Velocity X
        entityPacket.getIntegers().write(2, 0); // Velocity Y
        entityPacket.getIntegers().write(3, 0); // Velocity Z
        entityPacket.getIntegers().write(4, 0); // Pitch
        entityPacket.getIntegers().write(5, (int)(yaw * 256 / 360)); // Yaw
        entityPacket.getIntegers().write(6, 0); // Data

        // Set metadata
        PacketContainer metaPacket = protocol.createPacket(PacketType.Play.Server.ENTITY_METADATA);
        metaPacket.getIntegers().write(0, id); // Entity ID
        metaPacket.getWatchableCollectionModifier().write(0, Arrays.asList(
                new WrappedWatchableObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte)0x20), // Entity: invisible
                new WrappedWatchableObject(new WrappedDataWatcher.WrappedDataWatcherObject(14, WrappedDataWatcher.Registry.get(Byte.class)), (byte)0x10), // Armor stand: marker
                new WrappedWatchableObject(new WrappedDataWatcher.WrappedDataWatcherObject(15, WrappedDataWatcher.Registry.getVectorSerializer()), Vector3F.getConverter().getGeneric(new Vector3F(pitch, 0, 0))) // Armor stand: head pose
        ));

        // Set equipment
        PacketContainer equipmentPacket = protocol.createPacket(PacketType.Play.Server.ENTITY_EQUIPMENT);
        equipmentPacket.getIntegers().write(0, id); // Entity ID
        if (MinecraftVersion.atOrAbove(MinecraftVersion.NETHER_UPDATE)) {
            // 1.16 code TODO
            // field "b" = List<nms.Pair<nms.EquipmentSlot, nms.ItemStack>>
            plugin.log(PrettyLogger.Level.ERROR, "Bullet holes are not supported in 1.16 yet! Please disable `gun.bullet_hole.enable` in the settings for now.");
            return;
        } else {
            // 1.15- code
            equipmentPacket.getItemModifier().write(0, item.create(1)); // Item
            equipmentPacket.getItemSlots().write(0, EnumWrappers.ItemSlot.HEAD); // Helmet slot
        }

        // Destroy entity
        PacketContainer destroyPacket = protocol.createPacket(PacketType.Play.Server.ENTITY_DESTROY);
        destroyPacket.getIntegerArrays().write(0, new int[]{id}); // Entity ID

        location.getNearbyPlayers(32).forEach(player -> {
            plugin.sendPacket(player, entityPacket);
            plugin.sendPacket(player, metaPacket);
            plugin.sendPacket(player, equipmentPacket);
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> plugin.sendPacket(player, destroyPacket), (long)(plugin.setting("gun.bullet_hole.despawn", double.class, 30d) * 20));
        });

        /*
        NMS code:
        Entity entity = new EntityArmorStand(((CraftWorld)location.getWorld()).getHandle(), x, y, z);
        int id = entity.getId();

        PacketPlayOutSpawnEntity packet1 = new PacketPlayOutSpawnEntity(
                id, entity.getUniqueID(),
                x, y, z, 0, yaw,
                EntityTypes.ARMOR_STAND, 0, new Vec3D(0, 0, 0));

        DataWatcher watcher = new DataWatcher(entity);
        watcher.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), (byte)0x20);
        watcher.register(new DataWatcherObject<>(14, DataWatcherRegistry.a), (byte)0x10);
        watcher.register(new DataWatcherObject<>(15, DataWatcherRegistry.k), new Vector3f(pitch, 0, 0));
        PacketPlayOutEntityMetadata packet2 = new PacketPlayOutEntityMetadata(id, watcher, true);

        PacketPlayOutEntityEquipment packet3 = new PacketPlayOutEntityEquipment(id, EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(item.create(1)));

        PacketPlayOutEntityDestroy packet4 = new PacketPlayOutEntityDestroy(id);

        location.getNearbyPlayers(32).forEach(player -> {
            PlayerConnection conn = ((CraftPlayer)player).getHandle().playerConnection;
            conn.sendPacket(packet1);
            conn.sendPacket(packet2);
            conn.sendPacket(packet3);
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> conn.sendPacket(packet4), (long)(plugin.setting("gun.bullet_hole.despawn", double.class, 30d) * 20));
        });*/
    }
}
