package me.aecsocket.calibre;

import co.aikar.commands.PaperCommandManager;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.effect.WorldEffects;
import me.aecsocket.calibre.handle.CalibreCommand;
import me.aecsocket.calibre.handle.CalibrePacketAdapter;
import me.aecsocket.calibre.handle.EventHandle;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.CalibreItemAdapter;
import me.aecsocket.calibre.item.ItemReference;
import me.aecsocket.calibre.item.SuppliesItem;
import me.aecsocket.calibre.item.animation.Animation;
import me.aecsocket.calibre.item.animation.AnimationAdapter;
import me.aecsocket.calibre.item.component.*;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.gun.*;
import me.aecsocket.calibre.item.gun.ammo.*;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import me.aecsocket.calibre.item.gun.chamber.bullet.Bullet;
import me.aecsocket.calibre.item.gun.chamber.explosive.ExplosiveBullet;
import me.aecsocket.calibre.item.gun.chamber.incendiary.IncendiaryBullet;
import me.aecsocket.calibre.item.melee.MeleeComponent;
import me.aecsocket.calibre.item.throwable.damage.DamageThrowable;
import me.aecsocket.calibre.item.throwable.effect.EffectThrowable;
import me.aecsocket.calibre.item.throwable.explosive.ExplosiveThrowable;
import me.aecsocket.calibre.item.throwable.fire.FireThrowable;
import me.aecsocket.calibre.item.throwable.flash.FlashThrowable;
import me.aecsocket.calibre.item.throwable.triggerable.TriggerableAdapter;
import me.aecsocket.calibre.service.blockbreak.CalibreBlockBreakProvider;
import me.aecsocket.calibre.service.blockbreak.CalibreBlockBreakService;
import me.aecsocket.calibre.service.casing.CalibreCasingProvider;
import me.aecsocket.calibre.service.casing.CalibreCasingService;
import me.aecsocket.calibre.service.damage.CalibreDamageProvider;
import me.aecsocket.calibre.service.damage.CalibreDamageService;
import me.aecsocket.calibre.service.damage.CalibreLocationalDamageProvider;
import me.aecsocket.calibre.service.stabilization.CalibreAimStabilizationProvider;
import me.aecsocket.calibre.service.stabilization.CalibreAimStabilizationService;
import me.aecsocket.calibre.service.wind.CalibreWindProvider;
import me.aecsocket.calibre.service.wind.CalibreWindService;
import me.aecsocket.calibre.util.*;
import me.aecsocket.unifiedframework.UnifiedFramework;
import me.aecsocket.unifiedframework.exception.LocaleException;
import me.aecsocket.unifiedframework.exception.ResolveException;
import me.aecsocket.unifiedframework.gui.core.GUIVector;
import me.aecsocket.unifiedframework.item.core.Item;
import me.aecsocket.unifiedframework.locale.LocaleManager;
import me.aecsocket.unifiedframework.loop.PreciseLoop;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import me.aecsocket.unifiedframework.registry.Identifiable;
import me.aecsocket.unifiedframework.registry.Ref;
import me.aecsocket.unifiedframework.registry.Registry;
import me.aecsocket.unifiedframework.resource.Settings;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.stat.StatDataAdapter;
import me.aecsocket.unifiedframework.utils.*;
import me.aecsocket.unifiedframework.utils.json.JsonAdapters;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;
import me.aecsocket.unifiedframework.utils.json.adapter.DefaultableRuntimeTypeAdapterFactory;
import me.aecsocket.unifiedframework.utils.json.adapter.RuntimeTypeAdapterFactory;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Supplier;

public class CalibrePlugin extends JavaPlugin implements Tickable {
    public static final String ID_CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789_";
    public static final String WIKI_URL = "https://gitlab.com/aecsocket/calibre/-/wikis/home";

    private PrettyLogger logger;
    private PaperCommandManager commandManager;
    private UnifiedFramework unifiedFramework;
    private Registry registry;
    private SchedulerLoop schedulerLoop;
    private PreciseLoop preciseLoop;
    private ProtocolManager protocolManager;
    private CalibrePacketAdapter packetAdapter;
    private WorldEffects worldEffects;

    private CalibreCommand command;
    private EventHandle eventHandle;

    private StatDataAdapter statDataAdapter;
    private CalibreComponentAdapter componentAdapter;
    private ComponentReferenceAdapter componentReferenceAdapter;
    private RuntimeTypeAdapterFactory<Identifiable> identifiableAdapter;
    private DefaultableRuntimeTypeAdapterFactory<CalibreComponentSlot> componentSlotAdapter;
    private CalibreSlotMapAdapter slotMapAdapter;
    private Gson gson;


    private Settings settings;
    private final Map<String, NamespacedKey> keys = new HashMap<>();
    private final Map<Player, PlayerData> playerData = new HashMap<>();
    private final List<String> loadErrors = new ArrayList<>();
    private final Map<Object, Object> inbuiltProviders = new HashMap<>();

    @Override
    public void onEnable() {
        logger = new PrettyLogger(this, PrettyLogger.Level.VERBOSE);
        commandManager = new PaperCommandManager(this);
        unifiedFramework = new UnifiedFramework(this);
        registry = new Registry();
        schedulerLoop = new SchedulerLoop(this);
        preciseLoop = new PreciseLoop(2);
        protocolManager = ProtocolLibrary.getProtocolManager();
        packetAdapter = new CalibrePacketAdapter(this);
        worldEffects = new WorldEffects();

        schedulerLoop.registerTickable(this);
        schedulerLoop.start();
        preciseLoop.registerTickable(this);
        preciseLoop.start();

        schedulerLoop.registerTickable(worldEffects);

        protocolManager.addPacketListener(packetAdapter);

        commandManager.getCommandCompletions().registerCompletion("players", ctx -> CommandUtils.getPlayers());
        commandManager.getCommandCompletions().registerCompletion("categories", ctx -> registry.get().keySet());
        commandManager.getCommandCompletions().registerCompletion("ids", ctx -> {
            Map<String, Identifiable> map = registry.get(
                    ctx.hasConfig("c")
                            ? ctx.getConfig("c")
                            : ctx.getContextValue(CategoryContext.class, Integer.parseInt(ctx.getConfig("i"))).getName()
            );
            return map == null ? null : map.keySet();
        });

        commandManager.getCommandContexts().registerContext(PlayerWrapper.class, ctx -> {
            String name = ctx.popFirstArg();
            Player player = CommandUtils.getTarget(name, ctx.getSender());
            if (player == null)
                return null;
            return new PlayerWrapper(player);
        });
        commandManager.getCommandContexts().registerContext(CategoryContext.class, ctx -> {
            String name = ctx.popFirstArg();
            if (registry.has(name))
                return new CategoryContext(registry.get(name), name);
            return new CategoryContext("");
        });
        commandManager.getCommandContexts().registerContext(SuppliesItem.class, ctx -> {
            String category = ctx.hasFlag("c") ? ctx.getFlagValue("c", (String)null) : ((CategoryContext)ctx.getPassedArgs().get(ctx.getFlagValue("k", ""))).getName();
            String id = ctx.popFirstArg();
            if (registry.has(category, id, SuppliesItem.class))
                return registry.get(category, id);
            return null;
        });

        command = new CalibreCommand(this);
        commandManager.registerCommand(command);
        eventHandle = new EventHandle(this);
        getServer().getPluginManager().registerEvents(eventHandle, this);

        statDataAdapter = new StatDataAdapter();
        componentAdapter = new CalibreComponentAdapter(this);
        componentReferenceAdapter = new ComponentReferenceAdapter(this);
        identifiableAdapter = RuntimeTypeAdapterFactory.of(Identifiable.class);
        componentSlotAdapter = DefaultableRuntimeTypeAdapterFactory.of(CalibreComponentSlot.class);
        slotMapAdapter = new CalibreSlotMapAdapter(this);
        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Animation.class, new AnimationAdapter())
                .registerTypeAdapter(BoundingBox.class, JsonAdapters.BOUNDING_BOX)
                .registerTypeAdapter(ChatColors.class, JsonAdapters.CHAT_COLORS)
                .registerTypeAdapter(GUIVector.class, JsonAdapters.GUI_VECTOR)
                .registerTypeAdapter(Location.class, JsonAdapters.LOCATION)
                .registerTypeAdapter(Optional.class, JsonAdapters.OPTIONAL)
                .registerTypeAdapter(ParticleData.class, JsonAdapters.PARTICLE_DATA)
                .registerTypeAdapter(PotionEffect.class, JsonAdapters.POTION_EFFECT)
                .registerTypeAdapter(Vector.class, JsonAdapters.VECTOR)
                .registerTypeAdapter(Vector2.class, JsonAdapters.VECTOR2)

                .registerTypeAdapter(StatData.class, statDataAdapter)
                .registerTypeAdapter(ComponentReference.class, componentReferenceAdapter)
                .registerTypeAdapter(CalibreSlotMap.class, slotMapAdapter)
                .registerTypeAdapter(AmmoReference.class, new AmmoReferenceAdapter())
                .registerTypeAdapterFactory(new AcceptsPluginAdapterFactory(this))
                .registerTypeAdapterFactory(componentAdapter)
                .registerTypeAdapterFactory(componentSlotAdapter)
                .registerTypeAdapterFactory(identifiableAdapter)
                .create();

        registerInbuilt();
        load();
    }

    @Override
    public void onDisable() {
        schedulerLoop.stop();
        preciseLoop.stop();
    }

    public PrettyLogger getPluginLogger() { return logger; }
    public PaperCommandManager getCommandManager() { return commandManager; }
    public UnifiedFramework getUnifiedFramework() { return unifiedFramework; }
    public Registry getRegistry() { return registry; }
    public SchedulerLoop getSchedulerLoop() { return schedulerLoop; }
    public PreciseLoop getPreciseLoop() { return preciseLoop; }
    public ProtocolManager getProtocolManager() { return protocolManager; }
    public CalibrePacketAdapter getPacketAdapter() { return packetAdapter; }
    public WorldEffects getWorldEffects() { return worldEffects; }

    public CalibreCommand getCommand() { return command; }
    public EventHandle getEventHandle() { return eventHandle; }

    public StatDataAdapter getStatDataAdapter() { return statDataAdapter; }
    public CalibreComponentAdapter getComponentAdapter() { return componentAdapter; }
    public ComponentReferenceAdapter getComponentReferenceAdapter() { return componentReferenceAdapter; }
    public DefaultableRuntimeTypeAdapterFactory<CalibreComponentSlot> getComponentSlotAdapter() { return componentSlotAdapter; }
    public CalibreSlotMapAdapter getSlotMapAdapter() { return slotMapAdapter; }
    public Gson getGson() { return gson; }

    public Settings getSettings() { return settings; }
    public Map<Player, PlayerData> getPlayerData() { return new HashMap<>(playerData); }
    public PlayerData getPlayerData(Player player) { return playerData.computeIfAbsent(player, p -> new PlayerData(this, p)); }
    public PlayerData removePlayerData(Player player) { return playerData.remove(player); }
    public PlayerData resetPlayerData(Player player) {
        removePlayerData(player);
        return getPlayerData(player);
    }
    public List<String> getLoadErrors() { return loadErrors; }

    protected void registerInbuilt() {
        // Core
        registerIdentifiableType(ItemReference.class, "item");
        unifiedFramework.getItemManager().registerItemAdapter(componentAdapter);

        // Melee
        registerComponentType(MeleeComponent.class, "melee_component", () -> MeleeComponent.STATS);

        // Gun
        registerComponentType(GunComponent.class, "gun_component", () -> GunComponent.STATS, new GunComponentAdapter<>(this));
        registerComponentType(Bullet.class, "bullet", () -> Bullet.STATS);
        registerComponentType(IncendiaryBullet.class, "incendiary_bullet", () -> IncendiaryBullet.STATS);
        registerComponentType(ExplosiveBullet.class, "explosive_bullet", () -> ExplosiveBullet.STATS);
        registerComponentType(AmmoComponent.class, "ammo_component", () -> AmmoComponent.STATS, new AmmoComponentAdapter<>(this));
        registerComponentType(SingleAmmoComponent.class, "single_ammo_component", () -> SingleAmmoComponent.STATS, new AmmoComponentAdapter<>(this));
        registerComponentType(LaserComponent.class, "laser_component", () -> LaserComponent.STATS);
        registerComponentType(RangefinderComponent.class, "rangefinder_component", () -> RangefinderComponent.STATS);
        registerComponentType(WindMeasureComponent.class, "wind_measure_component", () -> RangefinderComponent.STATS);

        registerComponentReferenceType(AmmoComponentReference.class, "ammo",
                (ref, jType, context) -> JsonUtils.buildObject()
                        .add("type", "ammo")
                        .add("id", ref.getId())
                        .add("ammo", context.serialize(((AmmoComponentReference)ref).getAmmo()))
                        .create(),
                (json, jType, context) -> {
                    JsonObject object = JsonUtils.assertObject(json);
                    return new AmmoComponentReference(
                            JsonUtils.get(object, "id").getAsString(),
                            context.deserialize(JsonUtils.get(object, "ammo"), new TypeToken<LinkedList<AmmoReference>>(){}.getType())
                    );
                });

        registerComponentSlotType(ChamberSlot.class, "chamber");
        registerComponentSlotType(AmmoSlot.class, "ammo");
        registerComponentSlotType(UnloadableAmmoSlot.class, "unloadable_ammo");

        // Throwable
        registerComponentType(ExplosiveThrowable.class, "explosive_throwable", () -> ExplosiveThrowable.STATS, new TriggerableAdapter<>(this));
        registerComponentType(EffectThrowable.class, "effect_throwable", () -> EffectThrowable.STATS);
        registerComponentType(DamageThrowable.class, "damage_throwable", () -> DamageThrowable.STATS);
        registerComponentType(FireThrowable.class, "fire_throwable", () -> FireThrowable.STATS);
        registerComponentType(FlashThrowable.class, "flash_throwable", () -> FlashThrowable.STATS);
    }

    public void load() {
        File dataFolder = getDataFolder();
        if (!dataFolder.exists())
            dataFolder.mkdirs();
        boolean noData = true;
        for (File file : dataFolder.listFiles()) {
            if (file.isDirectory()) {
                noData = false;
                break;
            }
        }
        if (noData) {
            log(PrettyLogger.Level.ERROR, "No data found. Check the documentation to learn how to set up this plugin.");
            log(PrettyLogger.Level.ERROR, WIKI_URL);
            settings = new Settings(gson);
            return;
        }
        loadSettings();
        loadProviders();
        loadData();
    }

    public void loadSettings() {
        try {
            settings = Settings.loadFrom(this, gson);
        } catch (IOException e) {
            log(PrettyLogger.Level.ERROR, "Could not load settings file: {0}", e.getMessage());
            settings = new Settings(gson);
        }
        logger.setLevel(setting("log_level", PrettyLogger.Level.class, PrettyLogger.Level.VERBOSE));
    }

    private void unloadProviders() {
        inbuiltProviders.forEach((service, provider) -> {
            if (provider instanceof Tickable)
                schedulerLoop.unregisterTickable((Tickable)provider);
        });
        inbuiltProviders.clear();
        getServer().getServicesManager().unregisterAll(this);
    }

    private <T> void loadInbuiltProvider(String name, Class<T> service, T provider, T alternateProvider) {
        T usedProvider = provider;
        if (!setting("service." + name + ".enable", boolean.class, true)) {
            if (alternateProvider == null) return;
            usedProvider = alternateProvider;
        };
        inbuiltProviders.put(service, usedProvider);
        getServer().getServicesManager().register(service, usedProvider, this, ServicePriority.Lowest);
        if (usedProvider instanceof Tickable)
            schedulerLoop.registerTickable((Tickable)usedProvider);
    }

    private <T> void loadInbuiltProvider(String name, Class<T> service, T provider) { loadInbuiltProvider(name, service, provider, null); }

    public void loadProviders() {
        unloadProviders();
        loadInbuiltProvider("locational_damage", CalibreDamageService.class, new CalibreLocationalDamageProvider(this), new CalibreDamageProvider(this));
        loadInbuiltProvider("aim_stabilization", CalibreAimStabilizationService.class, new CalibreAimStabilizationProvider(this));
        loadInbuiltProvider("wind", CalibreWindService.class, new CalibreWindProvider(this));
        loadInbuiltProvider("block_break", CalibreBlockBreakService.class, new CalibreBlockBreakProvider());
        loadInbuiltProvider("casing", CalibreCasingService.class, new CalibreCasingProvider(this));
    }

    public void loadData() {
        unifiedFramework.getLocaleManager().unregisterAll(this);
        unifiedFramework.getLocaleManager().registerDataTranslations(this);

        loadErrors.clear();
        registry.reload(walkLoad(null, new HashMap<>()));
        registry.getRefs().forEach((type, map) -> map.forEach((id, ref) -> resolve(ref)));
        registry.get().forEach((type, map) -> map.forEach((id, obj) -> resolve(obj)));
    }

    public void resolve(Identifiable id) {
        try {
            id.resolve(registry);
            if (!(id instanceof Ref))
                log(PrettyLogger.Level.VERBOSE, "Resolved {0} {1}", id.getType(), id.getId());
        } catch (ResolveException e) {
            loadError("Failed to resolve {0} {1}: {2}", id.getType(), id.getId(), e.getMessage());
        }
    }

    private void loadError(String msg, Object... args) {
        String text = TextUtils.format(msg, args);
        logger.log(PrettyLogger.Level.WARNING, text);
        loadErrors.add(text);
    }

    private Map<String, Map<String, Identifiable>> walkLoad(Path path, Map<String, Map<String, Identifiable>> map) {
        File root = path == null ? getDataFolder() : path.toFile();
        if (!root.isDirectory() || root.getName().equals("lang"))
            return map;
        for (File file : root.listFiles()) {
            if (file.isDirectory())
                walkLoad(root.toPath().resolve(file.getName()), map);
            else if (path != null) {
                // If root is null, we skip all non-directory folders.
                // This allows custom files in the root dir of the plugin folder
                // to not cause errors, since they will not necessarily be valid Identifiables.
                String data;
                try {
                    InputStream stream = new FileInputStream(file);
                    data = FrameworkUtils.streamToString(stream);
                    stream.close();
                } catch (IOException e) {
                    loadError("Failed to read file {0}: {1}", file.getAbsolutePath(), e.getMessage());
                    continue;
                }

                JsonElement tree;
                try {
                    tree = gson.fromJson(data, JsonElement.class);
                    if (tree == null)
                        continue;
                    JsonElement[] elements;
                    if (tree.isJsonArray()) {
                        JsonArray array = tree.getAsJsonArray();
                        elements = new JsonElement[array.size()];
                        for (int i = 0; i < array.size(); i++)
                            elements[i] = array.get(i);
                    } else if (tree.isJsonObject())
                        elements = new JsonElement[] { tree };
                    else {
                        loadError("File {0} is not JSON object or array", file.getAbsolutePath());
                        continue;
                    }
                    for (JsonElement element : elements) {
                        Identifiable id = gson.fromJson(element, Identifiable.class);

                        if (id == null) {
                            loadError("Parsed file {0} is invalid", file.getAbsolutePath());
                            continue;
                        }

                        String sId = id.getId();
                        if (!sId.chars().allMatch(c -> ID_CHARACTERS.contains(Character.toString(c)))) {
                            loadError("Parsed ID '{0}' does not follow [a-z][0-9]_ pattern", sId);
                            continue;
                        }

                        String type = id.getType();
                        if (map.containsKey(type) && map.get(type).containsKey(sId)) {
                            loadError("Object '{0}' with ID '{1}' already exists", type, sId);
                        }

                        map.computeIfAbsent(id.getType(), f -> new HashMap<>()).put(id.getId(), id);
                        log(PrettyLogger.Level.VERBOSE, "Registered {0} {1}", id.getType(), id.getId());
                    }
                } catch (JsonParseException | IllegalArgumentException e) {
                    loadError("Failed to parse {0}: {1}", file.getAbsolutePath(), e.getMessage());
                }
            }
        }
        return map;
    }

    public <T extends Identifiable> void registerIdentifiableType(Class<T> type, String name) {
        identifiableAdapter.registerSubtype(type, name);
    }

    public <T extends CalibreComponent> void registerComponentType(Class<T> type, String name, Supplier<StatMap> statSupplier, @Nullable CalibreItemAdapter<T> adapter) {
        registerIdentifiableType(type, name);
        componentAdapter.registerStatSupplier(type, statSupplier);
        if (adapter != null)
            unifiedFramework.getItemManager().registerItemAdapter(adapter);
    }

    public <T extends CalibreComponent> void registerComponentType(Class<T> type, String name, Supplier<StatMap> statSupplier) {
        registerComponentType(type, name, statSupplier, null);
    }

    public void registerComponentReferenceType(Class<? extends ComponentReference> type, String name, ComponentReferenceAdapter.Serializer serializer, ComponentReferenceAdapter.Deserializer deserializer) {
        componentReferenceAdapter.getSerializers().put(type, serializer);
        componentReferenceAdapter.getDeserializers().put(name, deserializer);
    }

    public void registerComponentSlotType(Class<? extends CalibreComponentSlot> type, String name) {
        componentSlotAdapter.registerSubtype(type, name);
    }

    @Override
    public void tick(TickContext context) {
        getServer().getOnlinePlayers().forEach(player ->
                context.tick(getPlayerData(player)));
    }

    public void log(PrettyLogger.Level level, String msg, Object... args) { logger.log(level, TextUtils.format(msg, args)); }

    public String getLocale() { return setting("locale", String.class, LocaleManager.DEFAULT_LOCALE); }
    public String gen(String locale, String key, Object... args) {
        try {
            return unifiedFramework.getLocaleManager().generate(this, locale, key, args);
        } catch (LocaleException e) {
            log(PrettyLogger.Level.WARNING, ChatColor.stripColor(e.getMessage()));
            if (e.getCause() != null)
                log(PrettyLogger.Level.WARNING, e.getCause().getLocalizedMessage());
            return unifiedFramework.getLocaleManager().fallbackMessage(key);
        }
    }
    public String gen(String key, Object... args) { return gen(getLocale(), key, args); }
    public String gen(CommandSender sender, String key, Object... args) { return gen(sender instanceof Player ? ((Player)sender).getLocale() : getLocale(), key, args); }
    public String gen(Player player, String key, Object... args) { return gen(player == null ? getLocale() : player.getLocale(), key, args); }

    public <T> T setting(String path, Type type, T defaultValue) {
        T result = settings.get(path, type);
        return result == null ? defaultValue : result;
    }
    public <T> T setting(String path, Class<T> type, T defaultValue) { return setting(path, (Type)type, defaultValue); }
    public <T> Optional<T> optSetting(String path, Type type) {
        return Optional.ofNullable(settings.get(path, type));
    }
    public <T> Optional<T> optSetting(String path, Class<T> type) { return optSetting(path, (Type)type); }

    public NamespacedKey key(String name) { return keys.computeIfAbsent(name, s -> new NamespacedKey(this, s)); }
    public NamespacedKey typeKey() { return unifiedFramework.getItemManager().getTypeKey(); }

    public void sendPacket(Player player, PacketContainer packer) {
        try {
            protocolManager.sendServerPacket(player, packer);
        } catch (InvocationTargetException | IllegalArgumentException ignore) {}
    }

    public double blockHardness(Block block) {return setting("hardness." + block.getType().name().toLowerCase(), float.class, block.getType().getBlastResistance()); }

    public CalibreItem getItem(ItemStack stack) {
        if (stack == null || stack.getType() == Material.AIR) return null;
        Item item = unifiedFramework.getItemManager().getItem(stack);
        return item instanceof CalibreItem ? (CalibreItem)item : null;
    }
}
