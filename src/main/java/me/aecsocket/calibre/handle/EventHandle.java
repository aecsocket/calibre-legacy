package me.aecsocket.calibre.handle;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.event.EventModifier;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EventHandle implements Listener {
    public interface EventConsumer<T extends EventModifier> { void accept(ItemEvent event, T mod); }

    private final CalibrePlugin plugin;

    public EventHandle(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    private void handleEvent(ItemStack stack, EventConsumer<CalibreItem> run) {
        CalibreItem item = plugin.getItem(stack);
        if (item != null)
            item.callEvent(CalibreItem.class, run);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event) {
        plugin.removePlayerData(event.getPlayer());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (FrameworkUtils.isDropping(event.getPlayer())) return;
        handleEvent(event.getItem(), (e, i) -> i.interact(e, event));
    }

    @EventHandler
    public void onSwap(PlayerSwapHandItemsEvent event) {
        handleEvent(event.getOffHandItem(), (e, i) -> i.swap(e, event));
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event) {
        handleEvent(event.getItemDrop().getItemStack(), (e, i) -> i.drop(e, event));
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player)
            handleEvent(((Player)event.getDamager()).getInventory().getItemInMainHand(), (e, i) -> i.damage(e, event));
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        handleEvent(event.getCurrentItem(), (e, i) -> i.click(e, event));
        handleEvent(event.getCursor(), (e, i) -> i.release(e, event));
    }

    @EventHandler
    public void onSwapItem(PlayerItemHeldEvent event) {
        Player player = event.getPlayer();
        Inventory inv = player.getInventory();
        handleEvent(inv.getItem(event.getPreviousSlot()), (e, i) -> i.holster(e, player, event.getPreviousSlot()));
        handleEvent(inv.getItem(event.getNewSlot()), (e, i) -> i.draw(e, player, event.getNewSlot()));
    }

    @EventHandler
    public void onJump(PlayerJumpEvent event) {
        handleEvent(event.getPlayer().getInventory().getItemInMainHand(), (e, i) -> i.jump(e, event));
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        handleEvent(event.getPlayer().getInventory().getItemInMainHand(), (e, i) -> i.move(e, event));
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        plugin.resetPlayerData(event.getEntity());
    }
}
