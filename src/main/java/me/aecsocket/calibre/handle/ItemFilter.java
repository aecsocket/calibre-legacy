package me.aecsocket.calibre.handle;

import me.aecsocket.calibre.item.ItemDescription;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemFilter {
    public static final long EXPIRE = 75;

    private ItemDescription description;
    private long expire;

    public ItemFilter(ItemDescription description) {
        this.description = description;
        expire = System.currentTimeMillis() + EXPIRE;
    }

    public ItemDescription getDescription() { return description; }
    public void setDescription(ItemDescription description) { this.description = description; }

    public long getExpire() { return expire; }
    public void setExpire(long expire) { this.expire = expire; }

    public boolean matches(ItemStack stack, ItemMeta meta) {
        return (meta.hasCustomModelData() && meta.getCustomModelData() == description.getModelData()) &&
                (!(meta instanceof Damageable) || ((Damageable)meta).getDamage() == description.getDamage());
    }
}
