package me.aecsocket.calibre.handle;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CalibrePacketAdapter extends PacketAdapter {
    private final CalibrePlugin plugin;

    public CalibrePacketAdapter(CalibrePlugin plugin) {
        super(plugin,
                PacketType.Play.Server.SET_SLOT,
                PacketType.Play.Server.ENTITY_SOUND, PacketType.Play.Server.CUSTOM_SOUND_EFFECT, PacketType.Play.Server.NAMED_SOUND_EFFECT,
                PacketType.Play.Server.EXPLOSION, PacketType.Play.Server.TITLE);
        this.plugin = plugin;
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        PacketType packetType = event.getPacketType();

        PacketContainer packet = event.getPacket();
        Player player = event.getPlayer();

        if (packetType == PacketType.Play.Server.SET_SLOT) {
            ItemStack stack = event.getPacket().getItemModifier().read(0);
            CalibreItem item = plugin.getItem(stack);
            if (!(item instanceof CalibreComponent)) return;
            CalibreComponent component = (CalibreComponent)item;
            if (!component.isComplete()) return;

            if (
                    item.isAnimated() && component.getCompleteItem().isSimilar(stack)
                    && packet.getIntegers().read(1) - 36 == player.getInventory().getHeldItemSlot()
                    && !FrameworkUtils.isClicking(player)
            )
                event.setCancelled(true);
        }

        if (plugin.getPlayerData(player).getFlash() > 0) {
            if (
                    packetType == PacketType.Play.Server.ENTITY_SOUND
                    || packetType == PacketType.Play.Server.CUSTOM_SOUND_EFFECT
                    || packetType == PacketType.Play.Server.NAMED_SOUND_EFFECT
                    || packetType == PacketType.Play.Server.EXPLOSION
            )
                event.setCancelled(true);

            if (packetType == PacketType.Play.Server.TITLE)
                event.setCancelled(true);
        }
    }
}
