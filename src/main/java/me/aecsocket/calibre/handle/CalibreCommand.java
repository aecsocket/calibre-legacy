package me.aecsocket.calibre.handle;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.ItemCreationException;
import me.aecsocket.calibre.item.SuppliesItem;
import me.aecsocket.calibre.item.gun.ammo.AmmoComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.util.CategoryContext;
import me.aecsocket.calibre.util.PlayerData;
import me.aecsocket.calibre.util.PlayerWrapper;
import me.aecsocket.unifiedframework.registry.Identifiable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

@CommandAlias("calibre|cal")
public class CalibreCommand extends BaseCommand {
    public static final String WILDCARD = "*";

    private final CalibrePlugin plugin;

    public CalibreCommand(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    private void send(CommandSender sender, String msg, Object... args) { sender.sendMessage(plugin.gen(sender, msg, args)); }

    private String idName(CommandSender sender, Identifiable obj) {
        return obj instanceof CalibreItem
            ? ((CalibreItem)obj).getName(sender)
            : plugin.gen(sender, obj.getType() + "." + obj.getId());
    }

    @Subcommand("version")
    @CommandPermission("calibre.command.version")
    @Description("Describes the plugin version")
    public void onVersion(CommandSender sender) {
        PluginDescriptionFile desc = plugin.getDescription();
        send(sender, "chat.command.version", desc.getName(), desc.getVersion(), String.join(", ", desc.getAuthors()));
    }

    @Subcommand("reload")
    @CommandPermission("calibre.command.reload")
    @Description("Reloads all settings and data files")
    public void onReload(CommandSender sender) {
        plugin.load();
        send(sender, "chat.command.reload");
        if (plugin.getLoadErrors().size() > 0) {
            boolean show = plugin.setting("show_errors_on_reload", boolean.class, true);
            send(sender, "chat.command.reload.errors" + (show ? ".show" : ""));
            if (show) {
                plugin.getLoadErrors().forEach(error -> send(sender, "chat.command.reload.error", error));
            }
        }
    }

    @Subcommand("view-offset")
    @CommandPermission("calibre.command.view-offset")
    @Description("Displays a particle to where a specified barrel offset is")
    @CommandCompletion("<x> <y> <z>")
    public void onViewOffset(Player player, @Optional Double x, @Optional Double y, @Optional Double z) {
        PlayerData data = plugin.getPlayerData(player);
        Vector viewOffset = data.getViewOffset();
        if (viewOffset != null && (x == null || y == null || z == null)) {
            data.setViewOffset(null);
            send(player, "chat.command.view_offset.disable");
        } else if (x != null && y != null && z != null) {
            if (viewOffset == null)
                send(player, "chat.command.view_offset.enable");
            data.setViewOffset(new Vector(x, y, z));
        }
    }

    @Subcommand("show-spread")
    @CommandPermission("calibre.command.show-spread")
    @Description("Toggles a title displaying what your current spread is")
    public void onShowSpread(Player player) {
        PlayerData data = plugin.getPlayerData(player);
        data.setShowSpread(!data.showSpread());
        send(player, "chat.command.show_spread." + (data.showSpread() ? "enable" : "disable"));
    }

    @Subcommand("list")
    @CommandPermission("calibre.command.list")
    @Description("Lists all registered items in a category")
    @CommandCompletion("@categories [filter]|* [class-filter]|*")
    public void onList(CommandSender sender, CategoryContext category, @Optional String filter, @Optional String classFilter) {
        Map<String, Identifiable> results;
        results = new HashMap<>();
        category.forEach((id, obj) -> {
            if (
                    (filter == null || filter.equals(WILDCARD) || id.toLowerCase().contains(filter.toLowerCase())) &&
                    (classFilter == null || classFilter.equals(WILDCARD) || obj.getClass().getSimpleName().toLowerCase().contains(classFilter.toLowerCase()))
            )
                results.put(id, obj);
        });

        if (results.isEmpty()) {
            send(sender, "chat.command.list.empty");
            return;
        }

        for (Map.Entry<String, Identifiable> entry : results.entrySet()) {
            String id = entry.getKey();
            Identifiable obj = entry.getValue();
            send(sender, "chat.command.list.entry", id, obj.getClass().getSimpleName(), idName(sender, obj));
        }
        send(sender, "chat.command.list." + (results.size() == 1 ? "singular" : "plural"), results.size());
    }

    @Subcommand("give")
    @CommandPermission("calibre.command.give")
    @Description("Gives a player an item")
    @CommandCompletion("@players @categories @ids:i=2 [amount]")
    public void onGive(CommandSender sender, PlayerWrapper target, CategoryContext category, @Flags("k=category") SuppliesItem item, @Optional Integer amount) {
        if (target == null) {
            send(sender, "chat.error.invalid_target");
            return;
        }
        if (item == null) {
            send(sender, "chat.error.invalid_item");
            return;
        }
        Player player = target.get();
        int rAmount = amount == null ? 1 : amount;
        for (int i = 0; i < rAmount; i++) {
            try {
                player.getInventory().addItem(item.create(player, 1));
            } catch (ItemCreationException e) {
                send(sender, "chat.error.item_error", item.getId(), e.getMessage());
                return;
            }
        }
        send(sender, "chat.command.give", rAmount, item.getName(sender), player.getDisplayName());
    }

    @Subcommand("fill-ammo")
    @CommandPermission("calibre.command.fill-ammo")
    @Description("Fills the ammo component in your hand with a specified chamber component")
    @CommandCompletion("@ids:c=component [amount]")
    public void onFillAmmo(Player player, @Flags("c=component") SuppliesItem item, @Optional Integer amount) {
        if (item == null) {
            send(player, "chat.error.invalid_item");
            return;
        }
        ItemStack stack = player.getInventory().getItemInMainHand();
        CalibreItem hand = plugin.getItem(stack);
        if (!(hand instanceof AmmoComponent)) {
            send(player, "chat.error.not_ammo");
            return;
        }
        if (!(item instanceof ChamberComponent)) {
            send(player, "chat.error.not_chamber", item.getId());
            return;
        }
        AmmoComponent ammo = (AmmoComponent)hand;
        ChamberComponent chamber = (ChamberComponent)item;
        if (!ammo.isAmmoCompatible(chamber)) {
            send(player, "chat.error.ammo_not_compatible");
            return;
        }
        int lost = ammo.getCapacity() - ammo.getAmount();
        int rAmount = Math.max(0, Math.min(lost, amount == null ? lost : amount));
        for (int i = 0; i < rAmount; i++)
            ammo.getAmmo().add(chamber);
        player.getInventory().setItemInMainHand(ammo.create(player, stack.getAmount()));
        send(player, "chat.command.fill_ammo", rAmount, chamber.getName(player));
    }
}
