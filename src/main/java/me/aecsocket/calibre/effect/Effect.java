package me.aecsocket.calibre.effect;

import me.aecsocket.unifiedframework.loop.Tickable;

public interface Effect extends Tickable {
    boolean isFinished();
    default void start(WorldEffects effects) {}
}
