package me.aecsocket.calibre.effect;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;

public class FlashEffect implements Effect {
    private final CalibrePlugin plugin;
    private Location location;
    private double radius;
    private int duration;
    private ParticleData[] particle;
    private SoundData[] sound;
    private double[][] angles;

    public FlashEffect(CalibrePlugin plugin, Location location, double radius, int duration, ParticleData[] particle, SoundData[] sound, double[][] angles) {
        this.plugin = plugin;
        this.location = location;
        this.radius = radius;
        this.duration = duration;
        this.particle = particle;
        this.sound = sound;
        this.angles = angles;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public Location getLocation() { return location; }
    public void setLocation(Location location) { this.location = location; }

    public double getRadius() { return radius; }
    public void setRadius(double radius) { this.radius = radius; }

    public int getDuration() { return duration; }
    public void setDuration(int duration) { this.duration = duration; }

    public ParticleData[] getParticle() { return particle; }
    public void setParticle(ParticleData[] particle) { this.particle = particle; }

    public SoundData[] getSound() { return sound; }
    public void setSound(SoundData[] sound) { this.sound = sound; }

    public double[][] getAngles() { return angles; }
    public void setAngles(double[][] angles) { this.angles = angles; }

    @Override
    public boolean isFinished() { return true; }

    @Override
    public void start(WorldEffects effects) {
        ParticleData.spawn(particle, location);
        SoundData.play(sound, location);
        location.getNearbyPlayers(radius).forEach(player -> {
            Location eye = player.getEyeLocation();
            double distance = eye.distance(location);
            if (distance > radius)
                return;
            if (!FrameworkUtils.canSee(location, eye, e -> e != player, b -> b.getType().isOccluding(), 4))
                return;
            double angle = Math.abs(FrameworkUtils.getYawAngle(Location.normalizeYaw(eye.getYaw()), eye.toVector(), location.toVector()));
            double percent = 1 - (distance / radius);
            for (double[] effect : angles) {
                if (effect.length < 2) continue;
                if (angle < effect[0] || effect[0] == -1) {
                    percent *= effect[1];
                    break;
                }
            }
            plugin.getPlayerData(player).flash((int)(duration * percent));
        });
    }

    @Override
    public void tick(TickContext tickContext) {}
}
