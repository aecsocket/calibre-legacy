package me.aecsocket.calibre.effect;

import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;

public class FireEffect extends DamageEffect {
    private int fireTicks;

    public FireEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration, int elapsed, double radius, double damage, PotionEffect[] effects, Entity damager, int damageDelay, EntityDamageEvent.DamageCause cause, boolean penetrates, int fireTicks) {
        super(particle, sound, startParticle, startSound, location, duration, elapsed, radius, damage, effects, damager, damageDelay, cause, penetrates);
        this.fireTicks = fireTicks;
    }

    public FireEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration, double radius, double damage, PotionEffect[] effects, Entity damager, int damageDelay, EntityDamageEvent.DamageCause cause, boolean penetrates, int fireTicks) {
        super(particle, sound, startParticle, startSound, location, duration, radius, damage, effects, damager, damageDelay, cause, penetrates);
        this.fireTicks = fireTicks;
    }

    public int getFireTicks() { return fireTicks; }
    public void setFireTicks(int fireTicks) { this.fireTicks = fireTicks; }

    @Override
    protected void damage(LivingEntity entity) {
        super.damage(entity);
        entity.setFireTicks(entity.getFireTicks() + fireTicks);
    }
}
