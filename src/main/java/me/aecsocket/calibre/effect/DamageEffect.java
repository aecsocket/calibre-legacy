package me.aecsocket.calibre.effect;

import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;

public class DamageEffect extends GenericEffect {
    private double radius;
    private double damage;
    private PotionEffect[] effects;
    private Entity damager;
    private int damageDelay;
    private EntityDamageEvent.DamageCause cause;
    private boolean penetrates;

    public DamageEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration, int elapsed, double radius, double damage, PotionEffect[] effects, Entity damager, int damageDelay, EntityDamageEvent.DamageCause cause, boolean penetrates) {
        super(particle, sound, startParticle, startSound, location, duration, elapsed);
        this.radius = radius;
        this.damage = damage;
        this.effects = effects;
        this.damager = damager;
        this.damageDelay = damageDelay;
        this.cause = cause;
        this.penetrates = penetrates;
    }

    public DamageEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration, double radius, double damage, PotionEffect[] effects, Entity damager, int damageDelay, EntityDamageEvent.DamageCause cause, boolean penetrates) {
        super(particle, sound, startParticle, startSound, location, duration);
        this.radius = radius;
        this.damage = damage;
        this.effects = effects;
        this.damager = damager;
        this.damageDelay = damageDelay;
        this.cause = cause;
        this.penetrates = penetrates;
    }

    public double getRadius() { return radius; }
    public void setRadius(double radius) { this.radius = radius; }

    public double getDamage() { return damage; }
    public void setDamage(double damage) { this.damage = damage; }

    public Entity getDamager() { return damager; }
    public void setDamager(Entity damager) { this.damager = damager; }

    public PotionEffect[] getEffects() { return effects; }
    public void setEffects(PotionEffect[] effects) { this.effects = effects; }

    public int getDamageDelay() { return damageDelay; }
    public void setDamageDelay(int damageDelay) { this.damageDelay = damageDelay; }

    public EntityDamageEvent.DamageCause getCause() { return cause; }
    public void setCause(EntityDamageEvent.DamageCause cause) { this.cause = cause; }

    public boolean penetrates() { return penetrates; }
    public void setPenetrates(boolean penetrates) { this.penetrates = penetrates; }

    protected void damage(LivingEntity entity) {
        if (damage > 0) {
            CalibreUtils.damage(entity, null, damage, damager, cause);
            entity.setNoDamageTicks(0);
        }
        if (effects != null) {
            for (PotionEffect effect : effects)
                entity.addPotionEffect(effect);
        }
    }

    @Override
    public void tick(TickContext tickContext) {
        super.tick(tickContext);
        if (!tickContext.is(damageDelay)) return;
        getLocation().getNearbyLivingEntities(radius).forEach(entity -> {
            if (entity.isDead()) return;
            if (!penetrates && !FrameworkUtils.canSee(getLocation().clone().add(0, 0.01, 0), entity.getLocation(), e -> e != entity)) return;
            damage(entity);
        });
    }
}
