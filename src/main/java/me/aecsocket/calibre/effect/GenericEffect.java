package me.aecsocket.calibre.effect;

import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;

public class GenericEffect implements Effect {
    private ParticleData[] particle;
    private SoundData[] sound;
    private ParticleData[] startParticle;
    private SoundData[] startSound;
    private Location location;
    private int duration;
    private int elapsed;

    public GenericEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration, int elapsed) {
        this.particle = particle;
        this.sound = sound;
        this.startParticle = startParticle;
        this.startSound = startSound;
        this.location = location;
        this.duration = duration;
        this.elapsed = elapsed;
    }

    public GenericEffect(ParticleData[] particle, SoundData[] sound, ParticleData[] startParticle, SoundData[] startSound, Location location, int duration) {
        this.particle = particle;
        this.sound = sound;
        this.startParticle = startParticle;
        this.startSound = startSound;
        this.location = location;
        this.duration = duration;
    }

    public ParticleData[] getParticle() { return particle; }
    public void setParticle(ParticleData... particle) { this.particle = particle; }

    public SoundData[] getSound() { return sound; }
    public void setSound(SoundData[] sound) { this.sound = sound; }

    public ParticleData[] getStartParticle() { return startParticle; }
    public void setStartParticle(ParticleData[] startParticle) { this.startParticle = startParticle; }

    public SoundData[] getStartSound() { return startSound; }
    public void setStartSound(SoundData[] startSound) { this.startSound = startSound; }

    public Location getLocation() { return location; }
    public void setLocation(Location location) { this.location = location; }

    public int getDuration() { return duration; }
    public void setDuration(int duration) { this.duration = duration; }

    public int getElapsed() { return elapsed; }
    public void setElapsed(int elapsed) { this.elapsed = elapsed; }

    @Override
    public boolean isFinished() { return elapsed >= duration; }

    @Override
    public void start(WorldEffects effects) {
        ParticleData.spawn(startParticle, location);
        SoundData.play(startSound, location);
    }

    @Override
    public void tick(TickContext tickContext) {
        ++elapsed;
        ParticleData.spawn(particle, location);
        SoundData.play(sound, location);
    }
}
