package me.aecsocket.calibre.effect;

import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WorldEffects implements Tickable {
    private final List<Effect> effects = new ArrayList<>();

    public List<Effect> getEffects() { return effects; }
    public <T extends Effect> T addEffect(T effect) {
        effect.start(this);
        effects.add(effect);
        return effect;
    }

    @Override
    public void tick(TickContext tickContext) {
        Iterator<Effect> iter = effects.iterator();
        while (iter.hasNext()) {
            Effect effect = iter.next();
            tickContext.tick(effect);
            if (effect.isFinished())
                iter.remove();
        }
    }
}
