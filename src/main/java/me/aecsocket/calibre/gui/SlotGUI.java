package me.aecsocket.calibre.gui;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.gui.BaseGUI;
import me.aecsocket.unifiedframework.gui.GUIManager;
import me.aecsocket.unifiedframework.gui.core.GUIItem;
import me.aecsocket.unifiedframework.gui.core.GUIVector;
import me.aecsocket.unifiedframework.gui.core.GUIView;
import me.aecsocket.unifiedframework.utils.SizeType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.Map;

public class SlotGUI extends BaseGUI {
    public static final SizeType SIZE = new SizeType(6);

    private final CalibrePlugin plugin;
    private CalibreItem base;
    private int clickedSlot;
    private boolean canModify;

    public SlotGUI(CalibrePlugin plugin, CalibreItem base, int clickedSlot, boolean canModify) {
        this.plugin = plugin;
        this.base = base;
        this.clickedSlot = clickedSlot;
        this.canModify = canModify;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public CalibreItem getBase() { return base; }
    public void setBase(CalibreItem base) { this.base = base; }

    public int getClickedSlot() { return clickedSlot; }
    public void setClickedSlot(int clickedSlot) { this.clickedSlot = clickedSlot; }

    public boolean canModify() { return canModify; }
    public void setCanModify(boolean canModify) { this.canModify = canModify; }

    @Override protected GUIManager getGUIManager() { return plugin.getUnifiedFramework().getGUIManager(); }
    @Override public String getTitle(Player player) { return plugin.gen(player, "gui.slot.title"); }
    @Override public SizeType getSizeType(Player player) { return SIZE; }

    protected SlotGUIItem createItem(String slotName, CalibreComponentSlot slot) {
        return new SlotGUIItem(plugin, slotName, slot, canModify);
    }

    private void walk(Map<Integer, GUIItem> slots, GUIVector base, ComponentHolder holder) {
        if (holder.getSlots() == null)
            return;
        holder.getSlots().forEach((path, raw) -> {
            if (!(raw instanceof CalibreComponentSlot)) return;
            CalibreComponentSlot slot = (CalibreComponentSlot)raw;
            GUIVector vec = base.clone();
            if (slot.getViewOffset() != null)
                vec.add(slot.getViewOffset());
            slots.put(vec.getSlot(), createItem(path, slot));
            if (slot.get() != null)
                walk(slots, vec, slot.get());
        });
    }

    @Override
    public Map<Integer, GUIItem> getSlots(Player player) {
        Map<Integer, GUIItem> slots = new HashMap<>();
        GUIVector center = plugin.setting("gui.center", GUIVector.class, new GUIVector(4, 3));
        slots.put(center.clone().getSlot(), v -> base.createComponentIcon(v.getPlayer(), 1));
        walk(slots, center.clone(), base);
        return slots;
    }

    @Override
    public void onClick(GUIView view, InventoryClickEvent event) {
        if (
                event.getClickedInventory() == event.getView().getBottomInventory() &&
                        (event.getSlot() == clickedSlot) ||
                        (event.getClick() == ClickType.NUMBER_KEY && event.getHotbarButton() == clickedSlot)
        ) {
            event.setCancelled(true);
            return;
        }
        super.onClick(view, event);
    }

    @Override
    public void onClose(GUIView view, InventoryCloseEvent event) {
        Player player = (Player)event.getPlayer();
        PlayerInventory inv = player.getInventory();
        ItemStack stack = inv.getItem(clickedSlot);
        if (inv.getHeldItemSlot() == clickedSlot)
            base.updateItem(player, stack);
    }
}
