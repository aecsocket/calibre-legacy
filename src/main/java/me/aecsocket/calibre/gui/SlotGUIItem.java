package me.aecsocket.calibre.gui;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import me.aecsocket.unifiedframework.gui.core.GUIItem;
import me.aecsocket.unifiedframework.gui.core.GUIView;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class SlotGUIItem implements GUIItem {
    private final CalibrePlugin plugin;
    private String slotName;
    private CalibreComponentSlot slot;
    private boolean canModify;

    public SlotGUIItem(CalibrePlugin plugin, String slotName, CalibreComponentSlot slot, boolean canModify) {
        this.plugin = plugin;
        this.slotName = slotName;
        this.slot = slot;
        this.canModify = canModify;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public String getSlotName() { return slotName; }
    public void setSlotName(String slotName) { this.slotName = slotName; }

    public CalibreComponentSlot getSlot() { return slot; }
    public void setSlot(CalibreComponentSlot slot) { this.slot = slot; }

    public boolean canModify() { return canModify; }
    public void setCanModify(boolean canModify) { this.canModify = canModify; }

    @Override
    public ItemStack getHandle(GUIView view) {
        Player player = view.getPlayer();
        CalibreComponent component = slot.get();
        if (component == null) return slot.createIcon(player, slotName);
        ItemStack item = component.createComponentIcon(player, 1);
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();
        lore.add("");
        lore.add(plugin.gen(player, "gui.slot.text", slot.getName(player, slotName)));
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    private void error(Player player, String msg, Object... args) {
        player.sendMessage(plugin.gen(player, msg, args));
    }

    private void update(GUIView view) {
        SlotGUI gui = view.getGUI() instanceof SlotGUI ? (SlotGUI)view.getGUI() : null;
        if (gui == null) return;

        Player player = view.getPlayer();
        PlayerInventory inv = player.getInventory();
        int slot = gui.getClickedSlot();
        ItemStack stack = inv.getItem(slot);
        InventoryView handle = view.getHandle();

        CalibreItem base = gui.getBase();

        ItemStack cursor = handle.getCursor();
        handle.setCursor(null);
        gui.open(player);
        handle.setCursor(cursor);

        if (stack != null) {
            inv.setItem(slot, base.create(player, stack.getAmount()));
            if (inv.getHeldItemSlot() == slot)
                base.updateItem(player, stack);
        }
    }

    @Override
    public void onClick(GUIView view, InventoryClickEvent event) {
        Player player = view.getPlayer();
        if (!canModify) return;
        if (!plugin.getPlayerData(player).isAvailable()) return;
        if (!slot.canFieldModify() && !plugin.setting("field_modify.global_enable", boolean.class, false)) {
            error(player, "chat.error.cannot_field_modify_slot");
            return;
        }
        CalibreComponent component = slot.get();
        InventoryView handle = event.getView();

        ItemStack cursor = event.getCursor();
        if (cursor.getType() == Material.AIR) {
            if (component == null) return;
            event.setCancelled(false);

            slot.set(null);
            handle.setCursor(component.create(player, 1));
            SoundData.play(plugin.setting("field_modify.remove_sound", SoundData[].class, null), player, player.getLocation());
        } else {
            if (component != null) return;
            CalibreItem item = plugin.getItem(cursor);
            if (!(item instanceof CalibreComponent)) return;
            CalibreComponent replComponent = (CalibreComponent)item;
            if (!slot.isCompatible(replComponent)) {
                error(player, "chat.error.not_compatible");
                return;
            }
            event.setCancelled(false);

            slot.set(replComponent);
            handle.setCursor(cursor.subtract());
            SoundData.play(plugin.setting("field_modify.combine_sound", SoundData[].class, null), player, player.getLocation());
        }
        update(view);
    }

    @Override
    public void onMoveToOtherInventory(GUIView view, InventoryClickEvent event) {
        event.setCancelled(false);
    }
}
