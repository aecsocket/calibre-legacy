package me.aecsocket.calibre.item.data;

import com.google.gson.reflect.TypeToken;
import me.aecsocket.unifiedframework.stat.Operation;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListStat<T> extends CalibreStat<List<T>> {
    public static final Operation ADD = () -> "+";
    public static final Operation SET = () -> "=";

    public static final Map<String, Operation> OPERATION_MAP = Collections.unmodifiableMap(Stream.of(
            ADD, SET
    ).collect(Collectors.toMap(Operation::getSymbol, o -> o)));

    public ListStat(TypeToken<?> type) { super(type.getType()); }

    @Override public Map<String, Operation> getOperationMap() { return OPERATION_MAP; }

    @Override
    public List<T> combine(List<T> base, List<T> other, @Nullable Operation op) {
        Optional<List<T>> result = preCombine(base, other);
        if (result != null) return result.map(ArrayList::new).orElse(null);
        if (op == SET) return other;
        ArrayList<T> copy = new ArrayList<>(base);
        copy.addAll(other);
        return copy;
    }
}
