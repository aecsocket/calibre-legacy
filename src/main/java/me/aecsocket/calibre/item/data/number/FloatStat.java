package me.aecsocket.calibre.item.data.number;

public class FloatStat extends DecimalStat<Float> {
    public FloatStat() { super(float.class); }
    @Override protected Float toType(Number number) { return number.floatValue(); }
}
