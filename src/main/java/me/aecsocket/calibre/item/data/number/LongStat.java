package me.aecsocket.calibre.item.data.number;

public class LongStat extends NumberStat<Long> {
    public LongStat() { super(long.class); }
    @Override protected Long toType(Number number) { return number.longValue(); }
}
