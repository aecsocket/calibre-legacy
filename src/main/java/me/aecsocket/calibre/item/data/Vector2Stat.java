package me.aecsocket.calibre.item.data;

import me.aecsocket.unifiedframework.stat.Operation;
import me.aecsocket.unifiedframework.utils.Vector2;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Vector2Stat extends CalibreStat<Vector2> {
    public static final Operation ADD = () -> "+";
    public static final Operation SET = () -> "=";
    public static final Operation SUBTRACT = () -> "-";
    public static final Operation MULTIPLY = () -> "*";
    public static final Operation DIVIDE = () -> "/";

    public static final Map<String, Operation> OPERATION_MAP = Collections.unmodifiableMap(Stream.of(
            ADD, SET, SUBTRACT, MULTIPLY, DIVIDE
    ).collect(Collectors.toMap(Operation::getSymbol, o -> o)));

    public Vector2Stat() { super(Vector2.class); }

    @Override
    public Vector2 combine(Vector2 base, Vector2 other, @Nullable Operation op) {
        Optional<Vector2> result = preCombine(base, other);
        if (result != null) return result.map(Vector2::clone).orElse(null);
        if (op == SET) return other;
        if (op == SUBTRACT) return base.clone().subtract(other);
        if (op == MULTIPLY) return base.clone().multiply(other);
        if (op == DIVIDE) return base.clone().divide(other);
        return base.clone().add(other);
    }

    @Override public Map<String, Operation> getOperationMap() { return OPERATION_MAP; }
}
