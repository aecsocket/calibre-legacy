package me.aecsocket.calibre.item.data.number;

import me.aecsocket.calibre.item.data.CalibreStat;
import me.aecsocket.unifiedframework.stat.Operation;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class NumberStat<T extends Number> extends CalibreStat<T> {
    public static final Operation ADD = () -> "+";
    public static final Operation SET = () -> "=";
    public static final Operation SUBTRACT = () -> "-";
    public static final Operation MULTIPLY = () -> "*";
    public static final Operation DIVIDE = () -> "/";

    public static final Map<String, Operation> OPERATION_MAP = Collections.unmodifiableMap(Stream.of(
            ADD, SET, SUBTRACT, MULTIPLY, DIVIDE
    ).collect(Collectors.toMap(Operation::getSymbol, o -> o)));

    public NumberStat(Type type) { super(type); }

    @Override public Map<String, Operation> getOperationMap() { return OPERATION_MAP; }

    protected abstract T toType(Number number);

    @Override
    public T combine(T base, T other, @Nullable Operation op) {
        Optional<T> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        return toType(modify(base, other, op));
    }

    public static Number modify(Number base, Number other, @Nullable Operation op) {
        double d1 = base.doubleValue();
        double d2 = other.doubleValue();
        if (op == SET) return d2;
        if (op == SUBTRACT) return d1 - d2;
        if (op == MULTIPLY) return d1 * d2;
        if (op == DIVIDE) return d1 / d2;
        return d1 + d2;
    }
}
