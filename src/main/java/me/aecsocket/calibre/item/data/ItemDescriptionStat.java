package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.item.ItemDescription;
import me.aecsocket.unifiedframework.stat.Operation;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class ItemDescriptionStat extends CalibreStat<ItemDescription> {
    public ItemDescriptionStat() { super(ItemDescription.class); }

    @Override public Map<String, Operation> getOperationMap() { return Collections.emptyMap(); }

    @Override
    public ItemDescription combine(ItemDescription base, ItemDescription other, @Nullable Operation op) {
        Optional<ItemDescription> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        return other;
    }
}
