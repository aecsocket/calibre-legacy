package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.stat.Operation;
import me.aecsocket.unifiedframework.stat.StatInstance;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class EnumStat extends CalibreStat<Enum<?>> {
    public EnumStat(Class<? extends Enum<?>> type) { super(type); }

    @Override public Map<String, Operation> getOperationMap() { return Collections.emptyMap(); }

    @Override
    public Enum<?> combine(Enum<?> base, Enum<?> other, @Nullable Operation op) {
        Optional<Enum<?>> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        return other;
    }

    @Override
    public String getText(CalibrePlugin plugin, String name, StatInstance<Enum<?>> inst, Player player, Enum<?> value) { return value.name(); }
}
