package me.aecsocket.calibre.item.data;

import me.aecsocket.unifiedframework.utils.ParticleData;

public class ParticleDataStat extends ArrayStat<ParticleData> {
    public ParticleDataStat() { super(ParticleData[].class, ParticleData.class); }
}
