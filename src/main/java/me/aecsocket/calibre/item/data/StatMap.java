package me.aecsocket.calibre.item.data;

import me.aecsocket.unifiedframework.stat.Stat;

import java.util.LinkedHashMap;
import java.util.Map;

public class StatMap extends LinkedHashMap<String, Stat<?>> {
    public StatMap(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
    public StatMap(int initialCapacity) { super(initialCapacity); }
    public StatMap(Map<? extends String, ? extends Stat<?>> m) { super(m); }
    public StatMap(int initialCapacity, float loadFactor, boolean accessOrder) { super(initialCapacity, loadFactor, accessOrder); }

    public StatMap(StatMap... bases) {
        for (StatMap base : bases)
            putAll(base);
    }

    public StatMap add(String name, Stat<?> stat) {
        put(name, stat);
        return this;
    }
}
