package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.stat.Stat;
import me.aecsocket.unifiedframework.stat.StatInstance;
import org.bukkit.entity.Player;

import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public abstract class CalibreStat<T> implements Stat<T> {
    public interface TextSupplier<T> { String get(CalibreStat<T> stat, CalibrePlugin plugin, String name, StatInstance<T> inst, Player player, T value); }

    private final Type type;
    private Function<StatInstance<T>, T> defaultSupplier;
    private TextSupplier<T> textSupplier;
    private boolean hidden;

    public CalibreStat(Type type) { this.type = type; }

    @Override
    public Type getType() { return type; }

    @Override public Function<StatInstance<T>, T> getDefaultSupplier() { return defaultSupplier; }
    public CalibreStat<T> defaultSupplier(Function<StatInstance<T>, T> defaultSupplier) { this.defaultSupplier = defaultSupplier; return this; }

    public TextSupplier<T> getTextSupplier() { return textSupplier; }
    public CalibreStat<T> textSupplier(TextSupplier<T> textSupplier) { this.textSupplier = textSupplier; return this; }

    public boolean isHidden() { return hidden; }
    public CalibreStat<T> hide() { hidden = true; return this; }
    public CalibreStat<T> show() { hidden = false; return this; }

    protected boolean shouldShow(CalibrePlugin plugin, String name) {
        Optional<Boolean> cfgShow = plugin.optSetting("stats." + name, boolean.class);
        if (cfgShow.isPresent() && !cfgShow.get()) return false;
        return !hidden || cfgShow.isPresent();
    }

    public String getText(CalibrePlugin plugin, String name, StatInstance<T> inst, Player player, T value) {
        if (!shouldShow(plugin, name)) return null;
        return plugin.gen(player, "stat",
                plugin.gen(player, "stat." + name,
                textSupplier == null ? getDefaultText(plugin, name, inst, player, value) : textSupplier.get(this, plugin, name, inst, player, value))
        );
    }

    protected String getDefaultText(CalibrePlugin plugin, String name, StatInstance<T> inst, Player player, T value) { return value.toString(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalibreStat<?> that = (CalibreStat<?>)o;
        return hidden == that.hidden &&
                type.equals(that.type) &&
                Objects.equals(defaultSupplier, that.defaultSupplier) &&
                Objects.equals(textSupplier, that.textSupplier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, defaultSupplier, textSupplier, hidden);
    }
}
