package me.aecsocket.calibre.item.data.number;

public class DoubleStat extends DecimalStat<Double> {
    public DoubleStat() { super(double.class); }
    @Override protected Double toType(Number number) { return number.doubleValue(); }
}
