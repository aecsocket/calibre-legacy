package me.aecsocket.calibre.item.data;

import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.item.animation.Animation;
import me.aecsocket.unifiedframework.stat.Operation;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AnimationStat extends CalibreStat<Map<String, Animation>> {
    public AnimationStat() { super(new TypeToken<Map<String, Animation>>(){}.getType()); }

    @Override
    public Map<String, Animation> combine(Map<String, Animation> base, Map<String, Animation> other, @Nullable Operation op) {
        Optional<Map<String, Animation>> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        Map<String, Animation> map = new HashMap<>(base);
        map.putAll(other);
        return map;
    }

    @Override
    public Map<String, Operation> getOperationMap() { return Collections.emptyMap(); }
}
