package me.aecsocket.calibre.item.data;

import me.aecsocket.unifiedframework.stat.Operation;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArrayStat<T> extends CalibreStat<T[]> {
    public static final Operation ADD = () -> "+";
    public static final Operation SET = () -> "=";

    public static final Map<String, Operation> OPERATION_MAP = Collections.unmodifiableMap(Stream.of(
            ADD, SET
    ).collect(Collectors.toMap(Operation::getSymbol, o -> o)));

    private final Class<T> elementType;

    public ArrayStat(Class<T[]> arrayType, Class<T> elementType) {
        super(arrayType);
        this.elementType = elementType;
    }

    public Class<T> getElementType() { return elementType; }

    @Override public Map<String, Operation> getOperationMap() { return OPERATION_MAP; }

    @Override
    public T[] combine(T[] base, T[] other, @Nullable Operation op) {
        Optional<T[]> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        if (op == SET) return other;
        @SuppressWarnings("unchecked")
        T[] copy = (T[])Array.newInstance(elementType, base.length + other.length);
        System.arraycopy(base, 0, copy, 0, base.length);
        System.arraycopy(other, 0, copy, base.length, other.length);
        return copy;
    }
}
