package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.number.FloatStat;
import me.aecsocket.calibre.item.data.number.IntegerStat;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;

public final class Stats {
    private Stats() {}

    public static final StatMap EXPLOSIVE = new StatMap()
            .add("explosion_power", new FloatStat().precision(1).defaultSupplier(i -> 0f))
            .add("explosion_creates_fire", new BooleanStat().defaultSupplier(i -> false))
            .add("explosion_breaks_blocks", new BooleanStat().defaultSupplier(i -> false));
    public static final StatMap EFFECT = new StatMap()
            .add("particle", new ParticleDataStat().hide())
            .add("sound", new SoundDataStat().hide())
            .add("start_particle", new ParticleDataStat().hide())
            .add("start_sound", new SoundDataStat().hide())
            .add("duration", new IntegerStat());
    public static final StatMap DAMAGE = new StatMap(EFFECT)
            .add("radius", new DoubleStat().defaultSupplier(i -> 0d))
            .add("damage", new DoubleStat().defaultSupplier(i -> 0d))
            .add("effects", new ArrayStat<>(PotionEffect[].class, PotionEffect.class).hide())
            .add("damage_delay", new IntegerStat().defaultSupplier(i -> 1).hide())
            .add("damage_cause", new EnumStat(EntityDamageEvent.DamageCause.class).defaultSupplier(i -> EntityDamageEvent.DamageCause.ENTITY_ATTACK).hide())
            .add("damage_penetrates", new BooleanStat().defaultSupplier(i -> false));
    public static final StatMap FIRE = new StatMap(DAMAGE)
            .add("fire_ticks", new IntegerStat().defaultSupplier(i -> 0));
    public static final StatMap FLASH = new StatMap()
            .add("radius", new DoubleStat().defaultSupplier(i -> 0d))
            .add("duration", new IntegerStat().defaultSupplier(i -> 0))
            .add("particle", new ParticleDataStat().hide())
            .add("sound", new SoundDataStat().hide())
            .add("angles", new ArrayStat<>(double[][].class, double[].class).hide());
}
