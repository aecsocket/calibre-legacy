package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.util.CalibreSoundData;

public class SoundDataStat extends ArrayStat<CalibreSoundData> {
    public SoundDataStat() { super(CalibreSoundData[].class, CalibreSoundData.class); }
}
