package me.aecsocket.calibre.item.data;

import me.aecsocket.unifiedframework.stat.Operation;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VectorStat extends CalibreStat<Vector> {
    public static final Operation ADD = () -> "+";
    public static final Operation SET = () -> "=";
    public static final Operation SUBTRACT = () -> "-";
    public static final Operation MULTIPLY = () -> "*";
    public static final Operation DIVIDE = () -> "/";

    public static final Map<String, Operation> OPERATION_MAP = Collections.unmodifiableMap(Stream.of(
            ADD, SET, SUBTRACT, MULTIPLY, DIVIDE
    ).collect(Collectors.toMap(Operation::getSymbol, o -> o)));

    public VectorStat() { super(Vector.class); }

    @Override
    public Vector combine(Vector base, Vector other, @Nullable Operation op) {
        Optional<Vector> result = preCombine(base, other);
        if (result != null) return result.map(Vector::clone).orElse(null);
        if (op == SET) return other.clone();
        if (op == SUBTRACT) return base.clone().subtract(other);
        if (op == MULTIPLY) return base.clone().multiply(other);
        if (op == DIVIDE) return base.clone().divide(other);
        return base.clone().add(other);
    }

    @Override public Map<String, Operation> getOperationMap() { return OPERATION_MAP; }
}
