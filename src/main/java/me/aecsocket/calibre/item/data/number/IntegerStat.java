package me.aecsocket.calibre.item.data.number;

public class IntegerStat extends NumberStat<Integer> {
    public IntegerStat() { super(int.class); }
    @Override protected Integer toType(Number number) { return number.intValue(); }
}
