package me.aecsocket.calibre.item.data.number;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.stat.StatInstance;
import org.bukkit.entity.Player;

import java.lang.reflect.Type;
import java.text.DecimalFormat;

public abstract class DecimalStat<T extends Number> extends NumberStat<T> {
    public static final DecimalFormat DEFAULT_FORMAT = new DecimalFormat("0.0");

    private DecimalFormat format = DEFAULT_FORMAT;

    public DecimalStat(Type type) { super(type); }

    public DecimalFormat getFormat() { return format; }
    public DecimalStat<T> format(DecimalFormat format) { this.format = format; return this; }
    public DecimalStat<T> precision(int precision) { format = new DecimalFormat("0" + (precision > 0 ? "." + "0".repeat(precision) : "")); return this; }

    public String format(Object obj) { return format.format(obj); }

    @Override
    protected String getDefaultText(CalibrePlugin plugin, String name, StatInstance<T> inst, Player player, T value) {
        return format == null ? super.getDefaultText(plugin, name, inst, player, value) : format.format(value);
    }
}
