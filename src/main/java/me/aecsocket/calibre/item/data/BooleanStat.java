package me.aecsocket.calibre.item.data;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.stat.Operation;
import me.aecsocket.unifiedframework.stat.StatInstance;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class BooleanStat extends CalibreStat<Boolean> {
    public BooleanStat() { super(boolean.class); }

    @Override public Map<String, Operation> getOperationMap() { return Collections.emptyMap(); }

    @Override
    public Boolean combine(Boolean base, Boolean other, @Nullable Operation op) {
        Optional<Boolean> result = preCombine(base, other);
        if (result != null) return result.orElse(null);
        return other;
    }

    @Override
    public String getText(CalibrePlugin plugin, String name, StatInstance<Boolean> inst, Player player, Boolean value) {
        if (!value) return null;
        return super.getText(plugin, name, inst, player, value);
    }
}
