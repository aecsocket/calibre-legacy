package me.aecsocket.calibre.item;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class ItemDescription {
    private Material material;
    private int modelData;
    private int damage;
    private boolean animated;

    public Material getMaterial() { return material; }
    public void setMaterial(Material material) { this.material = material; }

    public int getModelData() { return modelData; }
    public void setModelData(int modelData) { this.modelData = modelData; }

    public int getDamage() { return damage; }
    public void setDamage(int damage) { this.damage = damage; }

    public boolean isAnimated() { return animated; }
    public void setAnimated(boolean animated) { this.animated = animated; }

    public ItemStack create(int amount) {
        ItemStack item = new ItemStack(material, amount);
        apply(item);
        return item;
    }


    public void apply(ItemStack item, ItemMeta meta) {
        item.setType(material);
        meta.setCustomModelData(modelData);
        if (meta instanceof Damageable)
            ((Damageable)meta).setDamage(damage);
    }

    public void apply(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        apply(item, meta);
        item.setItemMeta(meta);
    }

    public boolean isSimilar(ItemStack stack) {
        if (stack == null || !stack.hasItemMeta()) return false;
        ItemMeta meta = stack.getItemMeta();
        return (meta.hasCustomModelData() && meta.getCustomModelData() == modelData) &&
                (!(meta instanceof Damageable) || ((Damageable)meta).getDamage() == damage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemDescription that = (ItemDescription)o;
        return modelData == that.modelData &&
                damage == that.damage &&
                animated == that.animated &&
                material == that.material;
    }

    @Override
    public int hashCode() {
        return Objects.hash(material, modelData, damage, animated);
    }
}
