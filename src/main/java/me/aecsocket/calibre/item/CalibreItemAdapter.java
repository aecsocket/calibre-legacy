package me.aecsocket.calibre.item;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.CalibreSlotMap;
import me.aecsocket.unifiedframework.item.core.ItemAdapter;
import me.aecsocket.unifiedframework.registry.Registry;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public abstract class CalibreItemAdapter<T extends CalibreItem> implements ItemAdapter<T> {
    private final CalibrePlugin plugin;

    public CalibreItemAdapter(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    protected abstract String getItemType();
    protected abstract String getRegistryType();
    protected void modify(PersistentDataContainer data, ItemStack stack, T item) {}

    protected NamespacedKey key(String name) { return plugin.key(name); }
    protected <T, Z> Z get(PersistentDataContainer data, String name, PersistentDataType<T, Z> type, Z defaultValue) { return data.getOrDefault(key(name), type, defaultValue); }
    protected T base(PersistentDataContainer data) {
        Registry registry = plugin.getRegistry();
        String id = id(data);
        return registry.has(getRegistryType(), id) ? registry.get(getRegistryType(), id) : null;
    }
    protected String id(PersistentDataContainer data) { return data.has(key("id"), PersistentDataType.STRING) ? get(data, "id", PersistentDataType.STRING, null) : null; }
    protected CalibreSlotMap slots(PersistentDataContainer data, CalibreItem base) {
        if (!data.has(key("slots"), PersistentDataType.STRING))
            return null;
        plugin.getSlotMapAdapter().setBase(base);
        return plugin.getGson().fromJson(get(data, "slots", PersistentDataType.STRING, null), CalibreSlotMap.class);
    }

    @Override
    public String getType() { return getItemType(); }
    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(ItemStack item) {
        PersistentDataContainer data = item.getItemMeta().getPersistentDataContainer();
        T base = base(data);
        if (base == null) return null;
        base = (T)base.copy();
        base.setSlots(slots(data, base));
        modify(data, item, base);
        return base;
    }
}
