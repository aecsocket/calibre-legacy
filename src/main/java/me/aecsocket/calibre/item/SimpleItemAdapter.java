package me.aecsocket.calibre.item;

import me.aecsocket.calibre.CalibrePlugin;

public abstract class SimpleItemAdapter<T extends CalibreItem> extends CalibreItemAdapter<T> {
    private String itemType;
    private String registryType;

    public SimpleItemAdapter(CalibrePlugin plugin, String itemType, String registryType) {
        super(plugin);
        this.itemType = itemType;
        this.registryType = registryType;
    }

    @Override
    public String getItemType() { return itemType; }
    public void setItemType(String itemType) { this.itemType = itemType; }

    @Override
    public String getRegistryType() { return registryType; }
    public void setRegistryType(String registryType) { this.registryType = registryType; }
}
