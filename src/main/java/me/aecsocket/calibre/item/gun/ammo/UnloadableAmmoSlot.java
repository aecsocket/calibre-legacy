package me.aecsocket.calibre.item.gun.ammo;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import org.bukkit.entity.Player;

public class UnloadableAmmoSlot extends AmmoSlot {
    @Override
    public void reload(ItemEvent base, Player player, GunComponent gun) {
        if (get() == null)
            return;
        super.reload(base, player, gun);
    }
}
