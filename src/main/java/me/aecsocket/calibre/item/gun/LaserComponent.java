package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.ParticleData;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class LaserComponent extends GunComponent {
    private ParticleData[] laserParticle;
    private ParticleData[] hitParticle;
    private double spread;
    private int distance;
    private Vector defaultOffset;

    public ParticleData[] getLaserParticle() { return laserParticle; }
    public void setLaserParticle(ParticleData[] laserParticle) { this.laserParticle = laserParticle; }

    public ParticleData[] getHitParticle() { return hitParticle; }
    public void setHitParticle(ParticleData[] hitParticle) { this.hitParticle = hitParticle; }

    public double getSpread() { return spread; }
    public void setSpread(double spread) { this.spread = spread; }

    public int getDistance() { return distance; }
    public void setDistance(int distance) { this.distance = distance; }

    public Vector getDefaultOffset() { return defaultOffset; }
    public void setDefaultOffset(Vector defaultOffset) { this.defaultOffset = defaultOffset; }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (!(tickContext.getLoop() instanceof SchedulerLoop)) return;
        if (getPlugin().getPlayerData(player).actionIs(
                CalibreItem.DRAW_ACTION, GunComponent.DOWN_ACTION, GunComponent.UP_ACTION,
                GunComponent.LOAD_ACTION, GunComponent.UNLOAD_ACTION, GunComponent.LOAD_SINGLE_ACTION))
            return;
        Location loc = base.getStats().getStat("barrel_offset").getRawValue() == null ? CalibreUtils.getViewOffset(player, defaultOffset) : getCheckedBarrelLocation(base, player);
        if (loc == null) return;
        CalibreUtils.spawnLine(loc, loc.getDirection(), spread, distance, laserParticle, hitParticle, e -> e != player);
    }
}
