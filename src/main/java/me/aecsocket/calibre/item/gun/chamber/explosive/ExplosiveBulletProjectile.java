package me.aecsocket.calibre.item.gun.chamber.explosive;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.bullet.BulletProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.projectile.ProjectileHitResult;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

public class ExplosiveBulletProjectile extends BulletProjectile {
    public ExplosiveBulletProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Entity entity) {
        super(location, velocity, base, gun, entity);
    }

    @Override
    public ProjectileHitResult hit(RayTraceResult ray, TickContext context) {
        Location location = ray.getHitPosition().toLocation(getWorld());
        location.getWorld().createExplosion(location,
                stat("explosion_power"),
                stat("explosion_creates_fire"),
                stat("explosion_breaks_blocks")); // TODO custom explosion algo
        return ProjectileHitResult.DESTROY;
    }
}
