package me.aecsocket.calibre.item.gun.ammo;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.calibre.util.PlayerData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class SingleAmmoComponent extends AmmoComponent {
    private class ReloadRunnable implements Runnable {
        private final CalibreUtils.SearchContext result;
        private final Player player;
        private final GunComponent gun;
        private final ItemEvent base;
        private final AmmoSlot slot;

        public ReloadRunnable(CalibreUtils.SearchContext result, Player player, GunComponent gun, ItemEvent base, AmmoSlot slot) {
            this.result = result;
            this.player = player;
            this.gun = gun;
            this.base = base;
            this.slot = slot;
        }

        @Override
        public void run() {
            if (!result.isValid(player)) {
                gun.finishReload(base, player);
                return;
            }
            gun.startAnimation(base, player, "load_single");
            result.apply(player, ItemStack::subtract);

            getAmmo().addLast((ChamberComponent)result.getResult());
            gun.updateItem(player);
            if (!internalReload(base, player, gun, slot))
                gun.finishReload(base, player);
        }
    }

    private boolean internalReload(ItemEvent base, Player player, GunComponent gun, AmmoSlot slot) {
        if (isFull()) return false;
        CalibreUtils.SearchContext result = CalibreUtils.findItem(getPlugin(), player, item -> item instanceof ChamberComponent && isAmmoCompatible((ChamberComponent)item));
        if (result == null) return false;

        SoundData.play(base.stat("load_single_sound", SoundData[].class), player.getEyeLocation());

        gun.availableIn(player, base.stat("load_single_delay", long.class), GunComponent.LOAD_SINGLE_ACTION, new ReloadRunnable(result, player, gun, base, slot));
        return true;
    }

    public void reload(ItemEvent base, Player player, GunComponent gun, AmmoSlot slot) {
        internalReload(base, player, gun, slot);
    }

    @Override
    public void interact(ItemEvent base, PlayerInteractEvent event) {
        super.interact(base, event);
        Player player = event.getPlayer();
        PlayerData data = getPlugin().getPlayerData(player);
        if (data.getAvailableCallback() instanceof ReloadRunnable) {
            ReloadRunnable reload = (ReloadRunnable)data.getAvailableCallback();
            reload.gun.finishReload(reload.base, reload.player);
            data.setAvailableCallback(null);
        }
    }

    @Override public SingleAmmoComponent clone() { return (SingleAmmoComponent)super.clone(); }
    @Override public SingleAmmoComponent copy() { return (SingleAmmoComponent)super.copy(); }
}
