package me.aecsocket.calibre.item.gun.chamber;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public abstract class ProjectileChamber extends ChamberComponent {
    @Override
    public void next(ItemEvent base, GunComponent gun, Player player, Location location, Vector direction) {
        Projectile projectile = createProjectile(location.clone(), direction.clone(), base, gun, player);
        // Prevents ConcurrentModificationExceptions.
        Bukkit.getScheduler().scheduleSyncDelayedTask(getPlugin(), () -> getPlugin().getSchedulerLoop().registerTickable(projectile));
    }

    public abstract Projectile createProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Player player);
}
