package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.item.event.ItemEvent;
import org.bukkit.entity.Player;

public interface Reloadable {
    void reload(ItemEvent base, Player player, GunComponent gun);
}
