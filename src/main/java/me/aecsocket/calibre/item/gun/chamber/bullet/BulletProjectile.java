package me.aecsocket.calibre.item.gun.chamber.bullet;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.service.blockbreak.CalibreBlockBreakService;
import me.aecsocket.calibre.service.wind.CalibreWindService;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.calibre.util.nms.WorldProtocol;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import me.aecsocket.unifiedframework.utils.projectile.ProjectileHitResult;
import org.bukkit.Bukkit;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BulletProjectile extends Projectile {
    public static final ParticleData BLOCK_HIT_PARTICLE = new ParticleData(Particle.BLOCK_CRACK, 16, 0, 0, 0, 0, null, true);

    private final CalibrePlugin plugin;
    private ItemEvent base;
    private GunComponent gun;
    private double originalDamage;

    private Set<Player> impactedPlayers = new HashSet<>();
    private double damage;
    private Vector wind;

    public BulletProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Entity entity) {
        super(location.clone(), velocity.clone(), base.stat("bounce"), base.stat("drag"), base.stat("gravity"), base.stat("steps"), FluidCollisionMode.NEVER, true, base.stat("projectile_size"), entity, e -> !e.isDead());
        plugin = gun.getPlugin();
        this.base = base;
        this.gun = gun;
        originalDamage = stat("damage");

        damage = originalDamage;
        RegisteredServiceProvider<CalibreWindService> windService = Bukkit.getServicesManager().getRegistration(CalibreWindService.class);
        if (windService == null) return;
        wind = windService.getProvider().getWind(getLocation());
        wind.multiply(1d / (SchedulerLoop.MILLIS_PER_TICK * getSteps()));
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public ItemEvent getBase() { return base; }
    public void setBase(ItemEvent base) { this.base = base; }

    public GunComponent getGun() { return gun; }
    public void setGun(GunComponent gun) { this.gun = gun; }

    public Set<Player> getImpactedPlayers() { return impactedPlayers; }
    public void setImpactedPlayers(Set<Player> impactedPlayers) { this.impactedPlayers = impactedPlayers; }

    public double getOriginalDamage() { return originalDamage; }
    public void setOriginalDamage(double originalDamage) { this.originalDamage = originalDamage; }

    public double getDamage() { return damage; }
    public void setDamage(double damage) { this.damage = damage; }

    @Override
    public void step(TickContext context) {
        super.step(context);
        ParticleData.spawn(base.stat("trail_particle", ParticleData[].class), getLocation());
        double impactSoundRadius = stat("flyby_sound_radius");
        SoundData[] impactSound = stat("flyby_sound");
        if (impactSoundRadius > 0 && impactSound != null) {
            getLocation().getNearbyPlayers(impactSoundRadius).forEach(player -> {
                if (player != getEntity() && !impactedPlayers.contains(player)) {
                    SoundData.play(impactSound, player, player.getLocation());
                    impactedPlayers.add(player);
                }
            });
        }

        if (wind != null)
            setLocation(getLocation().add(wind));
    }

    public void hitLiving(LivingEntity entity, RayTraceResult ray, TickContext context) {
        if (entity.getType() == EntityType.ARMOR_STAND) return;
        double damage = stat("damage");
        double armor = entity.getAttribute(Attribute.GENERIC_ARMOR).getValue();
        double multiplier = 1 - Math.min(1, Math.max(0, armor / stat("armor_penetration", double.class)));
        damage *= multiplier;

        double effectiveRange = stat("effective_range");
        double thisDamage = damage * (1 - Math.min(1, Math.max(0, ((getDistanceTraveled() - effectiveRange) / (stat("range", double.class) - effectiveRange)))));
        if (thisDamage <= 0) {
            this.damage = 0;
            return;
        }

        EntityShotByEntityEvent evt = new EntityShotByEntityEvent(getEntity(), entity, thisDamage, this);
        if (!evt.callEvent()) return;
        CalibreUtils.damage(entity, ray.getHitPosition(), thisDamage, evt);
        entity.setNoDamageTicks(0);
    }

    public void hitEntity(Entity entity, RayTraceResult ray, TickContext context) {
        if (entity instanceof LivingEntity)
            hitLiving((LivingEntity)entity, ray, context);
        else if (entity instanceof Hanging) {
            if (new HangingBreakByEntityEvent((Hanging)entity, getEntity(), HangingBreakEvent.RemoveCause.ENTITY).callEvent())
                entity.remove();
        }

        double multiplier = 1 - Math.min(1, Math.max(0, stat("entity_penetration", double.class)));
        setVelocity(getVelocity().multiply(1 - multiplier));
        damage -= originalDamage * multiplier;
    }

    public void hitBlock(Block block, RayTraceResult ray, TickContext context) {
        Location location = ray.getHitPosition().toLocation(getWorld());
        BLOCK_HIT_PARTICLE.setData(block.getBlockData());
        BLOCK_HIT_PARTICLE.spawn(location);
        ParticleData.spawn(stat("hit_particle", ParticleData[].class), location);

        double multiplier = Math.min(1, Math.max(0, plugin.blockHardness(block) / stat("block_penetration", double.class)));
        setVelocity(getVelocity().multiply(1 - multiplier));
        damage -= originalDamage * multiplier;

        boolean spawnBulletHole = true;
        if (base.stat("breakable_blocks", List.class).contains(block.getType().name().toLowerCase())) {
            RegisteredServiceProvider<CalibreBlockBreakService> blockBreak = Bukkit.getServicesManager().getRegistration(CalibreBlockBreakService.class);
            if (blockBreak != null)
                spawnBulletHole = !blockBreak.getProvider().breakBlock(base, this, block, context);
        }

        if (spawnBulletHole && plugin.setting("gun.bullet_hole.enable", boolean.class, false))
            WorldProtocol.spawnBulletHole(plugin, location, ray.getHitBlockFace());
    }

    @Override
    public ProjectileHitResult hit(RayTraceResult ray, TickContext context) {
        if (ray.getHitEntity() != null)
            hitEntity(ray.getHitEntity(), ray, context);
        if (ray.getHitBlock() != null)
            hitBlock(ray.getHitBlock(), ray, context);
        return damage > plugin.setting("gun.damage_threshold", double.class, 0.1)
                ? getBounce() > 0
                ? ProjectileHitResult.BOUNCE
                : ProjectileHitResult.CONTINUE
                : ProjectileHitResult.DESTROY;
    }

    protected <T> T stat(String name) { return base.stat(name); }
    protected <T> T stat(String name, Class<T> type) { return base.stat(name, type); }
}
