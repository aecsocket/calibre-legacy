package me.aecsocket.calibre.item.gun.chamber.bullet;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ProjectileChamber;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Bullet extends ProjectileChamber {
    @Override
    public Projectile createProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Player player) {
        return new BulletProjectile(location, velocity, base, gun, player);
    }
}
