package me.aecsocket.calibre.item.gun.chamber.explosive;

import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.Stats;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ProjectileChamber;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class ExplosiveBullet extends ProjectileChamber {
    public static final StatMap STATS = new StatMap(GunComponent.STATS, Stats.EXPLOSIVE);

    @Override
    public Projectile createProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Player player) {
        return new ExplosiveBulletProjectile(location, velocity, base, gun, player);
    }
}
