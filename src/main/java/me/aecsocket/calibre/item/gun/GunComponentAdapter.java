package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.SimpleItemAdapter;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class GunComponentAdapter<T extends GunComponent> extends SimpleItemAdapter<T> {
    public GunComponentAdapter(CalibrePlugin plugin, String itemType, String registryType) {
        super(plugin, itemType, registryType);
    }

    public GunComponentAdapter(CalibrePlugin plugin) { this(plugin, GunComponent.ITEM_TYPE, GunComponent.TYPE); }

    @Override
    protected void modify(PersistentDataContainer data, ItemStack stack, T item) {
        item.setFireModeIndex(get(data, "fire_mode_index", PersistentDataType.INTEGER, 0));
        item.setSightIndex(get(data, "sight_index", PersistentDataType.INTEGER, 0));
        item.setAiming(get(data, "aiming", PersistentDataType.BYTE, (byte)0) == 1);
        item.setShootAt(get(data, "shoot_at", PersistentDataType.LONG_ARRAY, null));
    }
}
