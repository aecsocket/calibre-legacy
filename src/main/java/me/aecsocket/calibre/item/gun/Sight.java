package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.util.AcceptsPlugin;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import me.aecsocket.unifiedframework.stat.StatData;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public class Sight implements AcceptsPlugin {
    public static final float ZOOM_SPEED = -0.0001f;

    private transient CalibrePlugin plugin;
    private String id;
    private StatData stats;
    private StatData aimStats;
    private float fovMultiplier;

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public StatData getStats() { return stats; }
    public void setStats(StatData stats) { this.stats = stats; }

    public StatData getAimStats() { return aimStats; }
    public void setAimStats(StatData aimStats) { this.aimStats = aimStats; }

    public float getFOVMultiplier() { return fovMultiplier; }
    public void setFOVMultiplier(float fovMultiplier) { this.fovMultiplier = fovMultiplier; }

    public String getName(@Nullable Player player) { return plugin.gen(player, "sight." + id); }

    public void modifyStats(CalibreComponent component, StatData stats) {}

    public void apply(ItemEvent base, Player player, boolean aiming) {
        if (aiming)
            PlayerProtocol.setFOV(base.getBase().getPlugin(), player, fovMultiplier);
        else
            PlayerProtocol.resetFOV(base.getBase().getPlugin(), player);
    }
}
