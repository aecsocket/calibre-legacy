package me.aecsocket.calibre.item.gun.ammo;

import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.unifiedframework.registry.Registry;

import java.util.LinkedList;
import java.util.Objects;

public class AmmoComponentReference extends ComponentReference {
    private LinkedList<AmmoReference> ammo;

    public AmmoComponentReference(String id, LinkedList<AmmoReference> ammo) {
        super(id);
        this.ammo = ammo;
    }

    public LinkedList<AmmoReference> getAmmo() { return ammo; }
    public void setAmmo(LinkedList<AmmoReference> ammo) { this.ammo = ammo; }

    @Override
    public AmmoComponent create(Registry registry) {
        CalibreComponent component = super.create(registry);
        if (!(component instanceof AmmoComponent)) return null;
        AmmoComponent result = (AmmoComponent)component;
        result.setAmmo(AmmoComponent.fromReferenceAmmo(registry, ammo));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AmmoComponentReference that = (AmmoComponentReference)o;
        return Objects.equals(ammo, that.ammo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ammo);
    }
}
