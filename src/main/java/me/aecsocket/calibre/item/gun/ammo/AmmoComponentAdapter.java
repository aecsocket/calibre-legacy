package me.aecsocket.calibre.item.gun.ammo;

import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.gun.GunComponentAdapter;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.LinkedList;

public class AmmoComponentAdapter<T extends AmmoComponent> extends GunComponentAdapter<T> {
    public AmmoComponentAdapter(CalibrePlugin plugin, String itemType, String registryType) {
        super(plugin, itemType, registryType);
    }

    public AmmoComponentAdapter(CalibrePlugin plugin) { this(plugin, AmmoComponent.ITEM_TYPE, AmmoComponent.TYPE); }

    protected void modify(PersistentDataContainer data, ItemStack stack, T item) {
        super.modify(data, stack, item);
        item.setAmmo(AmmoComponent.fromReferenceAmmo(
                getPlugin().getRegistry(),
                getPlugin().getGson().fromJson(get(data, "ammo", PersistentDataType.STRING, ""), new TypeToken<LinkedList<AmmoReference>>(){}.getType())
        ));
    }
}
