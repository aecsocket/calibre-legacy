package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.service.wind.CalibreWindService;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.CardinalDirection;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.MainHand;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.util.Vector;

public class WindMeasureComponent extends GunComponent {
    private static final String padding = " ".repeat(40);

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (!(tickContext.getLoop() instanceof SchedulerLoop)) return;
        RegisteredServiceProvider<CalibreWindService> provider = Bukkit.getServicesManager().getRegistration(CalibreWindService.class);
        if (provider == null) return;
        Vector wind = provider.getProvider().getWind(player.getEyeLocation());
        double yaw = FrameworkUtils.getYaw(wind);

        String text = getPlugin().gen(player, "wind_measure",
                getPlugin().gen(player, "direction." + CardinalDirection.Order2.get((float)yaw).name().toLowerCase()),
                metersPerSecondToKnots(wind.length())
        );
        player.sendTitle("", player.getMainHand() == MainHand.RIGHT ? padding + text : text + padding, 0, 3, 0);
    }

    public static double metersPerSecondToKnots(double metresPerSecond) { return metresPerSecond * 1.943844; }
}
