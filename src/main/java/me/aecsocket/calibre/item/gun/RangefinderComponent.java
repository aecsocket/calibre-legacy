package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.MainHand;
import org.bukkit.util.RayTraceResult;

public class RangefinderComponent extends GunComponent {
    private static final String padding = " ".repeat(40);

    private int maxDistance;
    private double precision;

    public int getMaxDistance() { return maxDistance; }
    public void setMaxDistance(int maxDistance) { this.maxDistance = maxDistance; }

    public double getPrecision() { return precision; }
    public void setPrecision(double precision) { this.precision = precision; }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (!(tickContext.getLoop() instanceof SchedulerLoop)) return;
        Location loc = getCheckedBarrelLocation(base, player);
        if (loc == null) return;
        RayTraceResult ray = loc.getWorld().rayTrace(loc, loc.getDirection(), maxDistance, FluidCollisionMode.NEVER, true, 0, e -> e != player);
        String text;
        if (ray == null)
            text = getPlugin().gen(player, "rangefinder.out_of_range");
        else {
            double d = ray.getHitPosition().distance(loc.toVector());
            text = getPlugin().gen(player, "rangefinder.range", precision * (Math.round(d / precision)));
        }
        player.sendTitle("", player.getMainHand() == MainHand.RIGHT ? padding + text : text + padding, 0, 3, 0);
    }
}
