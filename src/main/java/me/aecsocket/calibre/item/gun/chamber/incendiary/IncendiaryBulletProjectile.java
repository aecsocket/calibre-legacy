package me.aecsocket.calibre.item.gun.chamber.incendiary;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.bullet.BulletProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

public class IncendiaryBulletProjectile extends BulletProjectile {
    public IncendiaryBulletProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Entity entity) {
        super(location, velocity, base, gun, entity);
    }

    @Override
    public void hitLiving(LivingEntity entity, RayTraceResult ray, TickContext context) {
        super.hitLiving(entity, ray, context);
        entity.setFireTicks(entity.getFireTicks() + stat("incendiary_fire_time", int.class));
    }
}
