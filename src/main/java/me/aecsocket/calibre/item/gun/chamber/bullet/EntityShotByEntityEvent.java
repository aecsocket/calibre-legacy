package me.aecsocket.calibre.item.gun.chamber.bullet;

import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.jetbrains.annotations.NotNull;

public class EntityShotByEntityEvent extends EntityDamageByEntityEvent {
    private BulletProjectile bullet;

    public EntityShotByEntityEvent(@NotNull Entity damager, @NotNull Entity damagee, double damage, BulletProjectile bullet) {
        super(damager, damagee, DamageCause.ENTITY_ATTACK, damage);
        this.bullet = bullet;
    }

    public BulletProjectile getBullet() { return bullet; }
    public void setBullet(BulletProjectile bullet) { this.bullet = bullet; }
}
