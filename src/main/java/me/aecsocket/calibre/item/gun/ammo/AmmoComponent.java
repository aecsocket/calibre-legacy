package me.aecsocket.calibre.item.gun.ammo;

import com.google.gson.annotations.SerializedName;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.ActiveStatModifier;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.registry.Registry;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class AmmoComponent extends GunComponent implements ActiveStatModifier {
    public static final String ITEM_TYPE = "ammo_component";

    private StatData activeStats;
    private String caliber;
    private LinkedList<ChamberComponent> ammo = new LinkedList<>();
    private int capacity;
    private String icon;
    @SerializedName("ammo_ids")
    private List<String> compatibleIds;
    @SerializedName("ammo_categories")
    private List<String> compatibleCategories;

    @Override
    public StatData getActiveStats() { return activeStats; }
    @Override
    public void setActiveStats(StatData activeStats) { this.activeStats = activeStats; }

    public String getCaliber() { return caliber; }
    public void setCaliber(String caliber) { this.caliber = caliber; }

    public LinkedList<ChamberComponent> getAmmo() { return ammo; }
    public void setAmmo(LinkedList<ChamberComponent> ammo) { this.ammo = ammo; }

    public int getCapacity() { return capacity; }
    public void setCapacity(int capacity) { this.capacity = capacity; }

    public String getIcon() { return icon; }
    public void setIcon(String icon) { this.icon = icon; }

    public List<String> getCompatibleIds() { return compatibleIds; }
    public void setCompatibleIds(List<String> compatibleIds) { this.compatibleIds = compatibleIds; }

    public List<String> getCompatibleCategories() { return compatibleCategories; }
    public void setCompatibleCategories(List<String> compatibleCategories) { this.compatibleCategories = compatibleCategories; }

    public String createIcon(String prefix) { return ChatColor.translateAlternateColorCodes('&', prefix + icon); }

    public boolean isFull() { return ammo.size() >= capacity; }

    @Override
    public String getItemType() { return ITEM_TYPE; }

    @Override
    public void click(ItemEvent base, InventoryClickEvent event) {
        super.click(base, event);
        if (event.getClick() == ClickType.SHIFT_RIGHT) {
            event.setCancelled(true);
            ItemStack clickedStack = event.getCurrentItem();
            if (clickedStack != null && clickedStack.getAmount() > 1) return;
            ChamberComponent next = next();
            if (next != null) {
                Player player = (Player)event.getWhoClicked();
                PlayerInventory inv = player.getInventory();
                inv.addItem(next.create(player, 1));
                int slot = event.getSlot();
                ItemStack stack = inv.getItem(slot);
                inv.setItem(event.getSlot(), create(player, stack.getAmount()));
                SoundData.play(getPlugin().setting("ammo_modify.remove_sound", SoundData[].class, null), player, player.getLocation());
            }
        }
    }

    @Override
    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, StatData stats, @Nullable Player player, int amount) {
        super.modify(data, meta, item, stats, player, amount);
        data.set(key("ammo"), PersistentDataType.STRING, getPlugin().getGson().toJson(createReferenceAmmo()));
    }

    @Override
    public String getName(@Nullable CommandSender sender) {
        CalibrePlugin plugin = getPlugin();
        return plugin.gen(sender, "component.ammo.name" + (ammo.isEmpty() ? ".empty" : ""),
                super.getName(sender),
                ammo.size(),
                capacity
        );
    }

    @Override
    protected List<List<String>> getSections(@Nullable Player player, int amount) {
        List<List<String>> result = super.getSections(player, amount);

        if (getPlugin().setting("gun.show_ammo", boolean.class, true))
            addSection(result, getAmmo(player, amount));

        return result;
    }

    protected List<String> getAmmo(@Nullable Player player, int amount) {
        List<String> result = new ArrayList<>();
        ChamberComponent testChamber = null;
        int chamberAmount = 0;
        for (ChamberComponent chamber : ammo) {
            if (chamber.equals(testChamber))
                ++chamberAmount;
            else {
                if (testChamber != null)
                    result.add(getPlugin().gen(player, "component.ammo.lore", chamberAmount, testChamber.getName(player)));
                testChamber = chamber;
                chamberAmount = 1;
            }
        }
        if (testChamber != null)
            result.add(getPlugin().gen(player, "component.ammo.lore", chamberAmount, testChamber.getName(player)));
        return result;
    }

    public LinkedList<AmmoReference> createReferenceAmmo() {
        LinkedList<AmmoReference> refs = new LinkedList<>();
        ComponentReference testRef = null;
        int amount = 0;
        for (ChamberComponent chamber : ammo) {
            ComponentReference ref = chamber.createReference();
            if (ref.equals(testRef))
                ++amount;
            else {
                if (testRef != null)
                    refs.add(new AmmoReference(testRef, amount));
                testRef = ref;
                amount = 1;
            }
        }
        if (testRef != null)
            refs.add(new AmmoReference(testRef, amount));
        return refs;
    }

    public int getAmount() { return ammo.size(); }
    public boolean hasNext() { return !ammo.isEmpty(); }
    public ChamberComponent next() { return hasNext() ? ammo.removeLast() : null; }
    public ChamberComponent peek() { return hasNext() ? ammo.getLast() : null; }
    public boolean isAmmoCompatible(ChamberComponent chamber) {
        return caliber.equals(chamber.getCaliber()) && (
                (compatibleIds == null && compatibleCategories == null)
                || (compatibleIds != null && compatibleIds.contains(chamber.getId()))
                || (compatibleCategories != null && !Collections.disjoint(compatibleCategories, chamber.getCategories())));
    }

    public void reload(ItemEvent base, Player player, GunComponent gun, AmmoSlot slot) {
        // Unload
        SoundData.play(base.stat("unload_sound", SoundData[].class), player.getEyeLocation());
        gun.startAnimation(base, player, "unload");

        gun.availableIn(player, base.stat("unload_delay", long.class), GunComponent.UNLOAD_ACTION, () -> {
            boolean canLoad = CalibreUtils.findItem(getPlugin(), player, item -> item instanceof AmmoComponent && slot.isCompatible((CalibreComponent)item)) != null;
            CalibreUtils.safeGiveItem(player, create(player, 1));
            slot.set(null);
            gun.updateItem(player);
            if (canLoad)
                slot.reload(base, player, gun);
            else
                gun.finishReload(base, player);
        });
    }

    @Override
    public ComponentReference createReference() {
        return new AmmoComponentReference(getId(), createReferenceAmmo());
    }

    @Override public AmmoComponent clone() { return (AmmoComponent)super.clone(); }

    @Override
    public AmmoComponent copy() {
        AmmoComponent result = (AmmoComponent)super.copy();
        result.ammo = ammo == null ? null : new LinkedList<>(ammo);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AmmoComponent that = (AmmoComponent)o;
        return capacity == that.capacity &&
                Objects.equals(activeStats, that.activeStats) &&
                Objects.equals(caliber, that.caliber) &&
                Objects.equals(ammo, that.ammo) &&
                Objects.equals(icon, that.icon) &&
                Objects.equals(compatibleIds, that.compatibleIds) &&
                Objects.equals(compatibleCategories, that.compatibleCategories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), activeStats, caliber, ammo, capacity, icon, compatibleIds, compatibleCategories);
    }

    public static LinkedList<ChamberComponent> fromReferenceAmmo(Registry registry, LinkedList<AmmoReference> refs) {
        LinkedList<ChamberComponent> ammo = new LinkedList<>();
        refs.forEach(ref -> Collections.addAll(ammo, ref.create(registry)));
        return ammo;
    }
}
