package me.aecsocket.calibre.item.gun.ammo;

import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.unifiedframework.registry.Registry;

public class AmmoReference {
    private ComponentReference component;
    private int amount;

    public AmmoReference(ComponentReference component, int amount) {
        this.component = component;
        this.amount = amount;
    }

    public AmmoReference(ComponentReference component) { this(component, 1); }

    public ComponentReference getComponent() { return component; }
    public void setComponent(ComponentReference component) { this.component = component; }

    public int getAmount() { return amount; }
    public void setAmount(int amount) { this.amount = amount; }

    public ChamberComponent[] create(Registry registry) {
        ChamberComponent[] result = new ChamberComponent[amount];
        for (int i = 0; i < amount; i++) {
            CalibreComponent created = component.create(registry);
            if (created instanceof ChamberComponent)
                result[i] = (ChamberComponent)created;
        }
        return result;
    }
}
