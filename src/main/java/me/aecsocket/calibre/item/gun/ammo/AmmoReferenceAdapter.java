package me.aecsocket.calibre.item.gun.ammo;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;

public class AmmoReferenceAdapter implements UtilJsonAdapter, JsonAdapter<AmmoReference> {
    @Override
    public JsonElement serialize(AmmoReference ammo, Type jType, JsonSerializationContext context) {
        JsonElement serialized = context.serialize(ammo.getComponent());
        int amount = ammo.getAmount();
        return amount == 1 ? serialized : buildArray().add(serialized).add(amount).create();
    }

    @Override
    public AmmoReference deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        return json.isJsonArray()
                ? new AmmoReference(context.deserialize(json.getAsJsonArray().get(0), ComponentReference.class), json.getAsJsonArray().get(1).getAsInt())
                : new AmmoReference(context.deserialize(json, ComponentReference.class));
    }
}
