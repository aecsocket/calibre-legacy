package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import org.bukkit.inventory.ItemStack;

public class ReloadContext {
    private ItemStack item;
    private int inventorySlot;
    private CalibreComponentSlot slot;
    private CalibreComponent component;

    public ReloadContext(ItemStack item, int inventorySlot, CalibreComponentSlot slot, CalibreComponent component) {
        this.item = item;
        this.inventorySlot = inventorySlot;
        this.slot = slot;
        this.component = component;
    }

    public ItemStack getItem() { return item; }
    public void setItem(ItemStack item) { this.item = item; }

    public int getInventorySlot() { return inventorySlot; }
    public void setInventorySlot(int inventorySlot) { this.inventorySlot = inventorySlot; }

    public CalibreComponentSlot getSlot() { return slot; }
    public void setSlot(CalibreComponentSlot slot) { this.slot = slot; }

    public CalibreComponent getComponent() { return component; }
    public void setComponent(CalibreComponent component) { this.component = component; }
}
