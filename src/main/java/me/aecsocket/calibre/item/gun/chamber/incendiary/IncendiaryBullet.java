package me.aecsocket.calibre.item.gun.chamber.incendiary;

import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.IntegerStat;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ProjectileChamber;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;

public class IncendiaryBullet extends ProjectileChamber {
    public static final StatMap STATS = new StatMap(GunComponent.STATS)
            .add("incendiary_fire_time", new IntegerStat().defaultSupplier(i -> 0).textSupplier((stat, plugin, name, inst, player, value) -> new DecimalFormat("0.0").format(value / 20d)));

    @Override
    public Projectile createProjectile(Location location, Vector velocity, ItemEvent base, GunComponent gun, Player player) {
        return new IncendiaryBulletProjectile(location, velocity, base, gun, player);
    }
}
