package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.util.AcceptsPlugin;
import me.aecsocket.unifiedframework.stat.StatData;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public class FireMode implements AcceptsPlugin {
    private transient CalibrePlugin plugin;
    private String id;
    private StatData stats;

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public StatData getStats() { return stats; }
    public void setStats(StatData stats) { this.stats = stats; }

    public String getIcon(@Nullable Player player, boolean selected) { return plugin.gen(player, "fire_mode." + (selected ? "" : "un") + "selected") + plugin.gen(player, "fire_mode.icon." + id); }

    public String getName(@Nullable Player player) { return plugin.gen(player, "fire_mode." + id); }

    public void modifyStats(CalibreComponent component, StatData stats) {}

    @Override
    public String toString() {
        return "FireMode{" +
                "id='" + id + '\'' +
                ", stats=" + stats +
                '}';
    }
}
