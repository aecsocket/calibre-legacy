package me.aecsocket.calibre.item.gun;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import com.google.gson.reflect.TypeToken;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.animation.AnimationInstance;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import me.aecsocket.calibre.item.data.*;
import me.aecsocket.calibre.item.data.number.DecimalStat;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.number.IntegerStat;
import me.aecsocket.calibre.item.data.number.LongStat;
import me.aecsocket.calibre.item.event.ItemAction;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.ammo.AmmoComponent;
import me.aecsocket.calibre.item.gun.ammo.AmmoSlot;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import me.aecsocket.calibre.service.casing.CalibreCasingService;
import me.aecsocket.calibre.service.stabilization.CalibreAimStabilizationService;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.calibre.util.PlayerData;
import me.aecsocket.calibre.util.TextUtils;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.component.ComponentSlot;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.FrameworkUtils;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.Vector2;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.*;
import org.bukkit.entity.Item;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GunComponent extends CalibreComponent {
    public static final String ITEM_TYPE = "gun";
    public static final long LATE_ACTION_TIME = 100;
    private static final Random random = new Random();
    public static final StatMap STATS = new StatMap(CalibreItem.STATS)
            .add("fire_modes", new ListStat<FireMode>(new TypeToken<ArrayList<FireMode>>(){}).defaultSupplier(i -> new ArrayList<>()).hide())
            .add("sights", new ListStat<Sight>(new TypeToken<ArrayList<Sight>>(){}).defaultSupplier(i -> new ArrayList<>()).hide())

            .add("manual_chamber", new BooleanStat().defaultSupplier(i -> false))
            .add("can_fire_underwater", new BooleanStat().defaultSupplier(i -> false))
            .add("requires_release", new BooleanStat().defaultSupplier(i -> false))
            .add("barrel_offset", new VectorStat().defaultSupplier(i -> new Vector()).textSupplier((stat, plugin, name, inst, player, value) -> Double.toString(value.getZ())))
            .add("target_chamber_slot", new IntegerStat().defaultSupplier(i -> 0).hide())
            .add("target_ammo_slot", new IntegerStat().defaultSupplier(i -> 0).hide())

            .add("shots", new IntegerStat().defaultSupplier(i -> 1).hide())
            .add("projectiles", new IntegerStat().defaultSupplier(i -> 1))
            .add("muzzle_velocity", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("gravity", new DoubleStat().precision(1).defaultSupplier(i -> Projectile.GRAVITY))
            .add("drag", new DoubleStat().precision(1).defaultSupplier(i -> 0d).textSupplier((stat, plugin, name, inst, player, value) -> ((DecimalStat<?>)stat).getFormat().format(value * 100d)))
            .add("projectile_size", new DoubleStat().defaultSupplier(i -> 0.1d).hide())
            .add("steps", new IntegerStat().defaultSupplier(i -> 16).hide())
            .add("bounce", new DoubleStat().defaultSupplier(i -> 0d).textSupplier((stat, plugin, name, inst, player, value) -> ((DecimalStat<?>)stat).getFormat().format(value * 100d)))

            .add("zero_distance", new DoubleStat().defaultSupplier(i -> 0d))
            .add("spread", new DoubleStat().precision(2).defaultSupplier(i -> 0d))
            .add("firing_spread", new DoubleStat().precision(2).defaultSupplier(i -> 0d))
            .add("projectile_spread", new DoubleStat().precision(2).defaultSupplier(i -> 0d))
            .add("sneak_spread_multiplier", new DoubleStat().precision(1).defaultSupplier(i -> 1d))
            .add("aim_spread_multiplier", new DoubleStat().precision(1).defaultSupplier(i -> 1d))
            .add("move_spread", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("sprint_spread", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("jump_spread", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("recoil", new Vector2Stat().defaultSupplier(i -> new Vector2()).hide())
            .add("spread_recoil_multiplier", new DoubleStat().precision(1).defaultSupplier(i -> 1d).hide())
            .add("sway", new DoubleStat().defaultSupplier(i -> 0d))
            .add("sway_speed", new DoubleStat().defaultSupplier(i -> 0d))
            .add("spread_sway_multiplier", new DoubleStat().defaultSupplier(i -> 1d).hide())
            .add("aim_stabilize_multiplier", new DoubleStat().defaultSupplier(i -> 1d).hide())

            .add("damage", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("effective_range", new DoubleStat().defaultSupplier(i -> 0d))
            .add("range", new DoubleStat().defaultSupplier(i -> 0d))
            .add("breakable_blocks", new ListStat<String>(new TypeToken<ArrayList<String>>(){}).defaultSupplier(i -> new ArrayList<>()).hide())
            .add("flyby_sound_radius", new DoubleStat().defaultSupplier(i -> 0d).hide())
            .add("awareness_radius", new DoubleStat().defaultSupplier(i -> 0d).hide())
            .add("block_penetration", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("armor_penetration", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("entity_penetration", new DoubleStat().precision(1).defaultSupplier(i -> 0d).textSupplier((stat, plugin, name, inst, player, value) -> ((DecimalStat<?>)stat).getFormat().format(value * 100d)))

            .add("casing_offset", new VectorStat().defaultSupplier(i -> new Vector()).hide())
            .add("casing_velocity", new VectorStat().defaultSupplier(i -> new Vector()).hide())

            .add("shot_delay", new LongStat().textSupplier((stat, plugin, name, inst, player, value) -> new DecimalFormat("0.0").format(60000d / value)).defaultSupplier(i -> 1L))
            .add("fire_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("chamber_delay", new LongStat().textSupplier((stat, plugin, name, inst, player, value) -> new DecimalFormat("0.0").format(value / 1000d)).defaultSupplier(i -> 1L))
            .add("empty_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("aim_in_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("aim_out_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("change_fire_mode_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("change_sight_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("sprint_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("unload_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("load_delay", new LongStat().defaultSupplier(i -> 1L).hide())
            .add("load_single_delay", new LongStat().defaultSupplier(i -> 1L).hide())

            .add("shot_sound", new SoundDataStat().hide())
            .add("chamber_sound", new SoundDataStat().hide())
            .add("empty_sound", new SoundDataStat().hide())
            .add("aim_in_sound", new SoundDataStat().hide())
            .add("aim_out_sound", new SoundDataStat().hide())
            .add("change_fire_mode_sound", new SoundDataStat().hide())
            .add("unload_sound", new SoundDataStat().hide())
            .add("load_sound", new SoundDataStat().hide())
            .add("load_single_sound", new SoundDataStat().hide())
            .add("flyby_sound", new SoundDataStat().hide())

            .add("shot_particle", new ParticleDataStat().hide())
            .add("trail_particle", new ParticleDataStat().hide())
            .add("hit_particle", new ParticleDataStat().hide());
    public static final ItemAction FIRE_ACTION = new ItemAction(){};
    public static final ItemAction CHAMBER_ACTION = new ItemAction(){};
    public static final ItemAction AIM_ACTION = new ItemAction(){};
    public static final ItemAction CHANGE_SIGHT_ACTION = new ItemAction(){};
    public static final ItemAction CHANGE_FIRE_MODE_ACTION = new ItemAction(){};
    public static final ItemAction DOWN_ACTION = new ItemAction(){};
    public static final ItemAction UP_ACTION = new ItemAction(){};
    public static final ItemAction UNLOAD_ACTION = new ItemAction(){};
    public static final ItemAction LOAD_ACTION = new ItemAction(){};
    public static final ItemAction LOAD_SINGLE_ACTION = new ItemAction(){};

    private int fireModeIndex;
    private int sightIndex;
    private boolean aiming;

    private long[] shootAt;

    @Override protected StatMap getDefaultStats() { return STATS; }

    public int getFireModeIndex() { return fireModeIndex; }
    public void setFireModeIndex(int fireModeIndex) { this.fireModeIndex = fireModeIndex; }

    public int getSightIndex() { return sightIndex; }
    public void setSightIndex(int sightIndex) { this.sightIndex = sightIndex; }

    public boolean isAiming() { return aiming; }
    public void setAiming(boolean aiming) { this.aiming = aiming; }

    public long[] getShootAt() { return shootAt; }
    public void setShootAt(long[] shootAt) { this.shootAt = shootAt; }

    @Override public String getItemType() { return ITEM_TYPE; }

    public List<ChamberSlot> getChamberSlots(ComponentHolder component, StatData stats) { return getSlots(component, slot -> slot instanceof ChamberSlot, stats.getValue("target_chamber_slot")); }
    public List<ChamberSlot> getChamberSlots(ItemEvent base) { return getChamberSlots(base.getBase(), base.getStats()); }
    public List<AmmoSlot> getAmmoSlots(ComponentHolder component, StatData stats) { return getSlots(component, slot -> slot instanceof AmmoSlot, stats.getValue("target_ammo_slot")); }
    public List<AmmoSlot> getAmmoSlots(ItemEvent base) { return getAmmoSlots(base.getBase(), base.getStats()); }
    public <T> T getFromList(List<T> list, int index) {
        return list == null ? null : index < 0 ? null : list.size() == 0 ? null : list.get(index % list.size());
    }
    public FireMode getFireMode(ItemEvent event) { return getFromList(event.stat("fire_modes"), fireModeIndex); }
    public Sight getSight(ItemEvent event) { return getFromList(event.stat("sights"), sightIndex); }

    @Override
    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, StatData stats, @Nullable Player player, int amount) {
        super.modify(data, meta, item, stats, player, amount);
        data.set(key("fire_mode_index"), PersistentDataType.INTEGER, fireModeIndex);
        data.set(key("sight_index"), PersistentDataType.INTEGER, sightIndex);
        data.set(key("aiming"), PersistentDataType.BYTE, (byte)(aiming ? 1 : 0));
        if (shootAt != null) data.set(key("shoot_at"), PersistentDataType.LONG_ARRAY, shootAt);
    }

    @Override
    public void modifyStats(CalibreComponent component, StatData stats) {
        super.modifyStats(component, stats);
        if (component != this) return;

        Sight sight = getFromList(stats.getValue("sights"), sightIndex);
        if (sight != null) {
            if (sight.getStats() != null)
                stats.combine(sight.getStats());
            if (aiming && sight.getAimStats() != null)
                stats.combine(sight.getAimStats());
            sight.modifyStats(component, stats);
        }

        FireMode fireMode = getFromList(stats.getValue("fire_modes"), fireModeIndex);
        if (fireMode != null) {
            if (fireMode.getStats() != null)
                stats.combine(fireMode.getStats());
            fireMode.modifyStats(component, stats);
        }

        getChamberSlots(component, stats).stream()
                .filter(slot -> slot.get() != null && slot.get().getActiveStats() != null)
                .map(slot -> slot.get().getActiveStats())
                .forEach(stats::combine);
        getAmmoSlots(component, stats).stream()
                .filter(slot -> slot.get() != null && slot.get().getActiveStats() != null)
                .map(slot -> slot.get().getActiveStats())
                .forEach(stats::combine);
    }

    @SuppressWarnings("unchecked")
    private <T extends CalibreComponentSlot> List<T> getSlots(ComponentHolder component, Predicate<ComponentSlot> predicate, int targetPriority) {
        List<T> result = new ArrayList<>();
        component.walk(data -> {
            if (!predicate.test(data.getSlot())) return;
            T slot = (T)data.getSlot();
            if (slot.getPriority() == targetPriority)
                result.add(slot);
        });
        return result;
    }

    private boolean aimSetting(ItemEvent base, Player player, String setting) {
        if (!aiming) return true;
        if (getPlugin().setting("gun.when_aiming." + setting, boolean.class, false)) return true;
        toggleAiming(base, player, getSight(base), false);
        return false;
    }

    private void fireCheck(ItemEvent base, Player player) {
        if (shootAt == null || shootAt.length == 0) return;
        long time = System.currentTimeMillis();
        if (time >= shootAt[0] && time < shootAt[0] + LATE_ACTION_TIME) {
            shootAt = Arrays.copyOfRange(shootAt, 1, shootAt.length);
            fireOnce(base, player);
            fireCheck(base, player);
        }
    }

    private String actionBarAction(Player player, String[] bar, int duration, int elapsed, String baseText, ItemAction action, String actionKey) {
        if (getPlugin().getPlayerData(player).getLastAction() == action)
            return getPlugin().gen(player, "action_bar.gun." + actionKey, bar[0], bar[1], (duration - elapsed) / 1000d, baseText);
        return baseText;
    }

    @SuppressWarnings("unchecked")
    protected void sendActionBar(ItemEvent base, Player player) {
        boolean compactBar = getPlugin().setting("gun.compact_action_bar", boolean.class, false);
        List<String> chamberSegments = new ArrayList<>();
        List<String> ammoSegments = new ArrayList<>();

        // TODO clean up the color codes, prefix, etc.
        // It's awfully ugly.
        if (compactBar) {
            List<ChamberSlot> slots = getChamberSlots(base);
            if (slots.size() > 0) {
                List<ChamberComponent> chambers = slots.stream().filter(slot -> slot.get() != null).map(ChamberSlot::get).collect(Collectors.toList());
                int amount = chambers.size();
                chamberSegments.add(
                        amount > 0
                                ? ChatColor.translateAlternateColorCodes('&', chambers.get(0).getPrefix()) + amount
                                : Integer.toString(amount)
                );
            }
        } else {
            getChamberSlots(base).forEach(slot -> {
                if (slot.get() == null) return;
                chamberSegments.add(slot.get().createIcon());
            });
        }

        getAmmoSlots(base).stream().filter(slot -> slot.get() != null).map(slot -> {
            AmmoComponent ammo = slot.get();
            if (compactBar) {
                ChamberComponent next = ammo.peek();
                return (next == null ? getPlugin().gen(player, "component.ammo.empty") : ChatColor.translateAlternateColorCodes('&', next.getPrefix())) + ammo.getAmount();
            }
            StringBuilder str = new StringBuilder();
            ammo.getAmmo().forEach(chamber -> str.append(ammo.createIcon(chamber.getPrefix())));
            str.append(ammo.createIcon(getPlugin().gen(player, "component.ammo.empty")).repeat(ammo.getCapacity() - ammo.getAmount()));
            return str.toString();
        }).forEach(ammoSegments::add);

        double zeroDistance = base.stat("zero_distance");
        String join = getPlugin().gen(player, "action_bar.join");
        String baseText = getPlugin().gen(player, "action_bar.gun",
                        String.join(join, chamberSegments),
                        String.join(join, ammoSegments),
                        ((List<FireMode>)base.stat("fire_modes")).stream()
                                .map(mode -> mode.getIcon(player, mode == getFireMode(base)))
                                .collect(Collectors.joining(" "))
                );
        if (zeroDistance > 0)
            baseText = getPlugin().gen(player, "action_bar.gun.zero", baseText, zeroDistance);
        if (!isAvailable(player)) {
            PlayerData data = getPlugin().getPlayerData(player);
            int duration = (int)data.getDuration();
            int elapsed = (int)data.getElapsed();
            String[] bar = TextUtils.createBar(elapsed, duration, player, getPlugin(), "gun");
            baseText = actionBarAction(player, bar, duration, elapsed, baseText, UNLOAD_ACTION, "unload");
            baseText = actionBarAction(player, bar, duration, elapsed, baseText, LOAD_ACTION, "load");
            baseText = actionBarAction(player, bar, duration, elapsed, baseText, LOAD_SINGLE_ACTION, "load_single");
            baseText = actionBarAction(player, bar, duration, elapsed, baseText, CHAMBER_ACTION, "chamber");
        }
        player.sendActionBar(baseText);
    }

    protected void autoChamber(ItemEvent base, Player player) {
        List<ChamberSlot> slots = getChamberSlots(base);
        boolean[] load = {false};
        slots.forEach(chamberSlot -> {
            if (load[0]) return;
            if (chamberSlot.get() == null) load[0] = true;
        });
        if (load[0]) {
            AnimationInstance animation = getPlugin().getPlayerData(player).getAnimation();
            if (animation != null)
                animation.at("end", () -> callEvent(GunComponent.class, i -> i.chamber(base, player)));
        }
    }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (base.getBase() != this || !isComplete()) return;
        PlayerData data = getPlugin().getPlayerData(player);
        if (tickContext.getLoop() instanceof SchedulerLoop) {
            fireCheck(base, player);

            if (getPlugin().setting("gun.show_action_bar", boolean.class, true))
                sendActionBar(base, player);

            // Sprint
            if (!aiming && getPlugin().setting("gun.sprint_disables", boolean.class, true)) {
                if (player.isSprinting()) {
                    if (!data.actionIs(DOWN_ACTION) && (getPlugin().setting("gun.sprint_cancels", boolean.class, false) || isAvailable(player))) {
                        startAnimation(base, player, "down");
                        availableIn(player, Long.MAX_VALUE - System.currentTimeMillis(), DOWN_ACTION);
                    }
                } else if (data.actionIs(DOWN_ACTION)) {
                    startAnimation(base, player, "up");
                    availableIn(player, base.stat("sprint_delay"), UP_ACTION);
                }
            }
        } else {
            // Sway
            double multiplier = base.stat("sway", double.class) * (((data.getSpread() / base.stat("spread", double.class)) + 1) * base.stat("spread_sway_multiplier", double.class));
            if (multiplier > 0) {
                double angle = Math.toRadians(tickContext.getLoop().getTicks() * base.stat("sway_speed", double.class));
                Vector2 sway = new Vector2(
                        Math.cos(angle) * multiplier,
                        Math.sin(angle) * multiplier
                );
                RegisteredServiceProvider<CalibreAimStabilizationService> aimStabilization = Bukkit.getServicesManager().getRegistration(CalibreAimStabilizationService.class);
                if (aimStabilization != null && aimStabilization.getProvider().stabilize(player, tickContext, sway))
                    sway.multiply(base.stat("aim_stabilize_multiplier", double.class));

                if (sway.manhattanLength() > 0.0001)
                    PlayerProtocol.positionCamera(getPlugin(), player, (float)sway.getX(), (float)sway.getY());
            }

            // Fire
            if (
                    canBePrecise() &&
                    !base.stat("requires_release", boolean.class) &&
                    player.isHandRaised() &&
                    isAvailableIn(player, base.stat("fire_delay"), FIRE_ACTION)
            )
                callEvent(GunComponent.class, i -> i.fire(base, player));
        }
    }

    @Override
    public void draw(ItemEvent base, Player player, int slot) {
        super.draw(base, player, slot);
        if (base.getBase() != this || !isComplete()) return;
        shootAt = null;
        if (!getPlugin().setting("gun.auto_chamber", boolean.class, true)) return;
        autoChamber(base, player);
    }

    @Override
    public void holster(ItemEvent base, Player player, int slot) {
        if (base.getBase() != this || !isComplete()) return;
        shootAt = null;
        super.holster(base, player, slot);
        if (base.getBase() != this || !isComplete()) return;
        if (aiming)
            callEvent(GunComponent.class, i -> i.toggleAiming(base, player, getSight(base), false));
    }

    @Override
    public void changeClickState(ItemEvent base, Player player, TickContext tickContext, boolean clicked) {
        super.changeClickState(base, player, tickContext, clicked);
        if (base.getBase() != this || !isComplete()) return;
        if (
                !(tickContext.getLoop() instanceof SchedulerLoop) &&
                canBePrecise() &&
                clicked &&
                isAvailableIn(player, base.stat("fire_delay"), FIRE_ACTION)
        )
            callEvent(GunComponent.class, i -> i.fire(base, player));
    }

    @Override
    public void interact(ItemEvent base, PlayerInteractEvent event) {
        super.interact(base, event);
        if (base.getBase() != this || !isComplete()) return;
        Player player = event.getPlayer();
        if (FrameworkUtils.isDropping(player)) return;
        if (player.getOpenInventory().getType() != InventoryType.CRAFTING) return;
        if (CalibreUtils.isLeftClick(event)) {
            List<Sight> sights = base.stat("sights");
            if (player.isSneaking() && aiming && sights.size() > 1) {
                if (!isAvailableIn(player, base.stat("change_sight_delay"), CHANGE_SIGHT_ACTION)) return;
                callEvent(GunComponent.class, i -> i.changeSight(base, player, (sightIndex + 1) % sights.size()));
            } else if (isAvailableIn(player, base.stat("aim_" + (aiming ? "out" : "in") + "_delay"), AIM_ACTION))
                callEvent(GunComponent.class, i -> i.toggleAiming(base, player, getSight(base), !aiming));
        } else {
            if (!canBePrecise() && isAvailableIn(player, base.stat("fire_delay"), FIRE_ACTION))
                callEvent(GunComponent.class, i -> i.fire(base, player));
        }
    }

    @Override
    public void swap(ItemEvent base, PlayerSwapHandItemsEvent event) {
        super.swap(base, event);
        if (base.getBase() != this || !isComplete()) return;
        event.setCancelled(true);
        Player player = event.getPlayer();
        if (aimSetting(base, player, "change_fire_mode") && isAvailableIn(player, base.stat("change_fire_mode_delay"), CHANGE_FIRE_MODE_ACTION)) {
            List<FireMode> fireModes = base.stat("fire_modes");
            callEvent(GunComponent.class, i -> i.changeFireMode(base, player, fireModes.size() == 0 ? fireModeIndex : (fireModeIndex + 1) % fireModes.size()));
        }
    }

    @Override
    public void drop(ItemEvent base, PlayerDropItemEvent event) {
        super.drop(base, event);
        if (base.getBase() != this || !isComplete()) return;
        Player player = event.getPlayer();
        if (FrameworkUtils.isClicking(event.getPlayer())) return;
        if (aimSetting(base, player, "reload") && isAvailable(player))
            callEvent(GunComponent.class, i -> i.reload(base, player));
        Item drop = event.getItemDrop();
        ItemStack stack = drop.getItemStack().clone();
        updateItem(player, stack);
        int slot = player.getInventory().getHeldItemSlot();
        Bukkit.getScheduler().scheduleSyncDelayedTask(getPlugin(), () -> {
            if (player.getInventory().getHeldItemSlot() == slot)
                updateItem(player, stack);
        }, 2);
        drop.remove();
    }

    @Override
    public void click(ItemEvent base, InventoryClickEvent event) {
        super.click(base, event);
        if (base.getBase() != this || !isComplete()) return;
        if (aiming)
            callEvent(GunComponent.class, i -> i.toggleAiming(base, (Player)event.getWhoClicked(), getSight(base), false));
    }

    @Override
    public void move(ItemEvent base, PlayerMoveEvent event) {
        super.move(base, event);
        if (base.getBase() != this || !isComplete()) return;
        Location from = event.getFrom();
        Location to = event.getTo();
        if (
                Double.compare(from.getX(), to.getX()) == 0
                        && Double.compare(from.getZ(), to.getZ()) == 0
        ) return;
        Player player = event.getPlayer();
        PlayerData data = getPlugin().getPlayerData(player);
        data.setSpread(data.getSpread() + base.stat(player.isSprinting() ? "sprint_spread" : "move_spread", double.class));
    }

    @Override
    public void jump(ItemEvent base, PlayerJumpEvent event) {
        super.jump(base, event);
        if (base.getBase() != this || !isComplete()) return;
        PlayerData data = getPlugin().getPlayerData(event.getPlayer());
        data.setSpread(data.getSpread() + base.stat("jump_spread", double.class));
    }

    public void fire(ItemEvent base, Player player) {
        if (base.getBase() != this || !isComplete()) return;
        int shots = base.stat("shots", int.class);
        shootAt = new long[shots];
        for (int i = 0; i < shots; i++)
            shootAt[i] = System.currentTimeMillis() + (base.stat("shot_delay", long.class) * i);
        availableIn(player, base.stat("fire_delay"), FIRE_ACTION);
        updateItem(player);
    }

    public void fireOnce(ItemEvent base, Player player) {
        // Setup
        Location location = getAbsoluteLocation(base, player);
        if (location == null) return;
        if (!base.stat("can_fire_underwater", boolean.class) && (player.isSwimming() || CalibreUtils.getOptionalEyePosition(player).getBlock().getType() == Material.WATER)) {
            shootAt = null;
            updateItem(player);
            return;
        }

        // Get used chamber slot
        ChamberSlot chamberSlot = null;
        ChamberComponent chamber = null;
        for (ChamberSlot slot : getChamberSlots(base)) {
            if (slot.get() != null) {
                chamberSlot = slot;
                chamber = slot.get();
                break;
            }
        }
        if (chamber == null) {
            if (aimSetting(base, player, "chamber"))
                callEvent(GunComponent.class, i -> i.chamber(base, player));
            shootAt = null;
            return;
        }

        // Fire
        fireSuccess(base, player, location, chamber, chamberSlot);
    }

    public void fireSuccess(ItemEvent base, Player player, Location location, ChamberComponent chamber, ChamberSlot chamberSlot) {
        ParticleData.spawn(base.stat("shot_particle", ParticleData[].class), location);
        SoundData.play(base.stat("shot_sound", SoundData[].class), location);

        // Do spread calculations
        Vector direction = location.getDirection().normalize().multiply(base.stat("muzzle_velocity", double.class));
        spreadRotateVector(direction, () -> getMaxSpread(base, player));
        double projectileSpread = base.stat("projectile_spread");
        for (int i = 0; i < base.stat("projectiles", int.class); i++)
            chamber.next(base, this, player, location, spreadRotateVector(direction.clone(), () -> projectileSpread));

        // Recoil
        PlayerData data = getPlugin().getPlayerData(player);
        double spread = getMaxSpread(base, player);
        data.getRecoil().add(
                base.stat("recoil", Vector2.class)
                        .clone()
                        .multiply((spread / base.stat("spread", double.class)) * base.stat("spread_recoil_multiplier", double.class))
        );
        data.setSpread(data.getSpread() + base.stat("firing_spread", double.class));

        // Casing
        RegisteredServiceProvider<CalibreCasingService> casing = Bukkit.getServicesManager().getRegistration(CalibreCasingService.class);
        if (casing != null)
            casing.getProvider().spawnCasing(base, player, this, location, chamber, chamberSlot);

        // Mob awareness
        double awarenessRadius = base.stat("awareness_radius");
        if (awarenessRadius > 0 && player.getGameMode() != GameMode.CREATIVE) {
            location.getNearbyLivingEntities(awarenessRadius).forEach(entity -> {
                if (entity instanceof Monster && ((Monster)entity).getTarget() == null) {
                    ((Monster)entity).setTarget(player);
                }
            });
        }

        // Chamber
        if (base.stat("manual_chamber"))
            chamberSlot.set(null);
        else {
            AmmoComponent ammo = null;
            for (AmmoSlot slot : getAmmoSlots(base)) {
                AmmoComponent test = slot.get();
                if (test == null) continue;
                if (!test.getCaliber().equals(chamberSlot.getCaliber())) continue;
                if (test.getAmount() == 0) continue;
                ammo = test;
            }
            ChamberComponent nextChamber = ammo == null ? null : ammo.next();
            if (nextChamber == null || chamberSlot.isCompatible(nextChamber))
                chamberSlot.set(nextChamber);
            else
                SoundData.play(base.stat("empty_sound", SoundData[].class), location);
        }

        updateItem(player);
        startAnimation(base, player, "shoot");
    }

    public void chamber(ItemEvent base, Player player) {
        if (base.getBase() != this || !isComplete()) return;
        Location location = getBarrelLocation(base, player);
        if (location == null) return;
        boolean success = false;
        for (ChamberSlot chamberSlot : getChamberSlots(base)) {
            if (chamberSlot.get() != null) continue;
            for (AmmoSlot ammoSlot : getAmmoSlots(base)) {
                if (ammoSlot.get() == null) continue;
                AmmoComponent ammo = ammoSlot.get();
                if (!ammo.getCaliber().equals(chamberSlot.getCaliber())) continue;
                if (!ammo.hasNext()) continue;
                if (!chamberSlot.isCompatible(ammo.peek())) continue;
                chamberSlot.set(ammo.next());
                success = true;
                break;
            }
        }
        PlayerData data = getPlugin().getPlayerData(player);
        if (!success && getPlugin().setting("gun.auto_reload", boolean.class, true) && data.getLastAction() == FIRE_ACTION) {
            callEvent(GunComponent.class, i -> i.reload(base, player));
            updateItem(player);
            return;
        }
        SoundData.play(base.stat(success ? "chamber_sound" : "empty_sound", SoundData[].class), location);
        availableIn(player, base.stat((success ? "chamber" : "empty") + "_delay"), CHAMBER_ACTION);
        updateItem(player);
        startAnimation(base, player, success ? "chamber" : "empty");
    }

    public void toggleAiming(ItemEvent base, Player player, Sight sight, boolean aiming) {
        if (base.getBase() != this || !isComplete()) return;
        if (sight == null) return;
        Location location = getBarrelLocation(base, player);
        if (location == null) return;
        this.aiming = aiming;
        sight.apply(base, player, aiming);
        base.updateStats();
        SoundData.play(base.stat("aim_" + (aiming ? "in" : "out") + "_sound", SoundData[].class), location);
        updateItem(player);
        startAnimation(base, player, "aim_" + (aiming ? "in" : "out")).at("end", () -> startAnimation(base, player, "sight"));
    }

    public void changeFireMode(ItemEvent base, Player player, int newIndex) {
        if (base.getBase() != this || !isComplete()) return;
        if (!aimSetting(base, player, "change_fire_mode")) return;

        Location location = getBarrelLocation(base, player);
        if (location == null) return;
        int old = fireModeIndex;
        fireModeIndex = newIndex;
        if (old == fireModeIndex) return;
        SoundData.play(base.stat("change_fire_mode_sound", SoundData[].class), location);
        updateItem(player);
        startAnimation(base, player, "change_fire_mode");
    }

    public void changeSight(ItemEvent base, Player player, int newIndex) {
        if (base.getBase() != this || !isComplete()) return;
        Location location = getBarrelLocation(base, player);
        if (location == null) return;
        int old = sightIndex;
        Sight oldSight = getSight(base);
        sightIndex = newIndex;
        if (old == sightIndex) return;
        SoundData.play(base.stat("change_sight_sound", SoundData[].class), location);

        if (oldSight != null)
            oldSight.apply(base, player, false);
        Sight newSight = getSight(base);
        if (newSight != null)
            newSight.apply(base, player, true);

        updateItem(player);
        startAnimation(base, player, "change_sight");
    }

    public void reload(ItemEvent base, Player player) {
        if (base.getBase() != this || !isComplete()) return;
        if (!aimSetting(base, player, "reload")) return;

        Location location = getBarrelLocation(base, player);
        if (location == null) return;

        List<AmmoSlot> ammoSlots = getAmmoSlots(base);
        if (ammoSlots.size() == 0) {
            List<ChamberSlot> chamberSlots = getChamberSlots(base);
            if (chamberSlots.size() == 0) return;
            chamberSlots.get(0).reload(base, player, this);
        } else
            ammoSlots.get(0).reload(base, player, this);
    }

    public void finishReload(ItemEvent base, Player player) {
        startAnimation(base, player, "finish_reload");
        if (!getPlugin().setting("gun.auto_chamber_after_reload", boolean.class, true)) return;
        autoChamber(base, player);
    }

    public Location getBarrelLocation(ItemEvent base, Player player) { return CalibreUtils.getViewOffset(player, base.stat("barrel_offset")); }
    public Location getCheckedBarrelLocation(ItemEvent base, Player player) { return CalibreUtils.checkObstructed(player, getBarrelLocation(base, player)); }
    public Location getAbsoluteLocation(ItemEvent base, Player player) { return zero(base, getCheckedBarrelLocation(base, player)); }

    public Location zero(ItemEvent base, Location location) {
        if (location == null) return null;
        if (base.stat("zero_distance", double.class) <= 0) return location;
        return CalibreUtils.zero(location, base.stat("muzzle_velocity"), base.stat("zero_distance"), 0, base.stat("gravity"));
    }

    public double getMaxSpread(ItemEvent base, Player player) {
        double spread = base.stat("spread");
        spread += getPlugin().getPlayerData(player).getSpread();
        if (player.isSneaking())
            spread *= base.stat("sneak_spread_multiplier", double.class);
        if (aiming)
            spread *= base.stat("aim_spread_multiplier", double.class);
        return spread;
    }

    public double getSpread(ItemEvent base, Player player) { return random.nextGaussian() * getMaxSpread(base, player); }

    public Vector spreadRotateVector(Vector vector, Supplier<Double> spread) {
        vector.rotateAroundX(Math.toRadians(random.nextGaussian() * spread.get()));
        vector.rotateAroundY(Math.toRadians(random.nextGaussian() * spread.get()));
        vector.rotateAroundZ(Math.toRadians(random.nextGaussian() * spread.get()));
        return vector;
    }
}
