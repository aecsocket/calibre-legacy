package me.aecsocket.calibre.item.gun.chamber;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.Reloadable;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.component.Component;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ChamberSlot extends CalibreComponentSlot implements Reloadable {
    private String caliber;

    public String getCaliber() { return caliber; }
    public void setCaliber(String caliber) { this.caliber = caliber; }

    @Override public ChamberComponent get() { return (ChamberComponent)super.get(); }

    @Override
    public String getName(@Nullable CommandSender sender, String slotName) {
        CalibrePlugin plugin = getPlugin();
        return plugin.gen(sender, "slot.chamber.name",
                plugin.gen(sender, "slot." + slotName),
                plugin.gen(sender, "caliber." + caliber)
        );
    }

    @Override
    public boolean isCompatible(@NotNull Component raw) {
        return super.isCompatible(raw)
                && raw instanceof ChamberComponent
                && ((ChamberComponent)raw).getCaliber().equals(caliber);
    }

    @Override
    public void reload(ItemEvent base, Player player, GunComponent gun) {
        Location location = player.getEyeLocation();
        if (get() == null) {
            // Load
            CalibreUtils.SearchContext result = CalibreUtils.findItem(getPlugin(), player, item -> item instanceof ChamberComponent && isCompatible((CalibreComponent)item));
            if (result == null) return;

            SoundData.play(base.stat("load_sound", SoundData[].class), location);
            gun.startAnimation(base, player, "load");

            gun.availableIn(player, base.stat("load_delay", long.class), GunComponent.LOAD_ACTION, () -> {
                if (!result.isValid(player)) return;
                PlayerInventory inv = player.getInventory();
                inv.setItem(result.getSlot(), result.getStack().subtract());
                set((ChamberComponent)result.getResult());
                gun.updateItem(player);
                gun.finishReload(base, player);
            });
        } else {
            // Unload
            SoundData.play(base.stat("unload_sound", SoundData[].class), location);
            gun.startAnimation(base, player, "unload");

            gun.availableIn(player, base.stat("unload_delay", long.class), GunComponent.UNLOAD_ACTION, () -> {
                boolean canLoad = CalibreUtils.findItem(getPlugin(), player, item -> item instanceof ChamberComponent && isCompatible((CalibreComponent)item)) != null;
                CalibreUtils.safeGiveItem(player, get().create(player, 1));
                set(null);
                gun.updateItem(player);
                if (canLoad)
                    reload(base, player, gun);
                else
                    gun.finishReload(base, player);
            });
        }
    }
}
