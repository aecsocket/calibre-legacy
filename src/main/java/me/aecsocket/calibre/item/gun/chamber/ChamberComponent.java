package me.aecsocket.calibre.item.gun.chamber;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.ItemDescription;
import me.aecsocket.calibre.item.component.ActiveStatModifier;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.ammo.AmmoComponent;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Objects;

public abstract class ChamberComponent extends GunComponent implements ActiveStatModifier {
    private StatData activeStats;
    private String caliber;
    private String prefix;
    private String icon;

    private ItemDescription casingItem;
    private int casingLifetime;
    private String casingSoundGroup;

    @Override
    public StatData getActiveStats() { return activeStats; }
    @Override
    public void setActiveStats(StatData activeStats) { this.activeStats = activeStats; }

    public String getCaliber() { return caliber; }
    public void setCaliber(String caliber) { this.caliber = caliber; }

    public String getPrefix() { return prefix; }
    public void setPrefix(String prefix) { this.prefix = prefix; }

    public String getIcon() { return icon; }
    public void setIcon(String icon) { this.icon = icon; }

    public ItemDescription getCasingItem() { return casingItem; }
    public void setCasingItem(ItemDescription casingItem) { this.casingItem = casingItem; }

    public int getCasingLifetime() { return casingLifetime; }
    public void setCasingLifetime(int casingLifetime) { this.casingLifetime = casingLifetime; }

    public String getCasingSoundGroup() { return casingSoundGroup; }
    public void setCasingSoundGroup(String casingSoundGroup) { this.casingSoundGroup = casingSoundGroup; }

    public String createIcon() { return ChatColor.translateAlternateColorCodes('&', prefix + icon); }

    public abstract void next(ItemEvent base, GunComponent gun, Player player, Location location, Vector direction);

    @Override
    public void release(ItemEvent base, InventoryClickEvent event) {
        super.release(base, event);

        Player player = (Player)event.getWhoClicked();
        if (player.getGameMode() == GameMode.CREATIVE) return;
        if (!getPlugin().setting("ammo_modify.enable", boolean.class, true)) return;
        ItemStack clickedStack = event.getCurrentItem();
        if (clickedStack == null || clickedStack.getType() == Material.AIR || clickedStack.getAmount() > 1) return;
        event.setCancelled(true);
        CalibreItem clicked = getPlugin().getItem(clickedStack);
        if (!(clicked instanceof AmmoComponent)) return;

        AmmoComponent ammo = (AmmoComponent)clicked;
        if (!ammo.getCaliber().equals(caliber)) return;
        if (ammo.isFull()) return;
        if (!ammo.isAmmoCompatible(this)) return;
        ammo.getAmmo().add(this);

        Inventory inv = event.getClickedInventory();
        int slot = event.getSlot();
        inv.setItem(slot, clicked.create(player, inv.getItem(slot).getAmount()));
        event.getView().setCursor(event.getCursor().subtract());

        SoundData.play(getPlugin().setting("ammo_modify.fill_sound", SoundData[].class, null), player, player.getLocation());
    }

    @Override public ChamberComponent clone() { return (ChamberComponent)super.clone(); }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ChamberComponent that = (ChamberComponent)o;
        return Objects.equals(activeStats, that.activeStats) &&
                Objects.equals(caliber, that.caliber) &&
                Objects.equals(prefix, that.prefix) &&
                Objects.equals(icon, that.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), activeStats, caliber, prefix, icon);
    }
}
