package me.aecsocket.calibre.item.animation;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

public class AnimationFrame {
    private int duration;
    private int data;
    private int damage;
    private boolean offhand;
    private Material material;

    public AnimationFrame(int duration, int data, int damage, boolean offhand, Material material) {
        this.duration = duration;
        this.data = data;
        this.damage = damage;
        this.offhand = offhand;
        this.material = material;
    }

    public int getDuration() { return duration; }
    public void setDuration(int duration) { this.duration = duration; }

    public int getData() { return data; }
    public void setData(int data) { this.data = data; }

    public int getDamage() { return damage; }
    public void setDamage(int damage) { this.damage = damage; }

    public boolean isOffhand() { return offhand; }
    public void setOffhand(boolean offhand) { this.offhand = offhand; }

    public Material getMaterial() { return material; }
    public void setMaterial(Material material) { this.material = material; }

    public void apply(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        if (material != null) {
            stack.setType(material);
            if (stack.getAmount() == 0)
                stack.setAmount(1);
        }
        meta.setCustomModelData(data);
        if (meta instanceof Damageable)
            ((Damageable)meta).setDamage(damage);
        stack.setItemMeta(meta);
    }

    public void apply(CalibrePlugin plugin, Player player) {
        PlayerInventory inv = player.getInventory();
        ItemStack item = (offhand ? inv.getItemInOffHand() : inv.getItemInMainHand()).clone();
        if (!item.hasItemMeta())
            return;
        apply(item);
        if (offhand)
            PlayerProtocol.sendOffhand(plugin, player, item);
        else
            PlayerProtocol.sendHand(plugin, player, item);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
