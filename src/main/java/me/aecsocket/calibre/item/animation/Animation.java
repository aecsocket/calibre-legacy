package me.aecsocket.calibre.item.animation;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.util.List;
import java.util.Map;

public class Animation {
    private List<AnimationFrame> frames;
    private Map<String, Integer> keyframes;

    public Animation(List<AnimationFrame> frames, Map<String, Integer> keyframes) {
        this.frames = frames;
        this.keyframes = keyframes;
    }

    public List<AnimationFrame> getFrames() { return frames; }
    public void setFrames(List<AnimationFrame> frames) { this.frames = frames; }

    public Map<String, Integer> getKeyframes() { return keyframes; }
    public void setKeyframes(Map<String, Integer> keyframes) { this.keyframes = keyframes; }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
