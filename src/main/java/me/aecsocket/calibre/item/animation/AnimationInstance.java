package me.aecsocket.calibre.item.animation;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.util.nms.PlayerProtocol;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.loop.Tickable;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnimationInstance implements Tickable {
    private final Animation animation;
    private final CalibrePlugin plugin;
    private Player player;
    private int index;
    private int frameTime;
    private AnimationFrame frame;

    private final Map<String, Runnable> keyframeActions = new HashMap<>();

    public AnimationInstance(Animation animation, CalibrePlugin plugin, Player player) {
        this.animation = animation;
        this.plugin = plugin;
        this.player = player;
        updateFrame();
    }

    private void updateFrame() {
        List<AnimationFrame> frames = getFrames();
        if (frames != null && index < frames.size())
            frame = frames.get(index);
    }

    public Animation getAnimation() { return animation; }
    public CalibrePlugin getPlugin() { return plugin; }

    public Player getPlayer() { return player; }
    public void setPlayer(Player player) { this.player = player; }

    public int getIndex() { return index; }
    public void setIndex(int index) { this.index = index; updateFrame(); }

    public int getFrameTime() { return frameTime; }
    public void setFrameTime(int frameTime) { this.frameTime = frameTime; }

    public AnimationFrame getFrame() { return frame; }

    public Map<String, Runnable> getKeyframeActions() { return keyframeActions; }
    public AnimationInstance at(String frame, Runnable run) { keyframeActions.put(frame, run); return this; }

    public List<AnimationFrame> getFrames() { return animation == null ? null : animation.getFrames(); }
    public Map<String, Integer> getKeyframes() { return animation == null ? null : animation.getKeyframes(); }
    public boolean isFinished() { return getFrames() == null || index >= getFrames().size(); }

    public void apply() {
        if (frame == null)
            PlayerProtocol.sendHand(plugin, player, player.getInventory().getItemInMainHand());
        else
            frame.apply(plugin, player);
    }

    @Override
    public void tick(TickContext context) {
        if (frame == null && index == 0 && keyframeActions.containsKey("end") && (getFrames() == null || index == getFrames().size())) {
            keyframeActions.get("end").run();
            index = 1;
        }
        if (isFinished())
            return;
        if (index == 0 && frameTime == 0 && frame != null)
            frame.apply(plugin, player);
        while (frame != null && index < getFrames().size() && frameTime >= frame.getDuration()) {
            ++index;
            frameTime = 0;

            Map<String, Integer> keyframes = getKeyframes();
            keyframeActions.forEach((frame, run) -> {
                if (
                        (keyframes.containsKey(frame) && keyframes.get(frame) == index)
                        || (frame.equals("end") && (getFrames() == null || index == getFrames().size()))
                )
                    run.run();
            });

            updateFrame();
            apply();
        }
        ++frameTime;
    }
}
