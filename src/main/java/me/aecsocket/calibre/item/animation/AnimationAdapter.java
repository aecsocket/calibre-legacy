package me.aecsocket.calibre.item.animation;

import com.google.gson.*;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;
import org.bukkit.Material;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnimationAdapter implements UtilJsonAdapter, JsonDeserializer<Animation> {
    @Override
    public Animation deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = assertArray(json);
        List<AnimationFrame> frames = new ArrayList<>();
        Map<String, Integer> keyframes = new HashMap<>();
        int idx = 0;
        for (JsonElement element : array) {
            if (element.isJsonArray()) {
                // Basic frame
                JsonArray frame = element.getAsJsonArray();
                frames.add(new AnimationFrame(
                        frame.get(0).getAsInt(),
                        frame.get(1).getAsInt(),
                        frame.size() >= 3 ? frame.get(2).getAsInt() : 0,
                        frame.size() >= 4 && frame.get(3).getAsBoolean(),
                        frame.size() >= 5 ? context.deserialize(frame.get(4), Material.class) : null));
                ++idx;
            }
            if (element.isJsonObject()) {
                // Complex frame
                JsonObject frame = element.getAsJsonObject();
                if (frame.has("from")) {
                    // Range frames
                    int data = get(frame, "data").getAsInt();
                    int from = get(frame, "from").getAsInt();
                    int duration = getOrDefault(frame, "duration", new JsonPrimitive(1)).getAsInt();
                    int step = getOrDefault(frame, "step", new JsonPrimitive(1)).getAsInt();
                    boolean offhand = getOrDefault(frame, "offhand", new JsonPrimitive(false)).getAsBoolean();
                    Material material = frame.has("material") ? context.deserialize(frame.get("material"), Material.class) : null;
                    for (int i = 0; i <= get(frame, "range").getAsInt(); i++) {
                        frames.add(new AnimationFrame(duration, data, from + (i * step), offhand, material));
                        ++idx;
                    }
                }
            }
            if (element.isJsonPrimitive())
                keyframes.put(element.getAsString(), idx);
        }
        return new Animation(frames, keyframes);
    }
}
