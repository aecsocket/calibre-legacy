package me.aecsocket.calibre.item;

import me.aecsocket.unifiedframework.registry.Identifiable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

public interface SuppliesItem extends Identifiable {
    ItemStack create(@Nullable Player player, int amount);
    String getName(@Nullable CommandSender sender);
}
