package me.aecsocket.calibre.item.event;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public interface EventModifier {
    default void hold(ItemEvent base, Player player, TickContext tickContext) {}
    default void changeClickState(ItemEvent base, Player player, TickContext tickContext, boolean clicked) {}

    default void draw(ItemEvent base, Player player, int slot) {}
    default void holster(ItemEvent base, Player player, int slot) {}

    default void move(ItemEvent base, PlayerMoveEvent event) {}
    default void jump(ItemEvent base, PlayerJumpEvent event) {}

    default void interact(ItemEvent base, PlayerInteractEvent event) {}
    default void swap(ItemEvent base, PlayerSwapHandItemsEvent event) {}
    default void drop(ItemEvent base, PlayerDropItemEvent event) {}

    default void damage(ItemEvent base, EntityDamageByEntityEvent event) {}

    default void click(ItemEvent base, InventoryClickEvent event) {}
    default void release(ItemEvent base, InventoryClickEvent event) {}
}
