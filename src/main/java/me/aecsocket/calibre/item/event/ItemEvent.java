package me.aecsocket.calibre.item.event;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.unifiedframework.stat.StatBased;
import me.aecsocket.unifiedframework.stat.StatData;

public class ItemEvent implements StatBased {
    private CalibreItem base;
    private StatData stats;

    public ItemEvent(CalibreItem base, StatData stats) {
        this.base = base;
        this.stats = stats;
    }

    public ItemEvent(CalibreItem base) { this(base, null); }

    public CalibreItem getBase() { return base; }
    public void setBase(CalibreItem base) { this.base = base; }

    @Override
    public StatData getStats() { return stats; }
    public void setStats(StatData stats) { this.stats = stats; }

    public void updateStats() {
        stats = base.getFinalStats();
    }

    public <T> T stat(String name) { return stats.getValue(name); }
    public <T> T stat(String name, Class<T> type) { return stats.getValue(name, type); }

    public static ItemEvent of(CalibreItem base) {
        ItemEvent event = new ItemEvent(base);
        event.updateStats();
        return event;
    }
}
