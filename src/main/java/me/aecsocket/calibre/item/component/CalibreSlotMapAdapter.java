package me.aecsocket.calibre.item.component;

import com.google.gson.*;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.component.ComponentSlot;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class CalibreSlotMapAdapter implements UtilJsonAdapter, JsonAdapter<CalibreSlotMap> {
    private final CalibrePlugin plugin;
    private CalibreItem base;

    public CalibreSlotMapAdapter(CalibrePlugin plugin) {
        this.plugin = plugin;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public CalibreItem getBase() { return base; }
    public void setBase(CalibreItem base) { this.base = base; }

    private void walk(Map<String, CalibreComponentSlot> slots, JsonSerializationContext context, JsonObject object, String prefix) {
        if (slots == null)
            return;
        slots.forEach((name, slot) -> {
            object.add(prefix + name, slot.get() == null ? new JsonArray() : context.serialize(slot.get().createReference(), ComponentReference.class));
            if (slot.get() != null)
                walk(slot.get().getSlots(), context, object, prefix + name + ".");
        });
    }

    @Override
    public JsonElement serialize(CalibreSlotMap map, Type jType, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        walk(map, context, object, "");
        return object;
    }

    @Override
    public CalibreSlotMap deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = assertObject(json);
        CalibreSlotMap map = new CalibreSlotMap();
        Map<String, ComponentHolder> resolvedHolders = new HashMap<>();
        if (base == null)
            throw new JsonParseException("No base object was set to inherit slots");
        resolvedHolders.put("", base);
        object.entrySet().forEach(entry -> {
            String path = entry.getKey();
            JsonElement data = entry.getValue();

            // Split the sections
            String[] sections = path.split(ComponentHolder.COMPONENT_SEPARATOR_PATTERN);
            if (sections.length == 0) return;
            // Separate the first set of sections and last section
            String[] copy = new String[sections.length - 1];
            System.arraycopy(sections, 0, copy, 0, sections.length - 1);
            String firstSections = String.join(".", copy);
            String lastSection = sections[sections.length - 1];
            if (!resolvedHolders.containsKey(firstSections))
                return;
            ComponentHolder holder = resolvedHolders.get(firstSections);

            // Get the component of the set from the holder
            ComponentSlot raw = holder.getSlot(lastSection);
            if (!(raw instanceof CalibreComponentSlot))
                return;
            CalibreComponentSlot slot = ((CalibreComponentSlot)raw).copy();

            CalibreComponent component = data.isJsonArray() && data.getAsJsonArray().size() == 0 ? null : ((ComponentReference)context.deserialize(data, ComponentReference.class)).create(plugin.getRegistry());
            if (component == null)
                slot.set(null);
            else {
                if (!resolvedHolders.containsKey(path)) {
                    if (component.getSlots() != null)
                        component.getSlots().forEach((sPath, sSlot) -> map.setSlot(path + "." + sPath, sSlot));
                    resolvedHolders.put(path, component);
                }
                component = component.copy();
                if (slot.isCompatible(component))
                    slot.set(component);
            }
            map.setSlot(path, slot);
        });
        return map;
    }
}
