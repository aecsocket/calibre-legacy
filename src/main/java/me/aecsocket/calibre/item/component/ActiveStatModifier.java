package me.aecsocket.calibre.item.component;

import me.aecsocket.unifiedframework.stat.StatData;

public interface ActiveStatModifier {
    StatData getActiveStats();
    void setActiveStats(StatData activeStats);
}
