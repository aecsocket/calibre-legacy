package me.aecsocket.calibre.item.component;

import me.aecsocket.unifiedframework.registry.Registry;

import java.util.Objects;

public class ComponentReference {
    private String id;

    public ComponentReference(String id) {
        this.id = id;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public CalibreComponent create(Registry registry) {
        return
                registry.has(CalibreComponent.TYPE, id, CalibreComponent.class)
                ? ((CalibreComponent)registry.get(CalibreComponent.TYPE, id)).copy()
                : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComponentReference that = (ComponentReference)o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
