package me.aecsocket.calibre.item.component;

import com.google.gson.annotations.SerializedName;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.util.AcceptsPlugin;
import me.aecsocket.unifiedframework.component.Component;
import me.aecsocket.unifiedframework.component.ComponentSlot;
import me.aecsocket.unifiedframework.exception.IncompatibleComponentException;
import me.aecsocket.unifiedframework.gui.core.GUIVector;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CalibreComponentSlot implements ComponentSlot, AcceptsPlugin, Cloneable {
    private transient CalibrePlugin plugin;
    private CalibreComponent component;
    private boolean required;
    private int priority;
    @SerializedName("ids")
    private List<String> compatibleIds;
    @SerializedName("categories")
    private List<String> compatibleCategories;
    GUIVector viewOffset;
    private boolean canFieldModify;

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; }

    @Override
    public CalibreComponent get() { return component; }
    @Override
    public void set(Component component) {
        if (component == null || (component instanceof CalibreComponent && isCompatible(component)))
            this.component = (CalibreComponent)component;
        else
            throw new IncompatibleComponentException("Component " + component + " is not compatible");
    }

    @Override
    public boolean isRequired() { return required; }
    public void setRequired(boolean required) { this.required = required; }

    @Override
    public int getPriority() { return priority; }
    public void setPriority(int priority) { this.priority = priority; }

    public List<String> getCompatibleIds() { return compatibleIds; }
    public void setCompatibleIds(List<String> compatibleIds) { this.compatibleIds = compatibleIds; }

    public List<String> getCompatibleCategories() { return compatibleCategories; }
    public void setCompatibleCategories(List<String> compatibleCategories) { this.compatibleCategories = compatibleCategories; }

    public GUIVector getViewOffset() { return viewOffset; }
    public void setViewOffset(GUIVector viewOffset) { this.viewOffset = viewOffset; }

    public boolean canFieldModify() { return canFieldModify; }
    public void setCanFieldModify(boolean canFieldModify) { this.canFieldModify = canFieldModify; }

    public String getName(@Nullable CommandSender sender, String slotName) {
        return plugin.gen(sender, "slot.type." + (required ? "required" : "normal")) + plugin.gen(sender, "slot." + slotName);
    }

    @Override
    public boolean isCompatible(@NotNull Component raw) {
        if (!(raw instanceof CalibreComponent))
            return false;
        CalibreComponent component = (CalibreComponent)raw;
        return (compatibleIds == null && compatibleCategories == null)
                || (compatibleIds != null && compatibleIds.contains(component.getId()))
                || (compatibleCategories != null && !Collections.disjoint(compatibleCategories, component.getCategories()));
    }

    public ItemStack createIcon(@Nullable Player player, String slotName) {
        ItemStack item = new ItemStack(required ? Material.LIGHT_BLUE_STAINED_GLASS_PANE : Material.GRAY_STAINED_GLASS_PANE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName(player, slotName));
        List<String> lore = meta.getLore() == null ? new ArrayList<>() : meta.getLore();
        if (canFieldModify)
            lore.add(plugin.gen("slot.can_field_modify"));
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    public CalibreComponentSlot clone() { try { return (CalibreComponentSlot)super.clone(); } catch (CloneNotSupportedException e) { return null; } }

    public CalibreComponentSlot copy() {
        CalibreComponentSlot clone = clone();
        clone.component = component == null ? null : component.copy();
        clone.compatibleIds = compatibleIds == null ? null : new ArrayList<>(compatibleIds);
        clone.compatibleCategories = compatibleCategories == null ? null : new ArrayList<>(compatibleCategories);
        clone.viewOffset = viewOffset == null ? null : viewOffset.clone();
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalibreComponentSlot that = (CalibreComponentSlot)o;
        return required == that.required &&
                canFieldModify == that.canFieldModify &&
                Objects.equals(component, that.component) &&
                Objects.equals(compatibleIds, that.compatibleIds) &&
                Objects.equals(compatibleCategories, that.compatibleCategories) &&
                Objects.equals(viewOffset, that.viewOffset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(component, required, compatibleIds, compatibleCategories, viewOffset, canFieldModify);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }
}
