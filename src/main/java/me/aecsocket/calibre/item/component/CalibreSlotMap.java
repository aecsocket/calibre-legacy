package me.aecsocket.calibre.item.component;

import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.registry.Ref;
import me.aecsocket.unifiedframework.registry.Registry;

import java.util.LinkedHashMap;
import java.util.Map;

public class CalibreSlotMap extends LinkedHashMap<String, CalibreComponentSlot> implements ComponentHolder {
    public CalibreSlotMap(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
    public CalibreSlotMap(int initialCapacity) { super(initialCapacity); }
    public CalibreSlotMap() { }
    public CalibreSlotMap(Map<? extends String, ? extends CalibreComponentSlot> m) { super(m); }
    public CalibreSlotMap(int initialCapacity, float loadFactor, boolean accessOrder) { super(initialCapacity, loadFactor, accessOrder); }

    @Override
    public Map<String, CalibreComponentSlot> getSlots() { return this; }

    public LinkedHashMap<String, CalibreComponent> toComponentMap() {
        LinkedHashMap<String, CalibreComponent> map = new LinkedHashMap<>();
        forEach((path, slot) -> {
            if (slot.get() != null)
                map.put(path, slot.get());
        });
        return map;
    }

    public LinkedHashMap<String, Ref<CalibreComponent>> toComponentRefMap(Registry registry) {
        LinkedHashMap<String, Ref<CalibreComponent>> map = new LinkedHashMap<>();
        forEach((path, slot) -> {
            if (slot.get() != null) {
                CalibreComponent component = slot.get();
                map.put(path, registry.getRef(component.getType(), component.getId()));
            }
        });
        return map;
    }
}
