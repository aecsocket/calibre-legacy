package me.aecsocket.calibre.item.component;

import com.google.gson.*;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.unifiedframework.utils.json.JsonAdapter;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class ComponentReferenceAdapter implements UtilJsonAdapter, JsonAdapter<ComponentReference> {
    public interface Serializer { JsonElement serialize(ComponentReference ref, Type jType, JsonSerializationContext context); }
    public interface Deserializer { ComponentReference deserialize(JsonElement json, Type jType, JsonDeserializationContext context); }

    private final Map<Class<? extends ComponentReference>, Serializer> serializers = new HashMap<>();
    private final Map<String, Deserializer> deserializers = new HashMap<>();

    private final Serializer defaultSerializer;
    private final Deserializer defaultDeserializer;

    private final CalibrePlugin plugin;

    public ComponentReferenceAdapter(CalibrePlugin plugin) {
        this.plugin = plugin;
        defaultSerializer = (ref, jType, context) -> new JsonPrimitive(ref.getId());
        defaultDeserializer = (json, jType, context) -> new ComponentReference(json.getAsString());
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public Map<Class<? extends ComponentReference>, Serializer> getSerializers() { return serializers; }
    public Map<String, Deserializer> getDeserializers() { return deserializers; }

    public Serializer getDefaultSerializer() { return defaultSerializer; }
    public Deserializer getDefaultDeserializer() { return defaultDeserializer; }

    @Override
    public JsonElement serialize(ComponentReference ref, Type jType, JsonSerializationContext context) {
        return serializers.getOrDefault(ref.getClass(), defaultSerializer).serialize(ref, jType, context);
    }

    @Override
    public ComponentReference deserialize(JsonElement json, Type jType, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonObject())
            return deserializers.getOrDefault(JsonUtils.get(json.getAsJsonObject(), "type").getAsString(), defaultDeserializer).deserialize(json, jType, context);
        return defaultDeserializer.deserialize(json, jType, context);
    }
}
