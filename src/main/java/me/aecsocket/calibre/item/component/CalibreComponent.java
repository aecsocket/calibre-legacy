package me.aecsocket.calibre.item.component;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.ItemDescription;
import me.aecsocket.calibre.item.data.CalibreStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.unifiedframework.component.Component;
import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.component.ComponentSlot;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class CalibreComponent extends CalibreItem implements Component, ComponentHolder {
    public static final String TYPE = "component";
    public static final String ITEM_TYPE = "component";

    private transient StatData stats;
    private List<String> categories;
    private ItemDescription completeItem;

    public CalibreComponent() {
        this.stats = new StatData(getDefaultStats());
    }

    protected abstract StatMap getDefaultStats();

    @Override
    public StatData getStats() { return stats; }
    public void setStats(StatData stats) { this.stats = stats; }

    public List<String> getCategories() { return categories; }
    public void setCategories(List<String> categories) { this.categories = categories; }

    public ItemDescription getCompleteItem() { return completeItem; }
    public void setCompleteItem(ItemDescription completeItem) { this.completeItem = completeItem; }

    @Override public String getType() { return TYPE; }
    @Override public String getItemType() { return ITEM_TYPE; }
    @Override public String getNameKey() { return super.getNameKey() + (isComplete() ? ".complete" : ""); }

    @Override public boolean isAnimated() { return isComplete() ? completeItem.isAnimated() : super.isAnimated(); }

    public boolean canBePrecise() { return getCompleteItem() != null && getCompleteItem().getMaterial() == Material.SHIELD; }

    public boolean isComplete() {
        if (completeItem == null) return false;
        boolean[] result = {true};
        walk(data -> {
            if (!result[0]) return;
            if (data.getSlot().isRequired() && data.getComponent() == null) result[0] = false;
        });
        return result[0];
    }

    @Override
    public StatData getFinalStats() {
        StatData stats = this.stats.copy();
        // Get all the components in this tree, sorted by priority
        List<CalibreComponent> components = getAllSlots().stream()
                .filter(slot -> slot.get() instanceof CalibreComponent)
                .sorted(Comparator.comparingInt(ComponentSlot::getPriority))
                .map(slot -> (CalibreComponent)slot.get())
                .collect(Collectors.toList());

        // Combine all of the child component stats with our stats
        components.stream()
                .filter(comp -> comp.getStats() != null)
                .forEach(comp -> stats.combine(comp.stats));

        // Let the components do their own stuff with our complete data
        components.add(this);
        components.forEach(comp -> comp.modifyStats(this, stats));
        return stats;
    }

    @Override
    protected ItemDescription getCreatedItem(@Nullable Player player, int amount) {
        return isComplete() ? completeItem : super.getCreatedItem(player, amount);
    }

    public ComponentReference createReference() { return new ComponentReference(getId()); }

    @Override
    public ItemStack createComponentIcon(@Nullable Player player, int amount) {
        ItemDescription old = completeItem;
        completeItem = null;
        ItemStack item = super.createComponentIcon(player, amount);
        completeItem = old;
        return item;
    }

    public void modifyStats(CalibreComponent component, StatData stats) {}

    @Override
    protected List<List<String>> getSections(@Nullable Player player, int amount) {
        List<List<String>> result = super.getSections(player, amount);

        if (getPlugin().setting("item.show_stats", boolean.class, true))
            addSection(result, getStats(player, amount, isComplete()));

        return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    protected List<String> getStats(@Nullable Player player, int amount, boolean complete) {
        StatData stats = complete ? getFinalStats() : this.stats;
        if (stats == null) return null;
        List<String> lines = new ArrayList<>();
        stats.forEach((name, inst) -> {
            if (inst.getRawValue() == null) return;
            if (!(inst.getStat() instanceof CalibreStat)) return;
            CalibreStat stat = (CalibreStat)inst.getStat();
            String text = stat.getText(getPlugin(), name, inst, player, inst.getValue());
            if (text != null)
                lines.add(text);
        });
        return lines;
    }

    @Override
    public void draw(ItemEvent base, Player player, int slot) {
        if (!isComplete()) return;
        super.draw(base, player, slot);
    }

    @Override
    public void release(ItemEvent base, InventoryClickEvent event) {
        super.release(base, event);
        if (base.getBase() != this) return;

        Player player = (Player)event.getWhoClicked();
        ItemStack stack = event.getCurrentItem();
        CalibreItem clicked = getPlugin().getItem(stack);
        Inventory inv = event.getClickedInventory();
        int slot = event.getSlot();
        if (clicked == null) return;

        if (player.getGameMode() == GameMode.CREATIVE) return;
        if (!getPlugin().setting("quick_modify.enable", boolean.class, true)) return;
        if (stack.getType() == Material.AIR || stack.getAmount() > 1) return;
        if (clicked.getSlots() == null) return;
        event.setCancelled(true);

        List<ComponentSlot> candidates = new ArrayList<>();
        clicked.walk(data -> {
            ComponentSlot compSlot = data.getSlot();
            if (!(compSlot instanceof CalibreComponentSlot)) return;
            if (
                    compSlot.get() == null &&
                            compSlot.isCompatible(this) &&
                            (getPlugin().setting("quick_modify.global_enable", boolean.class, false)
                                    || ((CalibreComponentSlot)compSlot).canFieldModify())) {
                candidates.add(compSlot);
            }
        });

        if (candidates.size() == 1) {
            candidates.get(0).set(this);

            inv.setItem(slot, clicked.create(player, stack.getAmount()));
            if (inv == player.getInventory() && player.getInventory().getHeldItemSlot() == slot)
                clicked.updateItem(player);

            event.getView().setCursor(event.getCursor().subtract());
            SoundData.play(getPlugin().setting("quick_modify.combine_sound", SoundData[].class, null), player, player.getLocation());
        }
    }

    @Override public CalibreComponent clone() { return (CalibreComponent)super.clone(); }

    @Override
    public CalibreComponent copy() {
        CalibreComponent clone = (CalibreComponent)super.copy();
        clone.stats = stats == null ? null : stats.copy();
        clone.categories = categories == null ? null : new ArrayList<>(categories);
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CalibreComponent that = (CalibreComponent)o;
        return Objects.equals(stats, that.stats) &&
                Objects.equals(categories, that.categories) &&
                Objects.equals(completeItem, that.completeItem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), stats, categories, completeItem);
    }
}
