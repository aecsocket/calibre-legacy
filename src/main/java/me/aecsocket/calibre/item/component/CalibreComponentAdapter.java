package me.aecsocket.calibre.item.component;

import com.google.gson.*;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.CalibreItemAdapter;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.json.UtilJsonAdapter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class CalibreComponentAdapter extends CalibreItemAdapter<CalibreComponent> implements UtilJsonAdapter, TypeAdapterFactory {
    private final CalibrePlugin plugin;

    private final Map<Type, Supplier<StatMap>> statSuppliers = new HashMap<>();

    public CalibreComponentAdapter(CalibrePlugin plugin) {
        super(plugin);
        this.plugin = plugin;
    }

    public Map<Type, Supplier<StatMap>> getStatSuppliers() { return statSuppliers; }
    public void registerStatSupplier(Type type, Supplier<StatMap> supplier) { statSuppliers.put(type, supplier); }

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        if (!CalibreComponent.class.isAssignableFrom(type.getRawType()))
            return null;

        TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter json, T value) throws IOException { delegate.write(json, value); }

            @Override
            public T read(JsonReader json) throws IOException {
                JsonElement tree = Streams.parse(json);
                T value = delegate.fromJsonTree(tree);
                if (!tree.isJsonObject())
                    return value;
                if (value instanceof CalibreComponent) {
                    CalibreComponent component = (CalibreComponent)value;
                    JsonObject object = tree.getAsJsonObject();
                    Type raw = type.getType();

                    if (object.has("stats") && statSuppliers.containsKey(raw)) {
                        plugin.getStatDataAdapter().setStats(statSuppliers.get(raw).get());
                        component.setStats(gson.getAdapter(StatData.class).fromJsonTree(object.get("stats")));
                    }
                    if (component instanceof ActiveStatModifier && object.has("active_stats") && statSuppliers.containsKey(raw)) {
                        plugin.getStatDataAdapter().setStats(statSuppliers.get(raw).get());
                        ((ActiveStatModifier)component).setActiveStats(gson.getAdapter(StatData.class).fromJsonTree(object.get("active_stats")));
                    }
                }
                return value;
            }
        }.nullSafe();
    }

    @Override protected String getItemType() { return CalibreComponent.ITEM_TYPE; }
    @Override protected String getRegistryType() { return CalibreComponent.TYPE; }
}
