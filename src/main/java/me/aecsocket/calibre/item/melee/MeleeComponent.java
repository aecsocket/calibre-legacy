package me.aecsocket.calibre.item.melee;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.data.SoundDataStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.DecimalStat;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.number.LongStat;
import me.aecsocket.calibre.item.event.ItemAction;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.RayTraceResult;

public class MeleeComponent extends CalibreComponent {
    public static final StatMap STATS = new StatMap(CalibreItem.STATS)
            .add("damage", new DoubleStat().precision(1).defaultSupplier(i -> 0d))

            .add("backstab_threshold", new DoubleStat().precision(1).textSupplier((stat, plugin, name, inst, player, value) -> ((DecimalStat<?>)stat).format(Math.toDegrees(Math.acos(value)) * 2)).defaultSupplier(i -> 0d))
            .add("backstab_multiplier", new DoubleStat().precision(1).defaultSupplier(i -> 1d))

            .add("swing_delay", new LongStat().textSupplier((stat, plugin, name, inst, player, value) -> DecimalStat.DEFAULT_FORMAT.format(1000d / value)).defaultSupplier(inst -> 1L))

            .add("swing_sound", new SoundDataStat().hide());
    public static final ItemAction SWING_ACTION = new ItemAction(){};
    private static int lastDamage;

    @Override protected StatMap getDefaultStats() { return STATS; }

    @Override
    public void interact(ItemEvent base, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (base.getBase() != this || !isComplete()) return;
        if (CalibreUtils.isLeftClick(event) && isAvailableIn(player, base.stat("swing_delay"), SWING_ACTION))
            callEvent(MeleeComponent.class, i -> i.swing(base, player, null));
    }

    @Override
    public void damage(ItemEvent base, EntityDamageByEntityEvent event) {
        if (lastDamage == Bukkit.getCurrentTick()) return;
        if (event instanceof EntityDamageByMeleeEvent) return;
        if (base.getBase() != this || !isComplete()) return;
        event.setCancelled(true);
        if (!(event.getDamager() instanceof Player)) return;
        if (!(event.getEntity() instanceof LivingEntity)) return;
        if (isAvailableIn((Player)event.getDamager(), base.stat("swing_delay"), SWING_ACTION))
            callEvent(MeleeComponent.class, i -> i.swing(base, (Player)event.getDamager(), (LivingEntity)event.getEntity()));
    }

    public void swing(ItemEvent base, Player player, LivingEntity victim) {
        if (base.getBase() != this || !isComplete()) return;
        Location location = player.getEyeLocation();
        SoundData.play(base.stat("swing_sound", SoundData[].class), location);
        startAnimation(base, player, "swing");
        if (victim != null) {
            double dot = player.getLocation().getDirection().dot(victim.getLocation().getDirection());
            double damage = base.stat("damage");
            if (dot > base.stat("backstab_threshold", double.class))
                damage *= base.stat("backstab_multiplier", double.class);
            EntityDamageByMeleeEvent evt = new EntityDamageByMeleeEvent(player, victim, damage, this);
            if (!evt.callEvent()) return;
            RayTraceResult ray = player.getWorld().rayTraceEntities(player.getEyeLocation(), player.getEyeLocation().getDirection(), 10, 0, e -> e == victim);
            lastDamage = Bukkit.getCurrentTick();
            CalibreUtils.damage(victim, ray == null ? null : ray.getHitPosition(), damage, evt);
        }
    }
}
