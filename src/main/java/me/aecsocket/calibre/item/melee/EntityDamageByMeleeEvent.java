package me.aecsocket.calibre.item.melee;

import org.bukkit.entity.Entity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.jetbrains.annotations.NotNull;

public class EntityDamageByMeleeEvent extends EntityDamageByEntityEvent {
    private MeleeComponent melee;

    public EntityDamageByMeleeEvent(@NotNull Entity damager, @NotNull Entity damagee, double damage, MeleeComponent melee) {
        super(damager, damagee, DamageCause.ENTITY_ATTACK, damage);
        this.melee = melee;
    }

    public MeleeComponent getMelee() { return melee; }
}
