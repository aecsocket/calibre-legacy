package me.aecsocket.calibre.item;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.gui.SlotGUI;
import me.aecsocket.calibre.handle.EventHandle;
import me.aecsocket.calibre.item.animation.Animation;
import me.aecsocket.calibre.item.animation.AnimationInstance;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.CalibreComponentSlot;
import me.aecsocket.calibre.item.component.CalibreSlotMap;
import me.aecsocket.calibre.item.data.AnimationStat;
import me.aecsocket.calibre.item.data.SoundDataStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.number.LongStat;
import me.aecsocket.calibre.item.event.EventModifier;
import me.aecsocket.calibre.item.event.ItemAction;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.util.AcceptsPlugin;
import me.aecsocket.calibre.util.PlayerData;
import me.aecsocket.calibre.util.TextUtils;
import me.aecsocket.unifiedframework.component.ComponentHolder;
import me.aecsocket.unifiedframework.item.core.Item;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.registry.Identifiable;
import me.aecsocket.unifiedframework.stat.StatBased;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.SoundData;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.Consumer;

public abstract class CalibreItem implements Item, Identifiable, ComponentHolder, StatBased, EventModifier, AcceptsPlugin, SuppliesItem, Cloneable {
    private static final UUID attributeUuid = new UUID(69, 420);
    public static final StatMap STATS = new StatMap()
            .add("animations", new AnimationStat().hide())

            .add("movement_speed", new DoubleStat().defaultSupplier(i -> 1d))

            .add("draw_delay", new LongStat().textSupplier((stat, plugin, name, inst, player, value) -> new DecimalFormat("0.0").format(value / 1000d)).defaultSupplier(i -> 1L))
            .add("draw_sound", new SoundDataStat().hide());
    public static final ItemAction DRAW_ACTION = new ItemAction(){};
    public static final ItemAction HOLSTER_ACTION = new ItemAction(){};

    private transient CalibrePlugin plugin;
    private String id;
    private ItemDescription item;
    private LinkedHashMap<String, CalibreComponentSlot> slots;

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; }

    @Override
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public ItemDescription getItem() { return item; }
    public void setItem(ItemDescription item) { this.item = item; }

    @Override
    public LinkedHashMap<String, CalibreComponentSlot> getSlots() { return slots; }
    public void setSlots(LinkedHashMap<String, CalibreComponentSlot> slots) { this.slots = slots; }

    public boolean isAnimated() { return item.isAnimated(); }

    public abstract StatData getFinalStats();

    // Utils
    protected NamespacedKey key(String name) { return plugin.key(name); }
    protected NamespacedKey typeKey() { return plugin.typeKey(); }
    protected List<String> lines(String text, String prefix) { return TextUtils.lines(text, prefix); }
    protected List<String> lines(String text) { return TextUtils.lines(text); }

    public String getNameKey() { return getType() + "." + id; }
    public String getName(@Nullable CommandSender sender) { return plugin.gen(sender, getNameKey()); }

    public boolean isAvailable(Player player) { return plugin.getPlayerData(player).isAvailable(); }
    public void availableIn(Player player, long time, ItemAction action, Runnable callback) { plugin.getPlayerData(player).availableIn(time, action, callback); }
    public void availableIn(Player player, long time, ItemAction action) { availableIn(player, time, action, null); }
    public boolean isAvailableIn(Player player, long time, ItemAction action, Runnable callback) {
        if (isAvailable(player)) {
            availableIn(player, time, action, callback);
            return true;
        }
        return false;
    }
    public boolean isAvailableIn(Player player, long time, ItemAction action) { return isAvailableIn(player, time, action, null); }

    public AnimationInstance startAnimation(ItemEvent event, Player player, String name) {
        Map<String, Animation> animations = event.stat("animations");
        Animation animation = animations == null || !animations.containsKey(name) ? null : animations.get(name);
        AnimationInstance instance = new AnimationInstance(animation, plugin, player);
        getPlugin().getPlayerData(player).setAnimation(instance);
        instance.apply();
        return instance;
    }

    private void restore(Player player) {
        PersistentDataContainer data = player.getPersistentDataContainer();
        NamespacedKey key = key("shield_block_delay");
        Integer delay = data.get(key, PersistentDataType.INTEGER);
        if (delay != null) {
            player.setShieldBlockingDelay(delay);
            data.remove(key);
        }
    }

    // Item creation
    @Override
    public ItemStack create(@Nullable Player player, int amount) {
        ItemStack item = getCreatedItem(player, amount).create(amount);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(getName(player));

        PersistentDataContainer data = meta.getPersistentDataContainer();
        data.set(typeKey(), PersistentDataType.STRING, getItemType());
        data.set(key("id"), PersistentDataType.STRING, id);
        if (slots != null)
            data.set(key("slots"), PersistentDataType.STRING, plugin.getGson().toJson(new CalibreSlotMap(slots)));

        StatData stats = getFinalStats();
        meta.setLore(getLore(player, amount));
        meta.addAttributeModifier(
                Attribute.GENERIC_MOVEMENT_SPEED, new AttributeModifier(attributeUuid, "movement_speed",
                        (stats.getValue("movement_speed", double.class) * (item.getType() == Material.SHIELD && player != null && plugin.getPlayerData(player).getHoldTicks() >= 5 ? 4 : 1)) - 1, AttributeModifier.Operation.MULTIPLY_SCALAR_1));
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE);
        meta.setUnbreakable(true);
        modify(data, meta, item, stats, player, amount);
        item.setItemMeta(meta);
        return item;
    }

    public ItemStack createComponentIcon(@Nullable Player player, int amount) {
        ItemStack item = this.item.create(amount);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName(player));
        meta.setLore(getLore(player, amount));
        item.setItemMeta(meta);
        return item;
    }

    protected ItemDescription getCreatedItem(@Nullable Player player, int amount) { return item; }

    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, StatData stats, @Nullable Player player, int amount) {}

    protected List<String> getLore(@Nullable Player player, int amount) {
        List<List<String>> sections = getSections(player, amount);

        List<String> result = new ArrayList<>();
        for (int i = 0; i < sections.size(); i++) {
            result.addAll(sections.get(i));
            if (i < sections.size() - 1)
                result.add("");
        }

        return result;
    }

    protected void addSection(List<List<String>> sections, List<String> toAdd) {
        if (toAdd == null) return;
        if (toAdd.size() == 0) return;
        sections.add(toAdd);
    }

    protected List<List<String>> getSections(@Nullable Player player, int amount) {
        List<List<String>> sections = new ArrayList<>();

        if (plugin.setting("item.show_description", boolean.class, true))
            addSection(sections, getDescription(player, amount));
        if (plugin.setting("item.show_slots", boolean.class, true))
            addSection(sections, getSlots(player, amount));

        return sections;
    }

    protected List<String> getDescription(@Nullable Player player, int amount) {
        return lines(
                plugin.gen(player, getNameKey() + ".description"),
                plugin.gen(player, "description")
        );
    }

    protected List<String> getSlots(@Nullable Player player, int amount) {
        List<String> result = new ArrayList<>();
        walk(data -> {
            CalibreComponentSlot slot = null;
            CalibreComponent component = null;
            if (data.getSlot() instanceof CalibreComponentSlot) slot = (CalibreComponentSlot)data.getSlot();
            if (data.getComponent() instanceof CalibreComponent) component = (CalibreComponent)data.getComponent();
            result.add(
                    plugin.gen(player, "slot",
                            " ".repeat(data.getDepth() * plugin.setting("item.slot_indent_size", int.class, 2)),
                            slot == null ? plugin.gen(player, "slot.unknown") : slot.getName(player, data.getSlotName()),
                            data.getComponent() == null
                            ? plugin.gen(player, "slot.empty")
                            : component == null
                            ? plugin.gen(player, "slot.unknown")
                            : component.getName(player)
                    )
            );
        });
        return result;
    }

    public void updateItem(Player player, ItemStack stack) {
        if (player == null) return;
        PlayerInventory inv = player.getInventory();
        inv.setItemInMainHand(create(player, stack.getAmount()));
        AnimationInstance animation = getPlugin().getPlayerData(player).getAnimation();
        if (animation != null)
            animation.apply();
    }

    public void updateItem(Player player) { updateItem(player, player.getInventory().getItemInMainHand()); }

    // Events
    public <T extends CalibreItem> void callEvent(Class<T> type, Consumer<T> run) {
        run.accept(type.cast(this));
        walk(data -> {
            if (data.getComponent() != null && type.isAssignableFrom(data.getComponent().getClass()))
                run.accept(type.cast(data.getComponent()));
        });
    }

    public <T extends CalibreItem> void callEvent(Class<T> type, EventHandle.EventConsumer<T> run) {
        ItemEvent event = ItemEvent.of(this);
        callEvent(type, i -> run.accept(event, i));
    }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        if (base.getBase() != this) return;
        if (player.isHandRaised()) {
            PersistentDataContainer data = player.getPersistentDataContainer();
            NamespacedKey key = key("shield_block_delay");
            if (!data.has(key, PersistentDataType.INTEGER)) {
                data.set(key, PersistentDataType.INTEGER, player.getShieldBlockingDelay());
                player.setShieldBlockingDelay(Integer.MAX_VALUE);
            }
        } else
            restore(player);
    }

    @Override
    public void draw(ItemEvent base, Player player, int slot) {
        if (base.getBase() != this) return;
        player.getInventory().setHeldItemSlot(slot);
        updateItem(player);
        availableIn(player, base.stat("draw_delay"), DRAW_ACTION);
        SoundData.play(base.stat("draw_sound", SoundData[].class), player.getLocation());
        startAnimation(base, player, "draw");
    }

    @Override
    public void holster(ItemEvent base, Player player, int slot) {
        if (base.getBase() != this) return;
        restore(player);
        Inventory inv = player.getInventory();
        inv.setItem(slot, create(player, inv.getItem(slot).getAmount()));
        PlayerData data = plugin.getPlayerData(player);
        data.setAnimation(null);
        data.availableIn(0, HOLSTER_ACTION);
    }

    @Override
    public void click(ItemEvent base, InventoryClickEvent event) {
        if (base.getBase() != this) return;
        Player player = (Player)event.getWhoClicked();
        PlayerInventory inv = player.getInventory();
        int slot = event.getSlot();
        if (event.getClick() == ClickType.RIGHT) {
            event.setCancelled(true);
            if (event.getClickedInventory() == inv) {
                if (plugin.setting("item.enable_slot_view", boolean.class, true))
                    new SlotGUI(plugin, this, slot, plugin.setting("field_modify.enable", boolean.class, true) && event.getCurrentItem().getAmount() == 1).open(player);
                if (inv.getHeldItemSlot() == slot)
                    updateItem(player, inv.getItem(slot));
            } else {
                if (plugin.setting("item.enable_slot_view", boolean.class, true))
                    new SlotGUI(plugin, this, -1, false).open(player);
            }
        }
    }

    @Override
    public void release(ItemEvent base, InventoryClickEvent event) {
        if (base.getBase() != this) return;
        if (event.getSlot() == event.getWhoClicked().getInventory().getHeldItemSlot())
            draw(base, (Player)event.getWhoClicked(), event.getSlot());
    }

    // Overrides
    @Override public CalibreItem clone() { try { return (CalibreItem)super.clone(); } catch (CloneNotSupportedException e) { return null; } }
    public CalibreItem copy() {
        CalibreItem clone = clone();
        if (this.slots != null) {
            LinkedHashMap<String, CalibreComponentSlot> slots = new LinkedHashMap<>(this.slots);
            this.slots.forEach((id, slot) -> slots.put(id, slot.copy()));
            clone.slots = slots;
        }
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalibreItem that = (CalibreItem)o;
        return Objects.equals(plugin, that.plugin) &&
                id.equals(that.id) &&
                item.equals(that.item) &&
                Objects.equals(slots, that.slots);
    }

    @Override
    public String toString() { return new ReflectionToStringBuilder(this).toString(); }

    @Override public int hashCode() {
        return Objects.hash(id, item);
    }
}
