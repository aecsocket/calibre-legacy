package me.aecsocket.calibre.item;

public class ItemCreationException extends RuntimeException {
    public ItemCreationException(String message) { super(message); }
}
