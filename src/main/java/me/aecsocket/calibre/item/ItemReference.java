package me.aecsocket.calibre.item;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.calibre.util.AcceptsPlugin;
import me.aecsocket.calibre.util.TextUtils;
import me.aecsocket.unifiedframework.component.ComponentSlot;
import me.aecsocket.unifiedframework.registry.Identifiable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;

public class ItemReference implements Identifiable, AcceptsPlugin, SuppliesItem, Cloneable {
    public static final String TYPE = "item";

    private transient CalibrePlugin plugin;
    private String id;
    private ComponentReference base;
    private LinkedHashMap<String, ComponentReference> components;

    public ItemReference(CalibrePlugin plugin, String id, ComponentReference base, LinkedHashMap<String, ComponentReference> components) {
        this.plugin = plugin;
        this.id = id;
        this.base = base;
        this.components = components;
    }

    public ItemReference() {}

    @Override
    public CalibrePlugin getPlugin() { return plugin; }
    @Override
    public void setPlugin(CalibrePlugin plugin) { this.plugin = plugin; }

    @Override
    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public ComponentReference getBase() { return base; }
    public void setBase(ComponentReference base) { this.base = base; }

    public LinkedHashMap<String, ComponentReference> getComponents() { return components; }
    public void setComponents(LinkedHashMap<String, ComponentReference> components) { this.components = components; }

    @Override
    public String getType() { return TYPE; }

    public CalibreItem create() throws ItemCreationException {
        CalibreItem item = base.create(plugin.getRegistry());
        if (item == null) throw new ItemCreationException(TextUtils.format("Item {0} does not exist", base.getId()));
        if (components == null) return item;
        components.forEach((path, ref) -> {
            CalibreComponent component = ref.create(plugin.getRegistry());
            if (component == null) throw new ItemCreationException(TextUtils.format("Component {0} for path {1} in {2} does not exist", ref.getId(), path, base.getId()));
            ComponentSlot slot = item.getSlot(path);
            if (slot == null) throw new ItemCreationException(TextUtils.format("Slot for path {0} in {1} does not exist", path, base.getId()));
            if (!slot.isCompatible(component)) throw new ItemCreationException(TextUtils.format("Component {0} for path {1} in {2} is not compatible", ref.getId(), path, base.getId()));
            slot.set(component);
        });
        return item;
    }

    @Override
    public ItemStack create(@Nullable Player player, int amount) {
        CalibreItem item = create();
        return item.create(player, amount);
    }

    @Override
    public String getName(@Nullable CommandSender sender) { return plugin.gen(sender, "item." + id); }

    public ItemReference clone() { try { return (ItemReference)super.clone(); } catch (CloneNotSupportedException e) { return null; } }
}
