package me.aecsocket.calibre.item.throwable.explosive;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.item.throwable.triggerable.TriggerableProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class ExplosiveProjectile extends TriggerableProjectile {
    public ExplosiveProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity, int elapsed) {
        super(location, velocity, base, throwable, entity, elapsed);
    }

    @Override
    public void trigger(TickContext tickContext) {
        getWorld().createExplosion(getLocation(),
                stat("explosion_power"),
                stat("explosion_creates_fire"),
                stat("explosion_breaks_blocks")); // TODO custom explosion algo
        tickContext.removeTickable();
    }
}
