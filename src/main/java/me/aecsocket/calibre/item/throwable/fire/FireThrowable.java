package me.aecsocket.calibre.item.throwable.fire;

import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.Stats;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.triggerable.TriggerableThrowable;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class FireThrowable extends TriggerableThrowable {
    public static final StatMap STATS = new StatMap(TriggerableThrowable.STATS, Stats.FIRE);

    @Override protected StatMap getDefaultStats() { return STATS; }

    @Override
    protected Projectile createProjectile(ItemEvent base, Player player, Location location, Vector velocity) {
        return new FireProjectile(location, velocity, base, this, player, getElapsed());
    }
}
