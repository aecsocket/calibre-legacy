package me.aecsocket.calibre.item.throwable.triggerable;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.SimpleItemAdapter;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class TriggerableAdapter<T extends TriggerableThrowable> extends SimpleItemAdapter<T> {
    public TriggerableAdapter(CalibrePlugin plugin, String itemType, String registryType) {
        super(plugin, itemType, registryType);
    }

    public TriggerableAdapter(CalibrePlugin plugin) { this(plugin, TriggerableThrowable.ITEM_TYPE, TriggerableThrowable.TYPE); }

    @Override
    protected void modify(PersistentDataContainer data, ItemStack stack, T item) {
        item.setPrimed(get(data, "primed", PersistentDataType.BYTE, (byte)0) == 1);
        item.setElapsed(get(data, "elapsed", PersistentDataType.INTEGER, 0));
    }
}
