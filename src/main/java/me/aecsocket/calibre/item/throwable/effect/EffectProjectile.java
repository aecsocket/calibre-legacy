package me.aecsocket.calibre.item.throwable.effect;

import me.aecsocket.calibre.effect.GenericEffect;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.item.throwable.triggerable.TriggerableProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class EffectProjectile extends TriggerableProjectile {
    private GenericEffect effect;

    public EffectProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity, int elapsed) {
        super(location, velocity, base, throwable, entity, elapsed);
    }

    protected GenericEffect create() {
        return getPlugin().getWorldEffects().addEffect(new GenericEffect(
                stat("particle"), stat("sound"),
                stat("start_particle"), stat("start_sound"),
                getLocation(), stat("duration"))
        );
    }

    @Override
    public void trigger(TickContext tickContext) {
        if (effect == null)
            effect = create();
        else
            effect.setLocation(getLocation());
        if (effect.isFinished())
            tickContext.removeTickable();
    }
}
