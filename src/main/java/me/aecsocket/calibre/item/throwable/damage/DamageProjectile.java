package me.aecsocket.calibre.item.throwable.damage;

import me.aecsocket.calibre.effect.DamageEffect;
import me.aecsocket.calibre.effect.GenericEffect;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.item.throwable.effect.EffectProjectile;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class DamageProjectile extends EffectProjectile {
    public DamageProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity, int elapsed) {
        super(location, velocity, base, throwable, entity, elapsed);
    }

    @Override
    protected GenericEffect create() {
        return getPlugin().getWorldEffects().addEffect(new DamageEffect(
                stat("particle"), stat("sound"),
                stat("start_particle"), stat("start_sound"),
                getLocation(), stat("duration"),
                stat("radius"), stat("damage"), stat("effects"), getEntity(), stat("damage_delay"), stat("damage_cause"), stat("damage_penetrates"))
        );
    }
}
