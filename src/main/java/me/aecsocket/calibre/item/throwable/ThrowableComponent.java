package me.aecsocket.calibre.item.throwable;

import me.aecsocket.calibre.item.CalibreItem;
import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.data.ParticleDataStat;
import me.aecsocket.calibre.item.data.SoundDataStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.number.IntegerStat;
import me.aecsocket.calibre.item.data.number.LongStat;
import me.aecsocket.calibre.item.event.ItemAction;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.util.CalibreUtils;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.stat.StatData;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

public abstract class ThrowableComponent extends CalibreComponent {
    public static final String ITEM_TYPE = "throwable";
    public static final StatMap STATS = new StatMap(CalibreItem.STATS)
            .add("throw_power", new DoubleStat().precision(1).defaultSupplier(i -> 0d))

            .add("bounce", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("drag", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("gravity", new DoubleStat().precision(1).defaultSupplier(i -> Projectile.GRAVITY))
            .add("steps", new IntegerStat().defaultSupplier(i -> 2))
            .add("projectile_size", new DoubleStat().precision(1).defaultSupplier(i -> 0d))

            .add("prime_delay", new LongStat().hide())
            .add("throw_delay", new LongStat().hide())

            .add("prime_sound", new SoundDataStat().hide())
            .add("throw_sound", new SoundDataStat().hide())
            .add("hit_sound", new SoundDataStat().hide())

            .add("trail_particle", new ParticleDataStat().hide());
    public static final ItemAction PRIME_ACTION = new ItemAction(){};
    public static final ItemAction THROW_ACTION = new ItemAction(){};

    private boolean primed;

    @Override protected StatMap getDefaultStats() { return STATS; }

    @Override public String getItemType() { return ITEM_TYPE; }

    public boolean isPrimed() { return primed; }
    public void setPrimed(boolean primed) { this.primed = primed; }

    @Override
    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, StatData stats, @Nullable Player player, int amount) {
        super.modify(data, meta, item, stats, player, amount);
        data.set(key("primed"), PersistentDataType.BYTE, (byte)(primed ? 1 : 0));
    }

    protected void subtractItem(Player player) {
        if (player.getGameMode() != GameMode.CREATIVE) {
            PlayerInventory inv = player.getInventory();
            inv.setItemInMainHand(inv.getItemInMainHand().subtract());
        }
    }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (base.getBase() != this || !isComplete()) return;
        boolean clicked = player.isHandRaised();
        if (primed && !clicked && isAvailableIn(player, base.stat("throw_delay"), THROW_ACTION))
            callEvent(ThrowableComponent.class, i -> throwItem(base, player));
        else if (!primed && clicked && isAvailableIn(player, base.stat("prime_delay"), PRIME_ACTION))
            prime(base, player);
    }

    @Override
    public void holster(ItemEvent base, Player player, int slot) {
        super.holster(base, player, slot);
        if (base.getBase() != this || !isComplete() || !primed) return;
        spawn(base, player, 0);
        primed = false;
        updateItem(player);
        subtractItem(player);
    }

    @Override
    public void interact(ItemEvent base, PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (base.getBase() != this || !isComplete()) return;
        if (canBePrecise()) return;
        if (!CalibreUtils.isLeftClick(event)) {
            if (primed && isAvailableIn(player, base.stat("throw_delay"), THROW_ACTION))
                throwItem(base, player);
            else if (isAvailableIn(player, base.stat("prime_delay"), PRIME_ACTION))
                prime(base, player);
        }
    }

    public void prime(ItemEvent base, Player player) {
        if (base.getBase() != this || !isComplete()) return;
        Location location = player.getEyeLocation();
        primed = true;
        SoundData.play(base.stat("prime_sound", SoundData[].class), location);
        updateItem(player);
        startAnimation(base, player, "prime");
    }

    public void throwItem(ItemEvent base, Player player) {
        if (base.getBase() != this || !isComplete()) return;
        Location location = player.getEyeLocation();
        spawn(base, player, 1);
        primed = false;
        SoundData.play(base.stat("throw_sound", SoundData[].class), location);
        updateItem(player);
        startAnimation(base, player, "throw");
        subtractItem(player);
    }

    protected abstract Projectile createProjectile(ItemEvent base, Player player, Location location, Vector velocity);

    public void spawn(ItemEvent base, Player player, double power) {
        Location location = player.getEyeLocation();
        Projectile projectile = createProjectile(base, player, location, location.getDirection().normalize().multiply(base.stat("throw_power", double.class) * power));
        Bukkit.getScheduler().scheduleSyncDelayedTask(getPlugin(), () -> getPlugin().getSchedulerLoop().registerTickable(projectile));
    }
}
