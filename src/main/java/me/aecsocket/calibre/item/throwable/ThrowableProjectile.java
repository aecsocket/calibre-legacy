package me.aecsocket.calibre.item.throwable;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.ParticleData;
import me.aecsocket.unifiedframework.utils.SoundData;
import me.aecsocket.unifiedframework.utils.projectile.Projectile;
import me.aecsocket.unifiedframework.utils.projectile.ProjectileHitResult;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

public class ThrowableProjectile extends Projectile {
    private final CalibrePlugin plugin;
    private ItemEvent base;
    private ThrowableComponent throwable;

    public ThrowableProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity ) {
        super(location, velocity, base.stat("bounce"), base.stat("drag"), base.stat("gravity"), base.stat("steps"), FluidCollisionMode.NEVER, true, base.stat("projectile_size"), entity, e -> !e.isDead());
        plugin = throwable.getPlugin();
        this.base = base;
        this.throwable = throwable;
    }

    public CalibrePlugin getPlugin() { return plugin; }

    public ItemEvent getBase() { return base; }
    public void setBase(ItemEvent base) { this.base = base; }

    public ThrowableComponent getThrowable() { return throwable; }
    public void setThrowable(ThrowableComponent throwable) { this.throwable = throwable; }

    @Override
    public void step(TickContext context) {
        ParticleData.spawn(stat("trail_particle", ParticleData[].class), getLocation());
        super.step(context);
    }

    @Override
    public ProjectileHitResult hit(RayTraceResult ray, TickContext context) {
        SoundData[] hitSound = stat("hit_sound");
        float volume = Math.min(1, Math.max(0, (float)(getStepLength() * getSteps())));
        Location location = getLocation();
        if (hitSound != null) {
            for (SoundData sound : hitSound) {
                sound = sound.clone();
                sound.setVolume(sound.getVolume() * volume);
                sound.play(location);
            }
        }
        return super.hit(ray, context);
    }

    protected <T> T stat(String name) { return base.stat(name); }
    protected <T> T stat(String name, Class<T> type) { return base.stat(name, type); }
}
