package me.aecsocket.calibre.item.throwable.flash;

import me.aecsocket.calibre.effect.FlashEffect;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.item.throwable.triggerable.TriggerableProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class FlashProjectile extends TriggerableProjectile {
    public FlashProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity, int elapsed) {
        super(location, velocity, base, throwable, entity, elapsed);
    }

    @Override
    public void trigger(TickContext tickContext) {
        getPlugin().getWorldEffects().addEffect(new FlashEffect(
                getPlugin(),
                getLocation(),
                stat("radius"), stat("duration"), stat("particle"), stat("sound"), stat("angles"))
        );
        tickContext.removeTickable();
    }
}
