package me.aecsocket.calibre.item.throwable.triggerable;

import me.aecsocket.calibre.item.data.BooleanStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.IntegerStat;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.util.TextUtils;
import me.aecsocket.unifiedframework.loop.SchedulerLoop;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.stat.StatData;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;

public abstract class TriggerableThrowable extends ThrowableComponent {
    private static final DecimalFormat format = new DecimalFormat("0.0");
    public static final String ITEM_TYPE = "triggerable_throwable";
    public static final StatMap STATS = new StatMap(ThrowableComponent.STATS)
            .add("fuse", new IntegerStat().textSupplier((stat, plugin, name, inst, player, value) -> new DecimalFormat("0.0").format(value / SchedulerLoop.TICKS_PER_SECOND)).defaultSupplier(i -> 0))
            .add("trigger_on_impact", new BooleanStat().defaultSupplier(i -> false));

    private int elapsed;

    public int getElapsed() { return elapsed; }
    public void setElapsed(int elapsed) { this.elapsed = elapsed; }

    @Override public String getItemType() { return ITEM_TYPE; }

    @Override
    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, StatData stats, @Nullable Player player, int amount) {
        super.modify(data, meta, item, stats, player, amount);
        data.set(key("elapsed"), PersistentDataType.INTEGER, elapsed);
    }

    protected void sendActionBar(ItemEvent base, Player player) {
        int fuse = base.stat("fuse", int.class);
        int remaining = fuse - elapsed;
        String[] bar = TextUtils.createBar(remaining * 50, fuse * 50, player, getPlugin(), "throwable");
        player.sendActionBar(getPlugin().gen(player, "action_bar.throwable" + (isPrimed() ? ".primed" : ""), bar[0], bar[1], remaining / 20d));
    }

    @Override
    public void hold(ItemEvent base, Player player, TickContext tickContext) {
        super.hold(base, player, tickContext);
        if (base.getBase() != this || !isComplete()) return;
        if (!(tickContext.getLoop() instanceof SchedulerLoop)) return;
        int fuse = base.stat("fuse");
        if (fuse > 0 && isPrimed()) {
            ++elapsed;
            updateItem(player);
            if (elapsed >= fuse) {
                spawn(base, player, 0);
                setPrimed(false);
                elapsed = 0;
                subtractItem(player);
            }
        }

        if (fuse > 0 && getPlugin().setting("throwable.show_action_bar", boolean.class, true))
            sendActionBar(base, player);
    }

    @Override
    public void throwItem(ItemEvent base, Player player) {
        super.throwItem(base, player);
        if (base.getBase() != this || !isComplete()) return;
        elapsed = 0;
        updateItem(player);
    }
}
