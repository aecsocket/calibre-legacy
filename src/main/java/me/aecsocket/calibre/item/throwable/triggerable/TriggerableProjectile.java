package me.aecsocket.calibre.item.throwable.triggerable;

import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.throwable.ThrowableComponent;
import me.aecsocket.calibre.item.throwable.ThrowableProjectile;
import me.aecsocket.unifiedframework.loop.TickContext;
import me.aecsocket.unifiedframework.utils.projectile.ProjectileHitResult;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

public abstract class TriggerableProjectile extends ThrowableProjectile {
    private int elapsed;
    private boolean triggered;

    public TriggerableProjectile(Location location, Vector velocity, ItemEvent base, ThrowableComponent throwable, Entity entity, int elapsed) {
        super(location, velocity, base, throwable, entity);
        this.elapsed = elapsed;
    }

    public int getElapsed() { return elapsed; }
    public void setElapsed(int elapsed) { this.elapsed = elapsed; }

    public boolean isTriggered() { return triggered; }
    public void setTriggered(boolean triggered) { this.triggered = triggered; }

    public abstract void trigger(TickContext tickContext);

    @Override
    public void tick(TickContext tickContext) {
        super.tick(tickContext);
        ++elapsed;
        int fuse = stat("fuse");
        if (fuse > 0 && elapsed >= fuse)
            triggered = true;
        if (triggered)
            trigger(tickContext);
    }

    @Override
    public ProjectileHitResult hit(RayTraceResult ray, TickContext tickContext) {
        super.hit(ray, tickContext);
        if (stat("trigger_on_impact")) {
            triggered = true;
            setVelocity(new Vector());
        }
        return ProjectileHitResult.BOUNCE;
    }
}
