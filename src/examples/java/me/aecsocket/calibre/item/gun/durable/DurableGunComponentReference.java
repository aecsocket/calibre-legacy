package me.aecsocket.calibre.item.gun.durable;

import me.aecsocket.calibre.item.component.CalibreComponent;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.unifiedframework.registry.Registry;

/** A reference to a {@link DurableGunComponent} which stores the component's durability. */
public class DurableGunComponentReference extends ComponentReference {
    private double durability;

    public DurableGunComponentReference(String id, double durability) {
        super(id);
        this.durability = durability;
    }

    public double getDurability() { return durability; }
    public void setDurability(double durability) { this.durability = durability; }

    @Override
    public DurableGunComponent create(Registry registry) {
        CalibreComponent component = super.create(registry);
        if (!(component instanceof DurableGunComponent)) return null;
        DurableGunComponent result = (DurableGunComponent)component;
        result.setDurability(durability);
        return result;
    }
}
