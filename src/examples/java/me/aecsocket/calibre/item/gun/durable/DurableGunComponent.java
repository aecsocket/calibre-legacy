package me.aecsocket.calibre.item.gun.durable;

import com.google.gson.JsonObject;
import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.component.ComponentReference;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import me.aecsocket.unifiedframework.utils.json.JsonUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.Nullable;

/** A {@link GunComponent} which can have a durability value. */
public class DurableGunComponent extends GunComponent {
    /** This class's default stat map. */
    public static final StatMap STATS = new StatMap(GunComponent.STATS)
            .add("durability", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("firing_wear", new DoubleStat().precision(1).defaultSupplier(i -> 0d));

    private double durability;

    public double getDurability() { return durability; }
    public void setDurability(double durability) { this.durability = durability; }

    @Override
    public void fireSuccess(ItemEvent base, Player player, Location location, ChamberComponent chamber, ChamberSlot chamberSlot) {
        super.fireSuccess(base, player, location, chamber, chamberSlot);
        durability -= base.stat("firing_wear", double.class);
    }

    @Override
    protected void modify(PersistentDataContainer data, ItemMeta meta, ItemStack item, @Nullable Player player, int amount) {
        super.modify(data, meta, item, player, amount);
        data.set(key("durability"), PersistentDataType.DOUBLE, durability);
    }

    /*@Override
    protected List<String> getStatLore(@Nullable Player player, int amount) {
        List<String> result = super.getStatLore(player, amount);
        result.add(getPlugin().gen("stat.meta.durability", durability));
        return result;
    } TODO */

    @Override
    public ComponentReference createReference() { return new DurableGunComponentReference(getId(), durability); }

    /** Registers all necessary parts in the {@link CalibrePlugin}.
     * @param plugin The {@link CalibrePlugin}.
     */
    public static void register(CalibrePlugin plugin) {
        plugin.registerComponentType(DurableGunComponent.class, "durable_gun_component", () -> STATS);
        plugin.registerComponentReferenceType(DurableGunComponentReference.class, "durable",
                (ref, jType, context) -> JsonUtils.buildObject()
                        .add("type", "durable")
                        .add("id", ref.getId())
                        .add("durability", ((DurableGunComponentReference)ref).getDurability())
                        .create(),
                (json, jType, context) -> {
                    JsonObject object = JsonUtils.assertObject(json);
                    return new DurableGunComponentReference(
                            JsonUtils.get(object, "id").getAsString(),
                            JsonUtils.get(object, "durability").getAsDouble()
                    );
        });
    }
}
