package me.aecsocket.calibre.item.gun.propelling;

import me.aecsocket.calibre.CalibrePlugin;
import me.aecsocket.calibre.item.data.BooleanStat;
import me.aecsocket.calibre.item.data.StatMap;
import me.aecsocket.calibre.item.data.number.DoubleStat;
import me.aecsocket.calibre.item.event.ItemEvent;
import me.aecsocket.calibre.item.gun.GunComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberComponent;
import me.aecsocket.calibre.item.gun.chamber.ChamberSlot;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/** Propels the shooter once they fire a bullet from a {@link GunComponent}. */
public class PropellingGunComponent extends GunComponent {
    /** This class's default stat map. */
    public static final StatMap STATS = new StatMap(GunComponent.STATS)
            .add("fire_propel", new DoubleStat().precision(1).defaultSupplier(i -> 0d))
            .add("propel_ignores_y", new BooleanStat().defaultSupplier(i -> false));

    @Override
    public void fireSuccess(ItemEvent base, Player player, Location location, ChamberComponent chamber, ChamberSlot chamberSlot) {
        super.fireSuccess(base, player, location, chamber, chamberSlot);
        Vector direction = player.getLocation().getDirection();
        direction.normalize().multiply(base.stat("fire_propel"));
        if (base.stat("propel_ignores_y"))
            direction.setY(0);
        player.setVelocity(player.getVelocity().add(direction));
    }

    /** Registers all necessary parts in the {@link CalibrePlugin}.
     * @param plugin The {@link CalibrePlugin}.
     */
    public static void register(CalibrePlugin plugin) {
        plugin.registerComponentType(PropellingGunComponent.class, "propelling_gun_component", () -> STATS);
    }
}
